#pragma once

// ラベル定義
#define ENEMY_MAX		     600

//エネミー当たり判定サイズ
#define ENEMY00_SIZEX		 30
#define ENEMY00_SIZEY		 30
#define ENEMY01_SIZEX		 30
#define ENEMY01_SIZEY		 28
#define ENEMY02_SIZEX		 30
#define ENEMY02_SIZEY		 20
#define ENEMY03_SIZEX		 30
#define ENEMY03_SIZEY		 20
#define ENEMY04_SIZEX		 30
#define ENEMY04_SIZEY		 20
#define ENEMY05_SIZEX		 44
#define ENEMY05_SIZEY		 44
#define ENEMY06_SIZEX		 44
#define ENEMY06_SIZEY		 44
#define ENEMY07_SIZEX		 120
#define ENEMY07_SIZEY		 540
#define ENEMY08_SIZEX		 44
#define ENEMY08_SIZEY		 44

#define ENEMY_SCLAE			 1.5f//エネミースケール
#define ENEMY_SWITCH_SCLAE   0.45f//スイッチのスケール
#define ENEMY_WAIT			 1.1f//エネミーの質量

//パラメータ、カウント関係
#define ENEMY01_MOVECNT		 120
#define ENEMY01_SPEED        5
#define ENEMY02_MOVECNT      120
#define ENEMY02_SPEED		 3.0f
#define ENEMY_MOVE_DISTANCE  1000
#define ENEMY03_MOVECNT		 80
#define ENEMY03_SHOTCNT		 32
#define ENEMY04_MOVECNT		 80
#define ENEMY04_SHOTCNT		 32

// BlockManagerクラス
class EnemyManager : public OBJ2DManager, public Singleton<EnemyManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return ENEMY_MAX; }
};

#define pEnemyManager	(EnemyManager::getInstance())

namespace enemy
{

	enum Type
	{
		ENEMY,
		GIMMICK,
	};

	// iWorkのラベル
	enum iWork {

	};

	// fWorkのラベル
	enum fWork {
		MoveAngle,
		InitPositionX,
		InitPositionY

	};
}
void enemy_move00(OBJ2D*obj);
void enemy_move01(OBJ2D*obj);
void enemy_move02(OBJ2D*obj);
void enemy_move03(OBJ2D*obj);
void enemy_move04(OBJ2D*obj);
void enemy_move05(OBJ2D*obj);
void enemy_move06(OBJ2D*obj);
void enemy_move07(OBJ2D*obj);
void enemy_move08(OBJ2D*obj);

