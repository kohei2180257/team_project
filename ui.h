#pragma once

#define UI_MAX 64


struct UI_INIT_DATA 
{
	MOVER mover;
	float posX;
	float posY;
};


class UiManager : public OBJ2DManager, public Singleton<UiManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return UI_MAX; }
};

#define pUiManager	(UiManager::getInstance())