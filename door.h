#pragma once
#define DOOR_MAX_SIZE 100


class DoorManager : public OBJ2DManager, public Singleton<DoorManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return DOOR_MAX_SIZE; }
};

#define pDoorManager	(DoorManager::getInstance())

