//******************************************************************************
//
//
//      ゲーム
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;

//--------------------------------
//  初期化処理
//--------------------------------
void Game::init()
{
	Scene::init();	    // 基底クラスのinitを呼ぶ
	
	isPaused = false;   // ポーズフラグの初期化
}

//--------------------------------
//  更新処理
//--------------------------------
void Game::update()
{
	using namespace input;



	// デバッグ文字列表示


	switch (state)
	{
	case 0:
		//////// 初期設定 ////////

		timer = 0;
		GameLib::setBlendMode(Blender::BS_ALPHA);   // 通常のアルファ処理
		// テクスチャの読み込み
		texture::load(loadTextureGame);

		if(pStageManager->stageNo%2==0)music::load(GAMEBGM, L"./Data/Musics/battle2.wav", 1.0f);
		else music::load(GAMEBGM, L"./Data/Musics/battle.wav", 1.0f);

		music::play(GAMEBGM,true);

		pStageManager->init();

	
		state++;    // 初期化処理の終了
	//	break;      // 意図的なコメントアウト

	case 1:
		//////// 通常時の処理 ////////



		//ヒットストップ
		if (SCENE_GAME->HitStop())
			return;
		//ヒットストップ

		timer++;

		pStageManager->update();





		break;
	}

	//debug::setString("timer%d", timer);

}

//--------------------------------
//  描画処理
//--------------------------------
void Game::draw()
{
	// 画面クリア
	GameLib::clear(VECTOR4(0, 0, 0, 1));

	pStageManager->draw();

	//font::textOut(4, "GAME", VECTOR2(60, 60), VECTOR2(2.0f, 2.0f), VECTOR4(1, 1, 0, 1));



}

//--------------------------------
//  終了処理
//--------------------------------
void Game::uninit()
{
	// テクスチャの解放
	texture::releaseAll();
	music::unload(GAMEBGM);
	
}

//******************************************************************************

bool Game::HitStop()
{
	if (!stop_fg)
		return false;

	stop_cnt++;

	if (stop_cnt < hitStopFrame)
		return true;

	if (stop_cnt >= hitStopFrame)
	{
		stop_cnt = 0;
		stop_fg = false;
	}
		return false;


}