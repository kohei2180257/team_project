#pragma once


//******************************************************************************
//
//
//      Player.h
//
//
//******************************************************************************
namespace player
{
	enum Flg
	{
	hitFloor=(1<<0),
	isGoal=(1<<1),
	isDead = (1 << 2),

	};

	// iWorkのラベル
	enum iWork {
		hitFlg,
		goalFlg,
		score,
	};

	// fWorkのラベル
	enum fWork{
		fromPlShotAngle,
		fromPlShotDistance,
	};
}

//スティック情報取得系
#define	GET_LEFT_STICK_X		(InputManager::getInstance()->getPadAddress()->leftX)
#define	GET_LEFT_STICK_Y		(InputManager::getInstance()->getPadAddress()->leftY)
#define	GET_RIGHT_STICK_X		(InputManager::getInstance()->getPadAddress()->rightX)
#define	GET_RIGHT_STICK_Y		(InputManager::getInstance()->getPadAddress()->rightY)

//定数
#define ENEMY_STOP_FRAME	(4)		//敵とのヒットストップ
#define PL_SLOW_SPEED		(0.4f)	//スロウ時倍率
#define DAMAGE_FRAME		(85)	//のけぞり時間
#define MUTEKI_FRAME		(45)	//無敵時間
#define PLUS_DASH_CNT		(3)		//ダッシュカウントプラス値
#define COMBOLEVEL_1		(5)		//コンボレベル１
#define COMBOLEVEL_2		(10)	//コンボレベル２
#define COMBOLEVEL_3		(15)	//コンボレベル３
#define GRAVITY				(0.4f)	//重力(横向き)
#define PLAYER_SIZEX		(26.f)	//当たり判定のサイズ
#define PLAYER_SIZEY		(28.f)  //当たり判定のサイズ
#define PLAYER_SCALE		(1.5f)  //スケール
#define PLAYER_WAIT			(2.0f)	//重量
#define PLAYER_SPEED_INIT	(7.f)	//初速
#define PLAYER_DASH_CNT		(30)	//ダッシュ持続
#define PLAYER_DIR_UP		(1)		//上向いてる
#define PLAYER_DIR_DOWN		(-1)	//下向いてる
#define PLAYER_VELOCITY		(0.1f)	//プレイヤー加速度
#define GRAVITY_LIMIT		(9.0f)	//限界速度
#define ADDSCORE_ENEMY00	(10)	//スコア加算値
#define ADDSCORE_ENEMY01	(20)	//スコア加算値
#define ADDSCORE_ENEMY02	(30)	//スコア加算値
#define ADDSCORE_ENEMY03	(50)	//スコア加算値
#define ADDSCORE_ITEM		(100)	//スコア加算値
#define ADDSCORE_HELP		(10000) //スコア加算値
#define FINISH_SPEED		(25.f)	//演出用初速
#define FINISH_VELOCITY		(1.1f)	//演出用加速度


//移動関数
void player_move(OBJ2D* obj);
void player_move_finish(OBJ2D* obj);

//その他
void PlayerBlink(OBJ2D*obj);



//==============================================================================
//
//      移動アルゴリズム
//
//==============================================================================


//==============================================================================
//
//      PlayerManagerクラス
//
//==============================================================================
class PlayerManager : public OBJ2DManager, public Singleton<PlayerManager>
{
public:

public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画

	bool isPowerup_flg;//パワーアップ状態か否か
	bool isBarrier_flg;//バリア状態か否か

	int resqueNum;//助けたウサギの数
private:
	int getSize() { return 1; }

};

//------< インスタンス取得 >-----------------------------------------------------
#define pPlayerManager  (PlayerManager::getInstance())


