#pragma once

//******************************************************************************
//
//
//		SPRITE_DATA
//
//
//******************************************************************************

#include "./GameLib/texture.h"

// ラベル定義
enum TEXNO
{
	// ゲーム
	PLAYER00,
	PLAYER01,
	PLAYER02,
	PLAYER03,
	ENEMY00,
	ENEMY01,
	ENEMY02,
	ENEMY03,
	ENEMY04,
	ENEMY05,
	ITEM00,
	ITEM01,
	OTHER00,
	OTHER01,
	TITLE,
	TEXT,
	BLACK,
	MAP_BACK,
	MAP_BACK2,
	MAP_TERRAIN,
	GIMMICK00,
	GIMMICK01,
	GIMMICK02,

	EFFECT00,
	EFFECT01,
	EFFECT02,
	EFFECT03,
	EFFECT04,
	EFFECT05,
	UI00,
	UI01,
	UI02,
	UI03,
	UI04,
	UI05,
	UI06,
	UI07,

	FONT00,
	FONT01,
	FONT02,
	FONT03,
	FONT04,
	FONT05,
	FONT06,
	FONT07,
	FONT08,
	FONT09,


	CLEARBG,
	OVERBG,

	DEMO,

};

extern GameLib::LoadTexture loadTextureDemo[];
extern GameLib::LoadTexture loadTextureTitle[];
extern GameLib::LoadTexture loadTextureGame[];
extern GameLib::LoadTexture loadTextureResult[];

extern GameLib::SpriteData spr_player00;
extern GameLib::SpriteData spr_player01;
extern GameLib::SpriteData spr_player02;
extern GameLib::SpriteData spr_player03;
extern GameLib::SpriteData spr_player04;
extern GameLib::SpriteData spr_player05;
extern GameLib::SpriteData spr_player06;
extern GameLib::SpriteData spr_player07;
extern GameLib::SpriteData spr_player08;
extern GameLib::SpriteData spr_player09;
extern GameLib::SpriteData spr_player10;
extern GameLib::SpriteData spr_player11;
extern GameLib::SpriteData spr_player12;
extern GameLib::SpriteData spr_player13;
extern GameLib::SpriteData spr_player14;
extern GameLib::SpriteData spr_player15;
extern GameLib::SpriteData spr_player16;


extern GameLib::SpriteData spr_effect_move00;
extern GameLib::SpriteData spr_effect_move01;
extern GameLib::SpriteData spr_effect_move02;
extern GameLib::SpriteData spr_effect_move03;
extern GameLib::SpriteData spr_effect_move04;
extern GameLib::SpriteData spr_effect_move05;
extern GameLib::SpriteData spr_effect_move06;
extern GameLib::SpriteData spr_effect_move07;
extern GameLib::SpriteData spr_effect_move08;
extern GameLib::SpriteData spr_effect_move09;
extern GameLib::SpriteData spr_effect_move10;
extern GameLib::SpriteData spr_effect_move11;
extern GameLib::SpriteData spr_effect_move12;
extern GameLib::SpriteData spr_effect_move13;
extern GameLib::SpriteData spr_effect_move14;
extern GameLib::SpriteData spr_effect_move15;
extern GameLib::SpriteData spr_effect_move16;
extern GameLib::SpriteData spr_effect_move17;
extern GameLib::SpriteData spr_effect_move18;
extern GameLib::SpriteData spr_effect_move19;
extern GameLib::SpriteData spr_effect_move20;
extern GameLib::SpriteData spr_effect_move21;
extern GameLib::SpriteData spr_effect_move22;
extern GameLib::SpriteData spr_effect_move23;
extern GameLib::SpriteData spr_effect_move24;
extern GameLib::SpriteData spr_effect_move25;
extern GameLib::SpriteData spr_effect_move26;
extern GameLib::SpriteData spr_effect_move27;
extern GameLib::SpriteData spr_effect_move28;
extern GameLib::SpriteData spr_effect_move29;
extern GameLib::SpriteData spr_effect_move30;
extern GameLib::SpriteData spr_effect_move31;
extern GameLib::SpriteData spr_effect_move32;
extern GameLib::SpriteData spr_effect_move33;
extern GameLib::SpriteData spr_effect_spawn00;
extern GameLib::SpriteData spr_effect_spawn01;
extern GameLib::SpriteData spr_effect_spawn02;
extern GameLib::SpriteData spr_effect_spawn03;
extern GameLib::SpriteData spr_effect_spawn04;
extern GameLib::SpriteData spr_effect_spawn05;
extern GameLib::SpriteData spr_effect_spawn06;
extern GameLib::SpriteData spr_effect_spawn07;
extern GameLib::SpriteData spr_effect_spawn08;
extern GameLib::SpriteData spr_effect_spawn09;
extern GameLib::SpriteData spr_title;
extern GameLib::SpriteData spr_titlebg;
extern GameLib::SpriteData spr_number00;
extern GameLib::SpriteData spr_number01;
extern GameLib::SpriteData spr_number02;
extern GameLib::SpriteData spr_number03;
extern GameLib::SpriteData spr_number04;
extern GameLib::SpriteData spr_number05;
extern GameLib::SpriteData spr_number06;
extern GameLib::SpriteData spr_number07;
extern GameLib::SpriteData spr_number08;
extern GameLib::SpriteData spr_number09;
extern GameLib::SpriteData spr_sc_number00;
extern GameLib::SpriteData spr_sc_number01;
extern GameLib::SpriteData spr_sc_number02;
extern GameLib::SpriteData spr_sc_number03;
extern GameLib::SpriteData spr_sc_number04;
extern GameLib::SpriteData spr_sc_number05;
extern GameLib::SpriteData spr_sc_number06;
extern GameLib::SpriteData spr_sc_number07;
extern GameLib::SpriteData spr_sc_number08;
extern GameLib::SpriteData spr_sc_number09;
extern GameLib::SpriteData spr_effect_enemy_death00;
extern GameLib::SpriteData spr_effect_enemy_death01;
extern GameLib::SpriteData spr_effect_enemy_death02;
extern GameLib::SpriteData spr_effect_enemy_death03;
extern GameLib::SpriteData spr_effect_enemy_death04;
extern GameLib::SpriteData spr_arrow;
extern GameLib::SpriteData spr_ui00;
extern GameLib::SpriteData spr_ui01;
extern GameLib::SpriteData spr_ui02;
extern GameLib::SpriteData spr_ui03;
extern GameLib::SpriteData spr_enemy_move00;
extern GameLib::SpriteData spr_enemy_move01;
extern GameLib::SpriteData spr_enemy_move02;
extern GameLib::SpriteData spr_enemy_move03;
extern GameLib::SpriteData spr_enemy_shot00;
extern GameLib::SpriteData spr_enemy_shot01;
extern GameLib::SpriteData spr_enemy_shot02;
extern GameLib::SpriteData spr_enemy_shot03;
extern GameLib::SpriteData spr_enemy_shot04;
extern GameLib::SpriteData spr_enemy_shot05;
extern GameLib::SpriteData spr_enemy_shot06;
extern GameLib::SpriteData spr_enemy_shot07;
extern GameLib::SpriteData spr_enemy_dash00;
extern GameLib::SpriteData spr_enemy_dash01;
extern GameLib::SpriteData spr_enemy_dash02;
extern GameLib::SpriteData spr_enemy_dash03;
extern GameLib::SpriteData spr_enemy_float00;
extern GameLib::SpriteData spr_enemy_float01;
extern GameLib::SpriteData spr_enemy_float02;
extern GameLib::SpriteData spr_enemy_float03;
extern GameLib::SpriteData spr_bg;
extern GameLib::SpriteData spr_break_floor;
extern GameLib::SpriteData color_black;

extern GameLib::SpriteData spr_player17;
extern GameLib::SpriteData spr_player18;
extern GameLib::SpriteData spr_player19;
extern GameLib::SpriteData spr_player20;
extern GameLib::SpriteData spr_player21;
extern GameLib::SpriteData spr_player22;


extern GameLib::SpriteData spr_effect_dash00;
extern GameLib::SpriteData spr_effect_dash01;
extern GameLib::SpriteData spr_effect_dash02;
extern GameLib::SpriteData spr_effect_dash03;
extern GameLib::SpriteData spr_effect_dash04;
extern GameLib::SpriteData spr_effect_dash05;
extern GameLib::SpriteData spr_effect_dash06;
extern GameLib::SpriteData spr_effect_dash07;
extern GameLib::SpriteData spr_effect_dash08;
extern GameLib::SpriteData spr_effect_dash09;
extern GameLib::SpriteData spr_effect_dash10;
extern GameLib::SpriteData spr_effect_dash11;
extern GameLib::SpriteData spr_effect_dash12;
extern GameLib::SpriteData spr_effect_dash13;
extern GameLib::SpriteData spr_effect_dash14;

extern GameLib::SpriteData spr_enemy_shot_begin00;
extern GameLib::SpriteData spr_enemy_shot_begin01;
extern GameLib::SpriteData spr_enemy_shot_begin02;
extern GameLib::SpriteData spr_enemy_shot_begin03;
extern GameLib::SpriteData spr_enemy_shot_begin04;
extern GameLib::SpriteData spr_enemy_shot_begin05;
extern GameLib::SpriteData spr_enemy_shot_begin06;
extern GameLib::SpriteData spr_enemy_shot_begin07;
extern GameLib::SpriteData spr_enemy_shot_begin08;
extern GameLib::SpriteData spr_enemy_shot2_begin00;
extern GameLib::SpriteData spr_enemy_shot2_begin01;
extern GameLib::SpriteData spr_enemy_shot2_begin02;
extern GameLib::SpriteData spr_enemy_shot2_begin03;
extern GameLib::SpriteData spr_enemy_shot2_begin04;
extern GameLib::SpriteData spr_enemy_shot2_begin05;
extern GameLib::SpriteData spr_enemy_shot2_begin06;
extern GameLib::SpriteData spr_enemy_shot2_begin07;
extern GameLib::SpriteData spr_enemy_shot2_begin08;
extern GameLib::SpriteData spr_enemy_bullet;

extern GameLib::SpriteData spr_enemy_black_hole00;
extern GameLib::SpriteData spr_enemy_black_hole01;
extern GameLib::SpriteData spr_enemy_black_hole02;
extern GameLib::SpriteData spr_enemy_black_hole03;
extern GameLib::SpriteData spr_enemy_black_hole04;
extern GameLib::SpriteData spr_enemy_black_hole05;
extern GameLib::SpriteData spr_enemy_black_hole06;
extern GameLib::SpriteData spr_enemy_black_hole07;

extern GameLib::SpriteData spr_help_rabitt00;
extern GameLib::SpriteData spr_help_rabitt01;
extern GameLib::SpriteData spr_help_rabitt02;
extern GameLib::SpriteData spr_help_rabitt03;
extern GameLib::SpriteData spr_help_rabitt04;
extern GameLib::SpriteData spr_help_rabitt05;
extern GameLib::SpriteData spr_help_rabitt06;
extern GameLib::SpriteData spr_item_power;
extern GameLib::SpriteData spr_item_bariier;
extern GameLib::SpriteData spr_item_1up;
extern GameLib::SpriteData spr_particle00;
extern GameLib::SpriteData spr_particle01;
extern GameLib::SpriteData spr_particle02;
extern GameLib::SpriteData spr_particle03;
extern GameLib::SpriteData spr_spark00;
extern GameLib::SpriteData spr_spark01;
extern GameLib::SpriteData spr_spark02;
extern GameLib::SpriteData spr_spark03;
extern GameLib::SpriteData spr_spark04;
extern GameLib::SpriteData spr_spark05;
extern GameLib::SpriteData spr_spark06;
extern GameLib::SpriteData spr_spark07;
extern GameLib::SpriteData spr_spark08;
extern GameLib::SpriteData spr_spark09;
extern GameLib::SpriteData spr_spark10;
extern GameLib::SpriteData spr_spark11;
extern GameLib::SpriteData spr_player23;
extern GameLib::SpriteData spr_player24;
extern GameLib::SpriteData spr_player25;
extern GameLib::SpriteData spr_player26;
extern GameLib::SpriteData spr_player27;
extern GameLib::SpriteData spr_block00;
extern GameLib::SpriteData spr_block01;
extern GameLib::SpriteData spr_block02;
extern GameLib::SpriteData spr_block03;
extern GameLib::SpriteData spr_block04;
extern GameLib::SpriteData spr_block05;
extern GameLib::SpriteData spr_block06;
extern GameLib::SpriteData spr_block07;
extern GameLib::SpriteData spr_block08;
extern GameLib::SpriteData spr_block09;
extern GameLib::SpriteData spr_block10;
extern GameLib::SpriteData spr_block11;
extern GameLib::SpriteData spr_switch00;
extern GameLib::SpriteData spr_switch01;
extern GameLib::SpriteData spr_switch02;
extern GameLib::SpriteData spr_switch03;
extern GameLib::SpriteData spr_switch04;
extern GameLib::SpriteData spr_switch05;
extern GameLib::SpriteData spr_power_up_font00;
extern GameLib::SpriteData spr_power_up_font01;
extern GameLib::SpriteData spr_power_up_font02;
extern GameLib::SpriteData spr_power_up_font03;
extern GameLib::SpriteData spr_power_up_font04;
extern GameLib::SpriteData spr_power_up_font05;
extern GameLib::SpriteData spr_power_up_font06;
extern GameLib::SpriteData spr_power_up_font07;
extern GameLib::SpriteData spr_power_up_font08;
extern GameLib::SpriteData spr_power_up_font09;
extern GameLib::SpriteData spr_power_up_font10;
extern GameLib::SpriteData spr_power_up_font11;
extern GameLib::SpriteData spr_power_up_font12;
extern GameLib::SpriteData spr_power_up_font13;
extern GameLib::SpriteData spr_power_up_font14;

extern GameLib::SpriteData spr_speed_up_font00;
extern GameLib::SpriteData spr_speed_up_font01;
extern GameLib::SpriteData spr_speed_up_font02;
extern GameLib::SpriteData spr_speed_up_font03;
extern GameLib::SpriteData spr_speed_up_font04;
extern GameLib::SpriteData spr_speed_up_font05;
extern GameLib::SpriteData spr_speed_up_font06;
extern GameLib::SpriteData spr_speed_up_font07;
extern GameLib::SpriteData spr_speed_up_font08;
extern GameLib::SpriteData spr_speed_up_font09;
extern GameLib::SpriteData spr_speed_up_font10;
extern GameLib::SpriteData spr_speed_up_font11;
extern GameLib::SpriteData spr_speed_up_font12;
extern GameLib::SpriteData spr_speed_up_font13;
extern GameLib::SpriteData spr_speed_up_font14;


extern GameLib::SpriteData spr_barrier_font00;
extern GameLib::SpriteData spr_barrier_font01;
extern GameLib::SpriteData spr_barrier_font02;
extern GameLib::SpriteData spr_barrier_font03;
extern GameLib::SpriteData spr_barrier_font04;
extern GameLib::SpriteData spr_barrier_font05;
extern GameLib::SpriteData spr_barrier_font06;
extern GameLib::SpriteData spr_barrier_font07;
extern GameLib::SpriteData spr_barrier_font08;
extern GameLib::SpriteData spr_barrier_font09;
extern GameLib::SpriteData spr_barrier_font10;
extern GameLib::SpriteData spr_barrier_font11;
extern GameLib::SpriteData spr_barrier_font12;
extern GameLib::SpriteData spr_barrier_font13;
extern GameLib::SpriteData spr_barrier_font14;


extern GameLib::SpriteData spr_help_rabbit00;
extern GameLib::SpriteData spr_help_rabbit01;
extern GameLib::SpriteData spr_help_rabbit02;
extern GameLib::SpriteData spr_help_rabbit03;
extern GameLib::SpriteData spr_help_rabbit04;
extern GameLib::SpriteData spr_help_rabbit05;
extern GameLib::SpriteData spr_help_rabbit06;

extern GameLib::SpriteData spr_barrier00;
extern GameLib::SpriteData spr_barrier01;
extern GameLib::SpriteData spr_barrier02;
extern GameLib::SpriteData spr_barrier03;
extern GameLib::SpriteData spr_barrier04;
extern GameLib::SpriteData spr_barrier05;

extern GameLib::SpriteData spr_player_title_n00;
extern GameLib::SpriteData spr_player_title_n01;
extern GameLib::SpriteData spr_player_title_n02;
extern GameLib::SpriteData spr_player_title_n03;
extern GameLib::SpriteData spr_player_title_n04;
extern GameLib::SpriteData spr_player_title_n05;
extern GameLib::SpriteData spr_player_title_f00;
extern GameLib::SpriteData spr_player_title_f01;
extern GameLib::SpriteData spr_player_title_f02;
extern GameLib::SpriteData spr_player_title_f03;
extern GameLib::SpriteData spr_player_title_f04;
extern GameLib::SpriteData spr_player_title_f05;
extern GameLib::SpriteData spr_player_title_ff00;
extern GameLib::SpriteData spr_player_title_ff01;
extern GameLib::SpriteData spr_player_title_ff02;
extern GameLib::SpriteData spr_player_title_ff03;
extern GameLib::SpriteData spr_player_title_ff04;
extern GameLib::SpriteData spr_player_title_ff05;
extern GameLib::SpriteData spr_anime_start00;
extern GameLib::SpriteData spr_anime_start01;
extern GameLib::SpriteData spr_anime_start02;
extern GameLib::SpriteData spr_anime_start03;
extern GameLib::SpriteData spr_anime_start04;
extern GameLib::SpriteData spr_anime_start05;
extern GameLib::SpriteData spr_anime_start06;
extern GameLib::SpriteData spr_anime_start07;
extern GameLib::SpriteData spr_anime_start08;
extern GameLib::SpriteData spr_anime_start09;
extern GameLib::SpriteData spr_anime_start10;

//クリアBG
extern GameLib::SpriteData spr_clear_bg;


//ネクストステージ
extern GameLib::SpriteData spr_font00;
//イグジット
extern GameLib::SpriteData spr_font01;
//スタート
extern GameLib::SpriteData spr_font02;
//リトライ
extern GameLib::SpriteData spr_font03;

//ネクストステージ
extern GameLib::SpriteData spr_font04;
//イグジット				
extern GameLib::SpriteData spr_font05;
//スタート					
extern GameLib::SpriteData spr_font06;
//リトライ
extern GameLib::SpriteData spr_font07;

extern GameLib::SpriteData spr_gameover;


extern GameLib::SpriteData spr_font05_clear;
extern GameLib::SpriteData spr_font01_clear;



extern GameLib::SpriteData spr_start_ui00;
extern GameLib::SpriteData spr_start_ui01;
extern GameLib::SpriteData spr_start_ui02;
extern GameLib::SpriteData spr_start_ui03;
extern GameLib::SpriteData spr_start_ui04;
extern GameLib::SpriteData spr_start_ui05;
extern GameLib::SpriteData spr_start_ui06;
extern GameLib::SpriteData spr_start_ui07;
extern GameLib::SpriteData spr_start_ui08;
extern GameLib::SpriteData spr_start_ui09;
extern GameLib::SpriteData spr_demo00;
extern GameLib::SpriteData spr_demo01;
extern GameLib::SpriteData spr_demo02;
extern GameLib::SpriteData spr_demo03;
extern GameLib::SpriteData spr_demo04;
extern GameLib::SpriteData spr_demo05;
extern GameLib::SpriteData spr_demo06;
extern GameLib::SpriteData spr_demo07;

//******************************************************************************
