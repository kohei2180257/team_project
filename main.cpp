//******************************************************************************
//
//
//      main
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

//--------------------------------
//  WinMain（エントリポイント）
//--------------------------------
int APIENTRY wWinMain(HINSTANCE instance, HINSTANCE prevInstance, 
    LPWSTR cmdLine, int cmdShow)
{
    UNREFERENCED_PARAMETER(instance);       // 一度も変数を使用しないと警告が出るため対処
    UNREFERENCED_PARAMETER(prevInstance);
    UNREFERENCED_PARAMETER(cmdLine);
    UNREFERENCED_PARAMETER(cmdShow);

    SceneManager sceneManager;
    sceneManager.execute(SCENE_DEMO);
	
    return 0;
}

//******************************************************************************
