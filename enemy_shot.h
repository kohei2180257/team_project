#pragma once

// ラベル定義
#define ENEMY_SHOT_MAX		100

// BlockManagerクラス
class EnemyShotManager : public OBJ2DManager, public Singleton<EnemyShotManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return ENEMY_SHOT_MAX; }
};

#define pEnemyShotManager	(EnemyShotManager::getInstance())


void enemyshot_move00(OBJ2D*obj);
void enemyshot_move01(OBJ2D*obj);

