#include"all.h"

#include <string>
using namespace GameLib;


bool erase_enemy(OBJ2D* obj)
{
	if (obj->position.x > system::SCREEN_WIDTH+pBGManager->getScrollX()) return true;
	if (obj->position.x < 0+pBGManager->getScrollX()) return true;
	if (obj->position.y > system::SCREEN_HEIGHT + pBGManager->getScrollY()) return true;
	if (obj->position.y < 0 + pBGManager->getScrollY()) return true;
	return false;
}

#define ENM_SHOT_SIZEX 30 
#define ENM_SHOT_SIZEY 10 
#define ENM_SHOT_SCALE 2.f 
#define ENM_SHOT_SPEED 5 


//左右弾
void enemyshot_move00(OBJ2D*obj)
{


	switch (obj->state) 
	{

	case 0:
		obj->size = VECTOR2(ENM_SHOT_SIZEX, ENM_SHOT_SIZEY);
		obj->scale *= ENM_SHOT_SCALE;
		obj->speed.x = -ENM_SHOT_SPEED;
		obj->angle = ToRadian(0);
		obj->data = &spr_enemy_bullet;
		obj->eraser = erase_enemy;
		GameLib::sound::play(SOUND::SOUND_SHOT);

		obj->state++;
		//break;

	case 1:

		obj->position.x += obj->speed.x;

		if (judgeCheck(*obj, *pHoleManager->getOBJ2D(0)))
			obj->clear();


		break;

	}
}


//上下弾
void enemyshot_move01(OBJ2D*obj)
{

	switch (obj->state) {

	case 0:
		obj->size = VECTOR2(ENM_SHOT_SIZEX, ENM_SHOT_SIZEY);
		obj->scale *= ENM_SHOT_SCALE;
		obj->speed.y = -ENM_SHOT_SPEED;
		obj->angle = ToRadian(90);
		obj->data = &spr_enemy_bullet;
		obj->eraser = erase_enemy;
		GameLib::sound::play(SOUND::SOUND_SHOT);


		obj->state++;

		//break;

	case 1:

		obj->position.y += obj->speed.y;
		
		if (judgeCheck(*obj, *pHoleManager->getOBJ2D(0)))
			obj->clear();


		break;
	
	}
	
}



void EnemyShotManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化
}

void EnemyShotManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの更新
}

void EnemyShotManager::draw()
{
	OBJ2DManager::draw();   // OBJ2DManagerの描画
	/*if (DEBUG_NOW) {
		for (auto &obj : *pEnemyShotManager) {
			primitive::rect(obj.position.x - obj.size.x - pBGManager->getScrollPos().x, obj.position.y - obj.size.y - pBGManager->getScrollPos().y, obj.size.x * 2, obj.size.y * 2, 0, 0, 0, 1, 0, 0, 0.3f);
		}
	}*/
}
