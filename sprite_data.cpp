//******************************************************************************
//
//
//		SPRITE_DATA
//
//
//******************************************************************************

//------< インクルード >--------------------------------------------------------

#include "all.h"

using namespace GameLib;

//------< データ >--------------------------------------------------------------
// 2D画像ロードデータデモ
LoadTexture loadTextureDemo[] = {
	{ TEXNO::DEMO, L"./Data/Images/DemoAnime.png", 3U },// プレイヤー
	{ -1, nullptr }	// 終了フラグ
};
// 2D画像ロードデータタイトル
LoadTexture loadTextureTitle[] = {
	{ TEXNO::PLAYER00, L"./Data/Images/player_nautral.png", 3U },// プレイヤー
	{ TEXNO::UI06,   L"./Data/Images/titlePlayer.png", 1U },
	{ TEXNO::TITLE,   L"./Data/Images/title.png",1U },
	{ TEXNO::PLAYER01, L"./Data/Images/player_attack.png", 1U },// プレイヤー
	{ TEXNO::MAP_BACK2,      L"./Data/Images/titlebg.png",    1U },    // マップ背景
	{ TEXNO::FONT05,   L"./Data/Images/startAnime.png",  1U },
	{ TEXNO::FONT07,   L"./Data/Images/font_some.png",  8U },
	{ -1, nullptr }	// 終了フラグ


};

// 2D画像ロードデータゲーム
LoadTexture loadTextureGame[] = {
	{ TEXNO::PLAYER00, L"./Data/Images/player_nautral.png", 1U },// プレイヤー
	{ TEXNO::PLAYER01, L"./Data/Images/player_attack.png", 1U },// プレイヤー
	{ TEXNO::PLAYER03, L"./Data/Images/player_attack2.png", 1U },// プレイヤー
	{ TEXNO::PLAYER02, L"./Data/Images/player_damage.png", 1U },// プレイヤー
	{ TEXNO::ENEMY00, L"./Data/Images/enemy.png", 200U },
	{ TEXNO::ENEMY01, L"./Data/Images/enm_shot.png", 100U },
	{ TEXNO::ENEMY02, L"./Data/Images/enm_shot2.png", 100U },
	{ TEXNO::ENEMY03, L"./Data/Images/enm_move2.png", 100U },
	{ TEXNO::ENEMY04, L"./Data/Images/bullet.png", 32U },
	{ TEXNO::ENEMY05, L"./Data/Images/blackhole.png", 1U },
	{ TEXNO::ITEM00, L"./Data/Images/item.png", 16U },
	{ TEXNO::ITEM01, L"./Data/Images/help.png", 16U },
	{ TEXNO::OTHER00, L"./Data/Images/particle.png", 32U },
	{ TEXNO::OTHER01, L"./Data/Images/effect_spark.png", 32U },
	{ TEXNO::EFFECT00,   L"./Data/Images/effect_enemy.png", 20U },
	{ TEXNO::EFFECT01,   L"./Data/Images/effect_move00.png", 20U },
	{ TEXNO::EFFECT02,   L"./Data/Images/effect_move01.png", 20U },
	{ TEXNO::EFFECT03,   L"./Data/Images/effect_dash.png", 20U },
	{ TEXNO::EFFECT04,   L"./Data/Images/spawn_effect.png", 20U },
	{ TEXNO::EFFECT05,   L"./Data/Images/barrier.png", 20U },
	{ TEXNO::UI00,   L"./Data/Images/sp_frame.png", 20U },
	{ TEXNO::UI01,   L"./Data/Images/sp.png", 20U },
	{ TEXNO::UI02,   L"./Data/Images/arrow.png", 20U },
	{ TEXNO::UI03,   L"./Data/Images/combo.png", 20U },
	{ TEXNO::UI04,   L"./Data/Images/number.png", 20U },
	{ TEXNO::UI05,   L"./Data/Images/scorenum.png", 20U },
	{ TEXNO::UI07,   L"./Data/Images/startUI.png", 20U },
	{ TEXNO::BLACK, L"./Data/Images/color_black.png", 1U },
	{ TEXNO::TEXT, L"./Data/Text/font.png", 256U },
	{ TEXNO::MAP_BACK,      L"./Data/Images/bg.png",    1U },    // マップ背景
	{ TEXNO::MAP_TERRAIN,   L"./Data/Maps/チップ.png", 4096U },
	{ TEXNO::GIMMICK00,   L"./Data/Images/floor.png", 20U },
	{ TEXNO::GIMMICK01,   L"./Data/Images/block.png", 100U },
	{ TEXNO::GIMMICK02,   L"./Data/Images/switch.png", 10U },
	{ TEXNO::FONT02,   L"./Data/Images/pup.png",   1U },
	{ TEXNO::FONT03,   L"./Data/Images/sup.png",   1U },
	{ TEXNO::FONT04,   L"./Data/Images/bget.png",  1U },
	{ TEXNO::FONT07,   L"./Data/Images/font_some.png",  8U },
	{ -1, nullptr }	// 終了フラグ
};

// 2D画像ロードデータリザルト
LoadTexture loadTextureResult[] = {

	{ TEXNO::FONT07,   L"./Data/Images/font_some.png",  8U },//各種フォント
	{ TEXNO::CLEARBG,   L"./Data/Images/clear_bg.png",  8U },//クリア背景
	{ TEXNO::PLAYER02, L"./Data/Images/player_damage.png", 1U },// プレイヤー
	{ TEXNO::UI05,   L"./Data/Images/scorenum.png", 20U },
	{ TEXNO::FONT07,   L"./Data/Images/font_some.png",  8U },
	{ TEXNO::OVERBG,   L"./Data/Images/gameover.png",  8U },
	{ TEXNO::FONT08,   L"./Data/Images/titleclear.png",  8U },
	{ TEXNO::FONT09,   L"./Data/Images/exitclear.png",  8U },


	{ -1, nullptr }	// 終了フラグ


};
//------< マクロ >--------------------------------------------------------------
#define SPRITE_CENTER(texno,left,top,width,height)	{ (texno),(left),(top),(width),(height),(width)/2,(height)/2 }  // 画像の真ん中が中心
#define SPRITE_BOTTOM(texno,left,top,width,height)	{ (texno),(left),(top),(width),(height),(width)/2,(height)   }  // 画像の足元が中心



//------< 待機 >---------------------------------------------------------------------
SpriteData spr_player00 = { TEXNO::PLAYER00, 128 * 0, 128 * 0, 128, 128,64,64 + 20 };
SpriteData spr_player01 = { TEXNO::PLAYER00, 128 * 1, 128 * 0, 128, 128,64,64 + 20 };
SpriteData spr_player02 = { TEXNO::PLAYER00, 128 * 0, 128 * 1, 128, 128,64,64 + 20 };
SpriteData spr_player03 = { TEXNO::PLAYER00, 128 * 1, 128 * 1, 128, 128,64,64 + 20 };
SpriteData spr_player04 = { TEXNO::PLAYER00, 128 * 0, 128 * 2, 128, 128,64,64 + 20 };
//------< 攻撃 >----------------------------------------------------------------------
SpriteData spr_player05 = { TEXNO::PLAYER01, 256 * 0, 128 * 0, 256, 128,64,64 + 20 };
SpriteData spr_player06 = { TEXNO::PLAYER01, 256 * 1, 128 * 0, 256, 128,64,64 + 20 };
SpriteData spr_player07 = { TEXNO::PLAYER01, 256 * 0, 128 * 1, 256, 128,64,64 + 20 };
SpriteData spr_player08 = { TEXNO::PLAYER01, 256 * 1, 128 * 1, 256, 128,64,64 + 20 };
SpriteData spr_player09 = { TEXNO::PLAYER01, 256 * 0, 128 * 2, 256, 128,64,64 + 20 };
SpriteData spr_player10 = { TEXNO::PLAYER01, 256 * 1, 128 * 2, 256, 128,64,64 + 20 };
SpriteData spr_player11 = { TEXNO::PLAYER01, 256 * 0, 128 * 3, 256, 128,64,64 + 20 };
SpriteData spr_player12 = { TEXNO::PLAYER01, 256 * 1, 128 * 3, 256, 128,64,64 + 20 };
SpriteData spr_player13 = { TEXNO::PLAYER01, 256 * 0, 128 * 4, 256, 128,64,64 + 20 };
//------< ダメージ >------------------------------------------------------------------
SpriteData spr_player14 = { TEXNO::PLAYER02, 128 * 0, 128 * 0, 128, 128,64,64 + 20 };
SpriteData spr_player15 = { TEXNO::PLAYER02, 128 * 1, 128 * 0, 128, 128,64,64 + 20 };
SpriteData spr_player16 = { TEXNO::PLAYER02, 128 * 2, 128 * 0, 128, 128,64,64 + 20 };
SpriteData spr_player17 = { TEXNO::PLAYER02, 128 * 3, 128 * 0, 128, 128,64,64 + 20 };
//------< 攻撃2 >---------------------------------------------------------------------
SpriteData spr_player18 = { TEXNO::PLAYER03, 512 * 0, 256 * 0, 512, 256,320,128 + 20 };
SpriteData spr_player19 = { TEXNO::PLAYER03, 512 * 1, 256 * 0, 512, 256,320,128 + 20 };
SpriteData spr_player20 = { TEXNO::PLAYER03, 512 * 0, 256 * 1, 512, 256,320,128 + 20 };
SpriteData spr_player21 = { TEXNO::PLAYER03, 512 * 1, 256 * 1, 512, 256,320,128 + 20 };
SpriteData spr_player22 = { TEXNO::PLAYER03, 512 * 0, 256 * 2, 512, 256,320,128 + 20 };
SpriteData spr_player23 = { TEXNO::PLAYER03, 512 * 1, 256 * 2, 512, 256,320,128 + 20 };
SpriteData spr_player24 = { TEXNO::PLAYER03, 512 * 0, 256 * 3, 512, 256,320,128 + 20 };
SpriteData spr_player25 = { TEXNO::PLAYER03, 512 * 1, 256 * 3, 512, 256,320,128 + 20 };
SpriteData spr_player26 = { TEXNO::PLAYER03, 512 * 0, 256 * 4, 512, 256,320,128 + 20 };


//その位置
SpriteData spr_effect_move00 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 0, 1024 * 0, 1024, 1024);
SpriteData spr_effect_move01 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 1, 1024 * 0, 1024, 1024);
SpriteData spr_effect_move02 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 2, 1024 * 0, 1024, 1024);
SpriteData spr_effect_move03 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 3, 1024 * 0, 1024, 1024);
SpriteData spr_effect_move04 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 0, 1024 * 1, 1024, 1024);
SpriteData spr_effect_move05 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 1, 1024 * 1, 1024, 1024);
SpriteData spr_effect_move06 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 2, 1024 * 1, 1024, 1024);
SpriteData spr_effect_move07 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 3, 1024 * 1, 1024, 1024);
SpriteData spr_effect_move08 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 0, 1024 * 3, 1024, 1024);
SpriteData spr_effect_move09 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 1, 1024 * 3, 1024, 1024);
SpriteData spr_effect_move10 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 2, 1024 * 3, 1024, 1024);
SpriteData spr_effect_move11 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 3, 1024 * 3, 1024, 1024);
SpriteData spr_effect_move12 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 0, 1024 * 4, 1024, 1024);
SpriteData spr_effect_move13 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 1, 1024 * 4, 1024, 1024);
SpriteData spr_effect_move14 = SPRITE_CENTER(TEXNO::EFFECT01, 1024 * 2, 1024 * 4, 1024, 1024);

//その2
SpriteData spr_effect_move15 = SPRITE_CENTER(TEXNO::EFFECT02, 1024 * 0, 1024 * 0, 1024, 1024);
SpriteData spr_effect_move16 = SPRITE_CENTER(TEXNO::EFFECT02, 1024 * 1, 1024 * 0, 1024, 1024);
SpriteData spr_effect_move17 = SPRITE_CENTER(TEXNO::EFFECT02, 1024 * 2, 1024 * 0, 1024, 1024);
SpriteData spr_effect_move18 = SPRITE_CENTER(TEXNO::EFFECT02, 1024 * 3, 1024 * 0, 1024, 1024);
SpriteData spr_effect_move19 = SPRITE_CENTER(TEXNO::EFFECT02, 1024 * 0, 1024 * 1, 1024, 1024);
SpriteData spr_effect_move20 = SPRITE_CENTER(TEXNO::EFFECT02, 1024 * 1, 1024 * 1, 1024, 1024);
SpriteData spr_effect_move21 = SPRITE_CENTER(TEXNO::EFFECT02, 1024 * 2, 1024 * 1, 1024, 1024);


//dashLv0
SpriteData spr_effect_dash00 = {TEXNO::EFFECT03, 512 * 0, 512 * 0, 512, 512,-100,266};
SpriteData spr_effect_dash01 = {TEXNO::EFFECT03, 512 * 1, 512 * 0, 512, 512,-100,266};
SpriteData spr_effect_dash02 = {TEXNO::EFFECT03, 512 * 2, 512 * 0, 512, 512,-100,266};
SpriteData spr_effect_dash03 = {TEXNO::EFFECT03, 512 * 3, 512 * 0, 512, 512,-100,266};
SpriteData spr_effect_dash04 = {TEXNO::EFFECT03, 512 * 4, 512 * 0, 512, 512,-100,266};
SpriteData spr_effect_dash05 = {TEXNO::EFFECT03, 512 * 0, 512 * 1, 512, 512,-100,266};
SpriteData spr_effect_dash06 = {TEXNO::EFFECT03, 512 * 1, 512 * 1, 512, 512,-100,266};
SpriteData spr_effect_dash07 = {TEXNO::EFFECT03, 512 * 2, 512 * 1, 512, 512,-100,266};
SpriteData spr_effect_dash08 = {TEXNO::EFFECT03, 512 * 3, 512 * 1, 512, 512,-100,266};
SpriteData spr_effect_dash09 = {TEXNO::EFFECT03, 512 * 4, 512 * 1, 512, 512,-100,266};
SpriteData spr_effect_dash10 = {TEXNO::EFFECT03, 512 * 0, 512 * 2, 512, 512,-100,266};
SpriteData spr_effect_dash11 = {TEXNO::EFFECT03, 512 * 1, 512 * 2, 512, 512,-100,266};
SpriteData spr_effect_dash12 = {TEXNO::EFFECT03, 512 * 2, 512 * 2, 512, 512,-100,266};
SpriteData spr_effect_dash13 = {TEXNO::EFFECT03, 512 * 3, 512 * 2, 512, 512,-100,266};
SpriteData spr_effect_dash14 = {TEXNO::EFFECT03, 512 * 4, 512 * 2, 512, 512,-100,266};
SpriteData spr_player_title_n00 = SPRITE_CENTER(TEXNO::UI06, 640 * 0, 720 * 0, 640, 720);
SpriteData spr_player_title_n01 = SPRITE_CENTER(TEXNO::UI06, 640 * 1, 720 * 0, 640, 720);
SpriteData spr_player_title_n02 = SPRITE_CENTER(TEXNO::UI06, 640 * 2, 720 * 0, 640, 720);
SpriteData spr_player_title_n03 = SPRITE_CENTER(TEXNO::UI06, 640 * 0, 720 * 1, 640, 720);
SpriteData spr_player_title_n04 = SPRITE_CENTER(TEXNO::UI06, 640 * 1, 720 * 1, 640, 720);
SpriteData spr_player_title_n05 = SPRITE_CENTER(TEXNO::UI06, 640 * 2, 720 * 1, 640, 720);
SpriteData spr_player_title_f00 = SPRITE_CENTER(TEXNO::UI06, 640 * 3, 720 * 0, 640, 720);
SpriteData spr_player_title_f01 = SPRITE_CENTER(TEXNO::UI06, 640 * 4, 720 * 0, 640, 720);
SpriteData spr_player_title_f02 = SPRITE_CENTER(TEXNO::UI06, 640 * 5, 720 * 0, 640, 720);
SpriteData spr_player_title_f03 = SPRITE_CENTER(TEXNO::UI06, 640 * 3, 720 * 1, 640, 720);
SpriteData spr_player_title_f04 = SPRITE_CENTER(TEXNO::UI06, 640 * 4, 720 * 1, 640, 720);
SpriteData spr_player_title_f05 = SPRITE_CENTER(TEXNO::UI06, 640 * 5, 720 * 1, 640, 720);
SpriteData spr_player_title_ff00 = SPRITE_CENTER(TEXNO::UI06, 640 * 6, 720 * 0, 640, 720);
SpriteData spr_player_title_ff01 = SPRITE_CENTER(TEXNO::UI06, 640 * 7, 720 * 0, 640, 720);
SpriteData spr_player_title_ff02 = SPRITE_CENTER(TEXNO::UI06, 640 * 8, 720 * 0, 640, 720);
SpriteData spr_player_title_ff03 = SPRITE_CENTER(TEXNO::UI06, 640 * 6, 720 * 1, 640, 720);
SpriteData spr_player_title_ff04 = SPRITE_CENTER(TEXNO::UI06, 640 * 7, 720 * 1, 640, 720);
SpriteData spr_player_title_ff05 = SPRITE_CENTER(TEXNO::UI06, 640 * 8, 720 * 1, 640, 720);
SpriteData spr_effect_spawn00 = SPRITE_CENTER(TEXNO::EFFECT04, 1024 * 0, 1024 * 0, 1024, 1024);
SpriteData spr_effect_spawn01 = SPRITE_CENTER(TEXNO::EFFECT04, 1024 * 1, 1024 * 0, 1024, 1024);
SpriteData spr_effect_spawn02 = SPRITE_CENTER(TEXNO::EFFECT04, 1024 * 2, 1024 * 0, 1024, 1024);
SpriteData spr_effect_spawn03 = SPRITE_CENTER(TEXNO::EFFECT04, 1024 * 3, 1024 * 0, 1024, 1024);
SpriteData spr_effect_spawn04 = SPRITE_CENTER(TEXNO::EFFECT04, 1024 * 0, 1024 * 1, 1024, 1024);
SpriteData spr_effect_spawn05 = SPRITE_CENTER(TEXNO::EFFECT04, 1024 * 1, 1024 * 1, 1024, 1024);
SpriteData spr_effect_spawn06 = SPRITE_CENTER(TEXNO::EFFECT04, 1024 * 2, 1024 * 1, 1024, 1024);
SpriteData spr_effect_spawn07 = SPRITE_CENTER(TEXNO::EFFECT04, 1024 * 3, 1024 * 1, 1024, 1024);
SpriteData spr_effect_spawn08 = SPRITE_CENTER(TEXNO::EFFECT04, 1024 * 0, 1024 * 3, 1024, 1024);
SpriteData spr_effect_spawn09 = SPRITE_CENTER(TEXNO::EFFECT04, 1024 * 1, 1024 * 3, 1024, 1024);
SpriteData spr_title = SPRITE_CENTER(TEXNO::TITLE, 0, 0, 1280, 720);
SpriteData spr_ui03 = SPRITE_CENTER(UI03, 0, 0, 512, 512);
SpriteData spr_ui02 = SPRITE_CENTER(UI02, 0, 0, 514, 514);
SpriteData spr_number00 = SPRITE_CENTER(TEXNO::UI04, 128 * 0, 128 * 0, 128, 128);
SpriteData spr_number01 = SPRITE_CENTER(TEXNO::UI04, 128 * 1, 128 * 0, 128, 128);
SpriteData spr_number02 = SPRITE_CENTER(TEXNO::UI04, 128 * 2, 128 * 0, 128, 128);
SpriteData spr_number03 = SPRITE_CENTER(TEXNO::UI04, 128 * 3, 128 * 0, 128, 128);
SpriteData spr_number04 = SPRITE_CENTER(TEXNO::UI04, 128 * 0, 128 * 1, 128, 128);
SpriteData spr_number05 = SPRITE_CENTER(TEXNO::UI04, 128 * 1, 128 * 1, 128, 128);
SpriteData spr_number06 = SPRITE_CENTER(TEXNO::UI04, 128 * 2, 128 * 1, 128, 128);
SpriteData spr_number07 = SPRITE_CENTER(TEXNO::UI04, 128 * 3, 128 * 1, 128, 128);
SpriteData spr_number08 = SPRITE_CENTER(TEXNO::UI04, 128 * 0, 128 * 2, 128, 128);
SpriteData spr_number09 = SPRITE_CENTER(TEXNO::UI04, 128 * 1, 128 * 2, 128, 128);
SpriteData spr_sc_number00 = SPRITE_CENTER(TEXNO::UI05, 1024 * 0, 1024 * 0, 1024, 1024);
SpriteData spr_sc_number01 = SPRITE_CENTER(TEXNO::UI05, 1024 * 1, 1024 * 0, 1024, 1024);
SpriteData spr_sc_number02 = SPRITE_CENTER(TEXNO::UI05, 1024 * 2, 1024 * 0, 1024, 1024);
SpriteData spr_sc_number03 = SPRITE_CENTER(TEXNO::UI05, 1024 * 3, 1024 * 0, 1024, 1024);
SpriteData spr_sc_number04 = SPRITE_CENTER(TEXNO::UI05, 1024 * 0, 1024 * 1, 1024, 1024);
SpriteData spr_sc_number05 = SPRITE_CENTER(TEXNO::UI05, 1024 * 1, 1024 * 1, 1024, 1024);
SpriteData spr_sc_number06 = SPRITE_CENTER(TEXNO::UI05, 1024 * 2, 1024 * 1, 1024, 1024);
SpriteData spr_sc_number07 = SPRITE_CENTER(TEXNO::UI05, 1024 * 3, 1024 * 1, 1024, 1024);
SpriteData spr_sc_number08 = SPRITE_CENTER(TEXNO::UI05, 1024 * 0, 1024 * 2, 1024, 1024);
SpriteData spr_sc_number09 = SPRITE_CENTER(TEXNO::UI05, 1024 * 1, 1024 * 2, 1024, 1024);











//エフェクト
SpriteData spr_effect_enemy_death00		= SPRITE_CENTER(TEXNO::EFFECT00, 64 * 0, 64 * 0, 64, 64);
SpriteData spr_effect_enemy_death01		= SPRITE_CENTER(TEXNO::EFFECT00, 64 * 0, 64 * 1, 64, 64);
SpriteData spr_effect_enemy_death02		= SPRITE_CENTER(TEXNO::EFFECT00, 64 * 0, 64 * 2, 64, 64);
SpriteData spr_effect_enemy_death03		= SPRITE_CENTER(TEXNO::EFFECT00, 64 * 1, 64 * 0, 64, 64);
SpriteData spr_effect_enemy_death04		= SPRITE_CENTER(TEXNO::EFFECT00, 64 * 1, 64 * 1, 64, 64);

SpriteData spr_arrow					= { TEXNO::UI02, 0, 0, 1024, 512,200,256 };

SpriteData spr_ui00						= SPRITE_CENTER(UI00, 0, 0, 1838, 277);
SpriteData spr_ui01						= SPRITE_CENTER(UI01, 0, 0, 417, 175);


SpriteData spr_enemy_move00				= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 0, 64 * 0, 128, 64);
SpriteData spr_enemy_move01				= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 1, 64 * 0, 128, 64);
SpriteData spr_enemy_move02				= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 2, 64 * 0, 128, 64);
SpriteData spr_enemy_move03				= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 3, 64 * 0, 128, 64);
SpriteData spr_enemy_shot00				= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 0, 64 * 1, 128, 64);
SpriteData spr_enemy_shot01				= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 1, 64 * 1, 128, 64);
SpriteData spr_enemy_shot02				= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 2, 64 * 1, 128, 64);
SpriteData spr_enemy_shot03				= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 3, 64 * 1, 128, 64);
SpriteData spr_enemy_shot04				= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 0, 64 * 4, 128, 64);
SpriteData spr_enemy_shot05				= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 1, 64 * 4, 128, 64);
SpriteData spr_enemy_shot06				= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 2, 64 * 4, 128, 64);
SpriteData spr_enemy_shot07			    = SPRITE_CENTER(TEXNO::ENEMY00, 128 * 3, 64 * 4, 128, 64);

SpriteData spr_enemy_dash00				= SPRITE_CENTER(TEXNO::ENEMY03, 128 * 0, 64 * 0, 128, 64);
SpriteData spr_enemy_dash01				= SPRITE_CENTER(TEXNO::ENEMY03, 128 * 1, 64 * 0, 128, 64);
SpriteData spr_enemy_dash02				= SPRITE_CENTER(TEXNO::ENEMY03, 128 * 2, 64 * 0, 128, 64);
SpriteData spr_enemy_dash03				= SPRITE_CENTER(TEXNO::ENEMY03, 128 * 3, 64 * 0, 128, 64);
SpriteData spr_enemy_float00			= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 0, 64 * 3, 128, 64);
SpriteData spr_enemy_float01			= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 1, 64 * 3, 128, 64);
SpriteData spr_enemy_float02			= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 2, 64 * 3, 128, 64);
SpriteData spr_enemy_float03			= SPRITE_CENTER(TEXNO::ENEMY00, 128 * 3, 64 * 3, 128, 64);

SpriteData spr_enemy_shot_begin00		= SPRITE_CENTER(TEXNO::ENEMY01, 67 * 0, 64 * 0, 67, 64);
SpriteData spr_enemy_shot_begin01		= SPRITE_CENTER(TEXNO::ENEMY01, 67 * 1, 64 * 0, 67, 64);
SpriteData spr_enemy_shot_begin02		= SPRITE_CENTER(TEXNO::ENEMY01, 67 * 2, 64 * 0, 67, 64);
SpriteData spr_enemy_shot_begin03		= SPRITE_CENTER(TEXNO::ENEMY01, 67 * 0, 64 * 1, 67, 64);
SpriteData spr_enemy_shot_begin04		= SPRITE_CENTER(TEXNO::ENEMY01, 67 * 1, 64 * 1, 67, 64);
SpriteData spr_enemy_shot_begin05		= SPRITE_CENTER(TEXNO::ENEMY01, 67 * 2, 64 * 1, 67, 64);
SpriteData spr_enemy_shot_begin06		= SPRITE_CENTER(TEXNO::ENEMY01, 67 * 0, 64 * 2, 67, 64);
SpriteData spr_enemy_shot_begin07		= SPRITE_CENTER(TEXNO::ENEMY01, 67 * 1, 64 * 2, 67, 64);
SpriteData spr_enemy_shot_begin08		= SPRITE_CENTER(TEXNO::ENEMY01, 67 * 2, 64 * 2, 67, 64);

SpriteData spr_enemy_shot2_begin00		= SPRITE_CENTER(TEXNO::ENEMY02, 77 * 0, 78 * 0, 77, 78);
SpriteData spr_enemy_shot2_begin01		= SPRITE_CENTER(TEXNO::ENEMY02, 77 * 1, 78 * 0, 77, 78);
SpriteData spr_enemy_shot2_begin02		= SPRITE_CENTER(TEXNO::ENEMY02, 77 * 2, 78 * 0, 77, 78);
SpriteData spr_enemy_shot2_begin03		= SPRITE_CENTER(TEXNO::ENEMY02, 77 * 0, 78 * 1, 77, 78);
SpriteData spr_enemy_shot2_begin04		= SPRITE_CENTER(TEXNO::ENEMY02, 77 * 1, 78 * 1, 77, 78);
SpriteData spr_enemy_shot2_begin05		= SPRITE_CENTER(TEXNO::ENEMY02, 77 * 2, 78 * 1, 77, 78);
SpriteData spr_enemy_shot2_begin06		= SPRITE_CENTER(TEXNO::ENEMY02, 77 * 0, 78 * 2, 77, 78);
SpriteData spr_enemy_shot2_begin07		= SPRITE_CENTER(TEXNO::ENEMY02, 77 * 1, 78 * 2, 77, 78);
SpriteData spr_enemy_shot2_begin08		= SPRITE_CENTER(TEXNO::ENEMY02, 77 * 2, 78 * 2, 77, 78);

SpriteData spr_enemy_bullet				= SPRITE_CENTER(TEXNO::ENEMY04, 33 * 0, 20 * 0, 33, 20);


SpriteData spr_enemy_black_hole00		= {TEXNO::ENEMY05, 1028 * 0, 1024 * 0, 1028, 1024,690,512};
SpriteData spr_enemy_black_hole01		= {TEXNO::ENEMY05, 1028 * 1, 1024 * 0, 1028, 1024,690,512};
SpriteData spr_enemy_black_hole02		= {TEXNO::ENEMY05, 1028 * 2, 1024 * 0, 1028, 1024,690,512};
SpriteData spr_enemy_black_hole03		= {TEXNO::ENEMY05, 1028 * 3, 1024 * 0, 1028, 1024,690,512};
SpriteData spr_enemy_black_hole04		= {TEXNO::ENEMY05, 1028 * 0, 1024 * 1, 1028, 1024,690,512};
SpriteData spr_enemy_black_hole05		= {TEXNO::ENEMY05, 1028 * 1, 1024 * 1, 1028, 1024,690,512};
SpriteData spr_enemy_black_hole06		= {TEXNO::ENEMY05, 1028 * 2, 1024 * 1, 1028, 1024,690,512};
SpriteData spr_enemy_black_hole07		= {TEXNO::ENEMY05, 1028 * 3, 1024 * 1, 1028, 1024,690,512};



SpriteData color_black					= SPRITE_CENTER(TEXNO::BLACK, 0, 0, 1, 1);
SpriteData spr_bg						= { TEXNO::MAP_BACK, 0, 0, 1920, 1080,0,0 };
SpriteData spr_titlebg					= { TEXNO::MAP_BACK2, 0, 0, 1920, 1080,0,0 };

SpriteData spr_break_floor				= SPRITE_CENTER(TEXNO::GIMMICK00, 0, 0, 96, 64);
SpriteData spr_block00					= SPRITE_CENTER(TEXNO::GIMMICK01, 512 * 0, 512 * 0, 512, 512);
SpriteData spr_block01					= SPRITE_CENTER(TEXNO::GIMMICK01, 512 * 1, 512 * 0, 512, 512);
SpriteData spr_block02					= SPRITE_CENTER(TEXNO::GIMMICK01, 512 * 2, 512 * 0, 512, 512);
SpriteData spr_block03					= SPRITE_CENTER(TEXNO::GIMMICK01, 512 * 3, 512 * 0, 512, 512);
SpriteData spr_block04					= SPRITE_CENTER(TEXNO::GIMMICK01, 512 * 0, 512 * 1, 512, 512);
SpriteData spr_block05					= SPRITE_CENTER(TEXNO::GIMMICK01, 512 * 1, 512 * 1, 512, 512);
SpriteData spr_block06					= SPRITE_CENTER(TEXNO::GIMMICK01, 512 * 2, 512 * 1, 512, 512);
SpriteData spr_block07					= SPRITE_CENTER(TEXNO::GIMMICK01, 512 * 3, 512 * 1, 512, 512);
SpriteData spr_block08					= SPRITE_CENTER(TEXNO::GIMMICK01, 512 * 0, 512 * 2, 512, 512);
SpriteData spr_block09					= SPRITE_CENTER(TEXNO::GIMMICK01, 512 * 1, 512 * 2, 512, 512);
SpriteData spr_block10					= SPRITE_CENTER(TEXNO::GIMMICK01, 512 * 2, 512 * 2, 512, 512);
SpriteData spr_block11					= SPRITE_CENTER(TEXNO::GIMMICK01, 512 * 3, 512 * 2, 512, 512);


SpriteData spr_help_rabitt00			= SPRITE_CENTER(TEXNO::ITEM01, 33 * 0, 39 * 0, 33, 39);
SpriteData spr_help_rabitt01			= SPRITE_CENTER(TEXNO::ITEM01, 33 * 0, 39 * 1, 33, 39);
SpriteData spr_help_rabitt02			= SPRITE_CENTER(TEXNO::ITEM01, 33 * 0, 39 * 2, 33, 39);
SpriteData spr_help_rabitt03			= SPRITE_CENTER(TEXNO::ITEM01, 33 * 0, 39 * 3, 33, 39);
SpriteData spr_help_rabitt04			= SPRITE_CENTER(TEXNO::ITEM01, 33 * 0, 39 * 4, 33, 39);
SpriteData spr_help_rabitt05			= SPRITE_CENTER(TEXNO::ITEM01, 33 * 0, 39 * 5, 33, 39);
SpriteData spr_help_rabitt06			= SPRITE_CENTER(TEXNO::ITEM01, 33 * 0, 39 * 6, 33, 39);
SpriteData spr_item_bariier				= SPRITE_CENTER(TEXNO::ITEM00, 255 * 0, 255 * 0, 255, 255);
SpriteData spr_item_power				= SPRITE_CENTER(TEXNO::ITEM00, 255 * 1, 255 * 0, 255, 255);
SpriteData spr_item_1up					= SPRITE_CENTER(TEXNO::ITEM00, 255 * 2, 255 * 0, 255, 255);
SpriteData spr_particle00				= SPRITE_CENTER(TEXNO::OTHER00, 32 * 0, 32 * 0, 32, 32);
SpriteData spr_particle01				= SPRITE_CENTER(TEXNO::OTHER00, 32 * 1, 32 * 0, 32, 32);
SpriteData spr_particle02				= SPRITE_CENTER(TEXNO::OTHER00, 32 * 2, 32 * 0, 32, 32);
SpriteData spr_particle03				= SPRITE_CENTER(TEXNO::OTHER00, 32 * 3, 32 * 0, 32, 32);
SpriteData spr_spark00					= SPRITE_CENTER(TEXNO::OTHER01, 256 * 0, 720 * 0, 256, 720);
SpriteData spr_spark01					= SPRITE_CENTER(TEXNO::OTHER01, 256 * 1, 720 * 0, 256, 720);
SpriteData spr_spark02					= SPRITE_CENTER(TEXNO::OTHER01, 256 * 2, 720 * 0, 256, 720);
SpriteData spr_spark03					= SPRITE_CENTER(TEXNO::OTHER01, 256 * 3, 720 * 0, 256, 720);
SpriteData spr_spark04					= SPRITE_CENTER(TEXNO::OTHER01, 256 * 4, 720 * 0, 256, 720);
SpriteData spr_spark05					= SPRITE_CENTER(TEXNO::OTHER01, 256 * 5, 720 * 0, 256, 720);
SpriteData spr_spark06					= SPRITE_CENTER(TEXNO::OTHER01, 256 * 6, 720 * 1, 256, 720);
SpriteData spr_spark07					= SPRITE_CENTER(TEXNO::OTHER01, 256 * 7, 720 * 1, 256, 720);
SpriteData spr_spark08					= SPRITE_CENTER(TEXNO::OTHER01, 256 * 8, 720 * 1, 256, 720);
SpriteData spr_spark09					= SPRITE_CENTER(TEXNO::OTHER01, 256 * 9, 720 * 1, 256, 720);
SpriteData spr_spark10					= SPRITE_CENTER(TEXNO::OTHER01, 256 *10, 720 * 1, 256, 720);
SpriteData spr_spark11					= SPRITE_CENTER(TEXNO::OTHER01, 256 *11, 720 * 1, 256, 720);

//起爆スイッチ
SpriteData spr_switch00 = SPRITE_CENTER(TEXNO::GIMMICK02, 256 * 0, 255 * 0, 256, 255);
SpriteData spr_switch01 = SPRITE_CENTER(TEXNO::GIMMICK02, 256 * 1, 255 * 0, 256, 255);
SpriteData spr_switch02 = SPRITE_CENTER(TEXNO::GIMMICK02, 256 * 2, 255 * 0, 256, 255);
SpriteData spr_switch03 = SPRITE_CENTER(TEXNO::GIMMICK02, 256 * 0, 255 * 1, 256, 255);
SpriteData spr_switch04 = SPRITE_CENTER(TEXNO::GIMMICK02, 256 * 1, 255 * 1, 256, 255);
SpriteData spr_switch05 = SPRITE_CENTER(TEXNO::GIMMICK02, 256 * 2, 255 * 1, 256, 255);

//パワーアップ
SpriteData spr_power_up_font00 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 0, 1024 * 0, 1024, 1024);
SpriteData spr_power_up_font01 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 1, 1024 * 0, 1024, 1024);
SpriteData spr_power_up_font02 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 2, 1024 * 0, 1024, 1024);
SpriteData spr_power_up_font03 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 3, 1024 * 0, 1024, 1024);
SpriteData spr_power_up_font04 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 0, 1024 * 1, 1024, 1024);
SpriteData spr_power_up_font05 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 1, 1024 * 1, 1024, 1024);
SpriteData spr_power_up_font06 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 2, 1024 * 1, 1024, 1024);
SpriteData spr_power_up_font07 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 3, 1024 * 1, 1024, 1024);
SpriteData spr_power_up_font08 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 0, 1024 * 2, 1024, 1024);
SpriteData spr_power_up_font09 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 1, 1024 * 2, 1024, 1024);
SpriteData spr_power_up_font10 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 2, 1024 * 2, 1024, 1024);
SpriteData spr_power_up_font11 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 3, 1024 * 2, 1024, 1024);
SpriteData spr_power_up_font12 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 0, 1024 * 3, 1024, 1024);
SpriteData spr_power_up_font13 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 1, 1024 * 3, 1024, 1024);
SpriteData spr_power_up_font14 = SPRITE_CENTER(TEXNO::FONT02, 1024 * 2, 1024 * 3, 1024, 1024);

//スピードアップ
SpriteData spr_speed_up_font00 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 0, 1024 * 0, 1024, 1024);
SpriteData spr_speed_up_font01 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 1, 1024 * 0, 1024, 1024);
SpriteData spr_speed_up_font02 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 2, 1024 * 0, 1024, 1024);
SpriteData spr_speed_up_font03 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 3, 1024 * 0, 1024, 1024);
SpriteData spr_speed_up_font04 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 0, 1024 * 1, 1024, 1024);
SpriteData spr_speed_up_font05 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 1, 1024 * 1, 1024, 1024);
SpriteData spr_speed_up_font06 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 2, 1024 * 1, 1024, 1024);
SpriteData spr_speed_up_font07 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 3, 1024 * 1, 1024, 1024);
SpriteData spr_speed_up_font08 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 0, 1024 * 2, 1024, 1024);
SpriteData spr_speed_up_font09 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 1, 1024 * 2, 1024, 1024);
SpriteData spr_speed_up_font10 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 2, 1024 * 2, 1024, 1024);
SpriteData spr_speed_up_font11 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 3, 1024 * 2, 1024, 1024);
SpriteData spr_speed_up_font12 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 0, 1024 * 3, 1024, 1024);
SpriteData spr_speed_up_font13 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 1, 1024 * 3, 1024, 1024);
SpriteData spr_speed_up_font14 = SPRITE_CENTER(TEXNO::FONT03, 1024 * 2, 1024 * 3, 1024, 1024);

//バリアゲット
SpriteData spr_barrier_font00 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 0, 1024 * 0, 1024, 1024);
SpriteData spr_barrier_font01 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 1, 1024 * 0, 1024, 1024);
SpriteData spr_barrier_font02 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 2, 1024 * 0, 1024, 1024);
SpriteData spr_barrier_font03 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 3, 1024 * 0, 1024, 1024);
SpriteData spr_barrier_font04 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 0, 1024 * 1, 1024, 1024);
SpriteData spr_barrier_font05 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 1, 1024 * 1, 1024, 1024);
SpriteData spr_barrier_font06 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 2, 1024 * 1, 1024, 1024);
SpriteData spr_barrier_font07 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 3, 1024 * 1, 1024, 1024);
SpriteData spr_barrier_font08 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 0, 1024 * 2, 1024, 1024);
SpriteData spr_barrier_font09 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 1, 1024 * 2, 1024, 1024);
SpriteData spr_barrier_font10 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 2, 1024 * 2, 1024, 1024);
SpriteData spr_barrier_font11 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 3, 1024 * 2, 1024, 1024);
SpriteData spr_barrier_font12 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 0, 1024 * 3, 1024, 1024);
SpriteData spr_barrier_font13 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 1, 1024 * 3, 1024, 1024);
SpriteData spr_barrier_font14 = SPRITE_CENTER(TEXNO::FONT04, 1024 * 2, 1024 * 3, 1024, 1024);

//救助待ち
SpriteData spr_help_rabbit00 = SPRITE_CENTER(TEXNO::ITEM01, 32 * 0, 39 * 0, 32, 39);
SpriteData spr_help_rabbit01 = SPRITE_CENTER(TEXNO::ITEM01, 32 * 0, 39 * 1, 32, 39);
SpriteData spr_help_rabbit02 = SPRITE_CENTER(TEXNO::ITEM01, 32 * 0, 39 * 2, 32, 39);
SpriteData spr_help_rabbit03 = SPRITE_CENTER(TEXNO::ITEM01, 32 * 0, 39 * 3, 32, 39);
SpriteData spr_help_rabbit04 = SPRITE_CENTER(TEXNO::ITEM01, 32 * 0, 39 * 4, 32, 39);
SpriteData spr_help_rabbit05 = SPRITE_CENTER(TEXNO::ITEM01, 32 * 0, 39 * 5, 32, 39);
SpriteData spr_help_rabbit06 = SPRITE_CENTER(TEXNO::ITEM01, 32 * 0, 39 * 6, 32, 39);

//バリアエフェクト
SpriteData spr_barrier00 = SPRITE_CENTER(TEXNO::EFFECT05, 128 * 0, 128 * 0, 128, 128);
SpriteData spr_barrier01 = SPRITE_CENTER(TEXNO::EFFECT05, 128 * 1, 128 * 0, 128, 128);
SpriteData spr_barrier02 = SPRITE_CENTER(TEXNO::EFFECT05, 128 * 2, 128 * 0, 128, 128);
SpriteData spr_barrier03 = SPRITE_CENTER(TEXNO::EFFECT05, 128 * 3, 128 * 0, 128, 128);
SpriteData spr_barrier04 = SPRITE_CENTER(TEXNO::EFFECT05, 128 * 4, 128 * 0, 128, 128);
SpriteData spr_barrier05 = SPRITE_CENTER(TEXNO::EFFECT05, 128 * 5, 128 * 0, 128, 128);




//
SpriteData spr_anime_start00 = SPRITE_CENTER(TEXNO::FONT05, 2048 * 0, 2048 * 0, 2048, 2048);
SpriteData spr_anime_start01 = SPRITE_CENTER(TEXNO::FONT05, 2048 * 1, 2048 * 0, 2048, 2048);
SpriteData spr_anime_start02 = SPRITE_CENTER(TEXNO::FONT05, 2048 * 2, 2048 * 0, 2048, 2048);
SpriteData spr_anime_start03 = SPRITE_CENTER(TEXNO::FONT05, 2048 * 3, 2048 * 0, 2048, 2048);
SpriteData spr_anime_start04 = SPRITE_CENTER(TEXNO::FONT05, 2048 * 0, 2048 * 1, 2048, 2048);
SpriteData spr_anime_start05 = SPRITE_CENTER(TEXNO::FONT05, 2048 * 1, 2048 * 1, 2048, 2048);
SpriteData spr_anime_start06 = SPRITE_CENTER(TEXNO::FONT05, 2048 * 2, 2048 * 1, 2048, 2048);
SpriteData spr_anime_start07 = SPRITE_CENTER(TEXNO::FONT05, 2048 * 3, 2048 * 1, 2048, 2048);
SpriteData spr_anime_start08 = SPRITE_CENTER(TEXNO::FONT05, 2048 * 0, 2048 * 2, 2048, 2048);
SpriteData spr_anime_start09 = SPRITE_CENTER(TEXNO::FONT05, 2048 * 1, 2048 * 2, 2048, 2048);
SpriteData spr_anime_start10 = SPRITE_CENTER(TEXNO::FONT05, 2048 * 1, 2048 * 3, 2048, 2048);

//クリアBG
SpriteData spr_clear_bg = SPRITE_CENTER(TEXNO::CLEARBG, 1280 * 0, 720 * 0, 1280, 720);
//ネクストステージ
SpriteData spr_font00  = SPRITE_CENTER(TEXNO::FONT07, 2048 * 0, 2048 * 0, 2048, 2048);
//イグジット
SpriteData spr_font01  = SPRITE_CENTER(TEXNO::FONT07, 2048 * 1, 2048 * 0, 2048, 2048);
//スタート
SpriteData spr_font02 = SPRITE_CENTER(TEXNO::FONT07, 2048 * 2, 2048 * 0, 1024, 1024);
//リトライ
SpriteData spr_font03 = SPRITE_CENTER(TEXNO::FONT07, 2048 * 2+1024, 2048 * 0, 2048, 2048);

//クリア
SpriteData spr_font04 = SPRITE_CENTER(TEXNO::FONT07, 2048 * 0, 2048 * 1, 2048, 2048);
//タイトル															 
SpriteData spr_font05 = SPRITE_CENTER(TEXNO::FONT07, 2048 * 1, 2048 * 1, 2048, 2048);
//スコア															  
SpriteData spr_font06 = SPRITE_CENTER(TEXNO::FONT07, 2048 * 2, 2048 * 1, 1024, 1024);
//ゲームオーバー
SpriteData spr_font07 = SPRITE_CENTER(TEXNO::FONT07, 2048 * 2 + 1024, 2048 * 1, 2048, 2048);


//ゲームオーバー
SpriteData spr_gameover = SPRITE_CENTER(TEXNO::OVERBG,0, 0, 1280, 720);

SpriteData spr_font05_clear = SPRITE_CENTER(TEXNO::FONT08, 0, 0, 2048, 2048);
SpriteData spr_font01_clear = SPRITE_CENTER(TEXNO::FONT09, 0, 0, 2048, 2048);


SpriteData spr_start_ui00 = SPRITE_CENTER(TEXNO::UI07, 512 * 0, 512 * 0, 512, 512);
SpriteData spr_start_ui01 = SPRITE_CENTER(TEXNO::UI07, 512 * 1, 512 * 0, 512, 512);
SpriteData spr_start_ui02 = SPRITE_CENTER(TEXNO::UI07, 512 * 2, 512 * 0, 512, 512);
SpriteData spr_start_ui03 = SPRITE_CENTER(TEXNO::UI07, 512 * 3, 512 * 0, 512, 512);
SpriteData spr_start_ui04 = SPRITE_CENTER(TEXNO::UI07, 512 * 0, 512 * 1, 512, 512);
SpriteData spr_start_ui05 = SPRITE_CENTER(TEXNO::UI07, 512 * 1, 512 * 1, 512, 512);
SpriteData spr_start_ui06 = SPRITE_CENTER(TEXNO::UI07, 512 * 2, 512 * 1, 512, 512);
SpriteData spr_start_ui07 = SPRITE_CENTER(TEXNO::UI07, 512 * 3, 512 * 1, 512, 512);
SpriteData spr_start_ui08 = SPRITE_CENTER(TEXNO::UI07, 512 * 0, 512 * 2, 512, 512);
SpriteData spr_start_ui09 = SPRITE_CENTER(TEXNO::UI07, 512 * 1, 512 * 2, 512, 512);


SpriteData spr_demo00 = SPRITE_CENTER(TEXNO::DEMO, 1920 * 0, 1080 * 0, 1920, 1080);
SpriteData spr_demo01 = SPRITE_CENTER(TEXNO::DEMO, 1920 * 1, 1080 * 0, 1920, 1080);
SpriteData spr_demo02 = SPRITE_CENTER(TEXNO::DEMO, 1920 * 2, 1080 * 0, 1920, 1080);
SpriteData spr_demo03 = SPRITE_CENTER(TEXNO::DEMO, 1920 * 3, 1080 * 0, 1920, 1080);
SpriteData spr_demo04 = SPRITE_CENTER(TEXNO::DEMO, 1920 * 0, 1080 * 1, 1920, 1080);
SpriteData spr_demo05 = SPRITE_CENTER(TEXNO::DEMO, 1920 * 1, 1080 * 1, 1920, 1080);
SpriteData spr_demo06 = SPRITE_CENTER(TEXNO::DEMO, 1920 * 2, 1080 * 1, 1920, 1080);
SpriteData spr_demo07 = SPRITE_CENTER(TEXNO::DEMO, 1920 * 3, 1080 * 1, 1920, 1080);
