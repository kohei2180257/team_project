//******************************************************************************
//
//
//      OBJ2Dクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;

//--------------------------------
//  コンストラクタ
//--------------------------------
OBJ2D::OBJ2D()
{
	clear();

	scale = VECTOR2(1, 1);          // 初期化忘れが無いように（スケールゼロなら何も表示されない）
	color = VECTOR4(1, 1, 1, 1);    // 初期化忘れが無いように（不透明度ゼロなら何も表示されない）
}

//--------------------------------
//  メンバ変数のクリア
//--------------------------------
void OBJ2D::clear()
{
	data = nullptr;
	SecureZeroMemory(&anime, sizeof(anime));
	position = {};
	scale = VECTOR2(1, 1);
	center = {};
	angle = 0.0f;
	color = VECTOR4(1, 1, 1, 1);
	size = {};
	flg = false;
	mover = 0;
	sway = 10;
	eraser = 0;
	slow = false;
	velocity = VECTOR2(0,0);
	speed = {};
	state = 0;
	act_state = 0;
	timer = 0;
	param = 0;
	cnt = 0;
	index = 0;
	wait = 0;
	SecureZeroMemory(iWork, sizeof(iWork));
	SecureZeroMemory(fWork, sizeof(fWork));
	type = 0;
	mutekiTimer = 0;
	damageTimer = 0;
	judgeFlag = false;
	moveFlg = false;
	parent = nullptr;
	comboCnt = 0;
	dashCnt = 0;
}

//--------------------------------
//  移動
//--------------------------------
// 更新処理
void OBJ2D::update()
{
	if (mover) mover(this);



	// 消去チェック
	if (eraser) {
		if (eraser(this)) clear();
	}
}


//--------------------------------
//  描画
//--------------------------------
void OBJ2D::draw()
{
	if (SCENE_GAME->stop_fg) { this->GameSway(this); }


	if (data)// OBJ2Dのdataメンバにスプライトデータがあれば
	{
		data->draw(position- pBGManager->getScrollPos(), scale, angle, color);  // dataのdrawメソッドでスプライトを描画する
	}
}
//--------------------------------
//  描画
//--------------------------------
void OBJ2D::draw_not_scroll()
{
	//if (SCENE_GAME->stop_fg) { this->GameSway(this); }


	if (data)// OBJ2Dのdataメンバにスプライトデータがあれば
	{
		data->draw(position, scale, angle, color);  // dataのdrawメソッドでスプライトを描画する
	}
}

//--------------------------------
//  アニメーション更新
//--------------------------------
//  戻り値：true  アニメが先頭に戻る瞬間
//        :false それ以外
//--------------------------------
bool OBJ2D::animeUpdate(AnimeData* animeData)
{
	if (animeData == nullptr) assert(!"animeUpdate関数でanimeDataがnullptr");

	if (anime.pPrev != animeData)           // アニメデータが切り替わったとき
	{
		anime.pPrev = animeData;
		anime.patNum = 0;	                // 先頭から再生
		anime.frame = 0;
	}

	animeData += anime.patNum;
	data = animeData->data;                 // 現在のパターン番号に該当する画像を設定

	anime.frame++;
	if (anime.frame >= animeData->frame)    // 設定フレーム数表示したら
	{
		anime.frame = 0;
		anime.patNum++;                     // 次のパターンへ
		if ((animeData + 1)->frame < 0)     // 終了コードのとき
		{
			anime.patNum = 0;               // 先頭へ戻る
			return true;
		}
	}

	return false;
}

//******************************************************************************
//
//
//      OBJ2DManagerクラス
//
//
//******************************************************************************

//--------------------------------
//  初期化
//--------------------------------
// 初期設定
void OBJ2DManager::init()
{
	obj_w.resize(getSize());
	for (auto& item : *this) item.clear();
}

//--------------------------------
//  配列へ追加
//--------------------------------
OBJ2D* OBJ2DManager::searchSet(MOVER mover, VECTOR2 pos)
{
	for (auto& item : *this) {
		if (item.mover) continue;
		item.clear();
		item.mover = mover;
		item.position = pos;
		return &item;
	}
	return nullptr;
}


//--------------------------------
//  更新
//--------------------------------
// 更新処理
void OBJ2DManager::update()
{
	for (auto& item : *this) item.update();
}



//--------------------------------
//  描画
//--------------------------------
void OBJ2DManager::draw()
{
	auto it = end();
	while (it != begin()) {
		it--;
		it->draw();
	};
}
void OBJ2DManager::draw_not_scroll()
{
	auto it = end();
	while (it != begin()) {
		it--;
		it->draw_not_scroll();
	};
}
//画面揺れ
void OBJ2D::GameSway(OBJ2D*obj)
{
	sway *= -1;
	obj->position.x += sway*2;

}


#define AIR_REDIST 0.95f
#define ADDFORCE_CNT 30




//漂い関数(数式ベース->力学ベースに変更）
void Floating_Move(OBJ2D*obj)
{

	if (obj->moveFlg)//移動中は漂わない
		return;


	//ランダムな方向に力を加える
	if (obj->timer==0	||	obj->timer % ADDFORCE_CNT == 0) {

		obj->speed.x += 10 * (rand() % 1000 - 500)*0.0001f;//x成分の振れ幅と方向
		obj->speed.y += 10 * (rand() % 1000 - 500)*0.0001f;//y成分の振れ幅と方向
	}

	//復元力を毎フレーム持たせる
	obj->speed.x -= (obj->position.x - obj->center.x) * 0.0095f;
	obj->speed.y -= (obj->position.y - obj->center.y) * 0.0095f;

	//空気抵抗を与える
	obj->speed *= AIR_REDIST;

	//位置を更新(重さによって揺れ方が異なる)
	if(obj->mover!=player_move)
	obj->position += (obj->speed/obj->wait);

}