#include "all.h"

#include <string>
#include <bitset>
#include <DirectXMath.h>


#define TITLE_PLAYER_VELOCITY		1.0f
#define TITLE_PLAYER_SPEED			40.0f
#define TITLE_PLAYER_CNT			100
#define TITLE_PLAYER_R_STATE_NUM	3  
#define TITLE_PLAYER_INIT_POS		750  
#define EXIT_FONT_POSX				1230
#define EXIT_FONT_POSY				900
#define EXIT_FONT_SCALE				0.34f
#define TITLE_LOGO_POSX			    1200
#define TITLE_LOGO_SCALE			1.5f




using namespace GameLib;

namespace
{

	AnimeData animePlayerN[] = {
		{ &spr_player00, 5 },
		{ &spr_player01, 5 },
		{ &spr_player02, 5 },
		{ &spr_player03, 5 },
		{ &spr_player04, 5 },

		{ nullptr, -1 },// 終了フラグ

	};

	AnimeData animePlayerA[] = {
		{ &spr_player05, 2 },
		{ &spr_player06, 2 },
		{ &spr_player07, 2 },
		{ &spr_player08, 3 },
		{ &spr_player09, 3 },
		{ &spr_player10, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ nullptr, -1 },// 終了フラグ

	};


	AnimeData animePlayerTitleN[] = {
		{ &spr_player_title_n00, 6 },
		{ &spr_player_title_n01, 6 },
		{ &spr_player_title_n02, 6 },
		{ &spr_player_title_n03, 6 },
		{ &spr_player_title_n04, 6 },
		{ &spr_player_title_n05, 6 },
		
		{ nullptr, -1 },// 終了フラグ

	};

	AnimeData animePlayerTitleF[] = {
		{ &spr_player_title_f00, 6 },
		{ &spr_player_title_f01, 6 },
		{ &spr_player_title_f02, 6 },
		{ &spr_player_title_f03, 6 },
		{ &spr_player_title_f04, 6 },
		{ &spr_player_title_f05, 6 },

		{ nullptr, -1 },// 終了フラグ

	};

	
	AnimeData animePlayerTitleF2[] = {
		{ &spr_player_title_ff00, 6 },
		{ &spr_player_title_ff01, 6 },
		{ &spr_player_title_ff02, 6 },
		{ &spr_player_title_ff03, 6 },
		{ &spr_player_title_ff04, 6 },
		{ &spr_player_title_ff05, 6 },

		{ nullptr, -1 },// 終了フラグ

	};


	AnimeData animeStart[] = {
		{ &spr_anime_start00, 6 },
		{ &spr_anime_start01, 6 },
		{ &spr_anime_start02, 6 },
		{ &spr_anime_start03, 6 },
		{ &spr_anime_start04, 6 },
		{ &spr_anime_start05, 6 },
		{ &spr_anime_start06, 6 },
		{ &spr_anime_start07, 6 },
		{ &spr_anime_start08, 6 },
		{ &spr_anime_start09, 6 },
		{ &spr_anime_start10, 60 },

		{ nullptr, -1 },// 終了フラグ

	};

	
}


void title_anime_player_large(OBJ2D*obj)
{
	AnimeData* animeData = NULL;
	using namespace input;
	switch (obj->state)
	{
	case 0:

		animeData = animePlayerTitleN;
		obj->velocity.x = TITLE_PLAYER_VELOCITY;
		obj->speed.x = -TITLE_PLAYER_SPEED;
		obj->state++;

	case 1:
		
		animeData = animePlayerTitleN;

		if (++obj->timer % TITLE_PLAYER_CNT == 0)
		{
			obj->state = (rand() % TITLE_PLAYER_R_STATE_NUM)+1;
		}
		break;

	case 2:
		animeData = animePlayerTitleF;

		
		break;

	case 3:
		animeData = animePlayerTitleF2;


		
		break;

	}

	switch (obj->act_state)
	{
	case 0:

		if (SCENE_TITLE->getState() == 2) {
			obj->speed.x += obj->velocity.x;
			obj->position.x += obj->speed.x;
		}
		if (obj->speed.x >= 0)
		{
			obj->act_state++;
			obj->position.y = TITLE_PLAYER_INIT_POS;
		}
		break;

	case 1:
		animeData = animePlayerA;

		obj->speed.x += obj->velocity.x;
		obj->position.x += obj->speed.x;

		obj->animeUpdate(animeData);

		break;

	}


	if (obj->animeUpdate(animeData))
	{
		obj->state = 1;
	}
	

}


#define START_UI_CNT 75
#define START_UI_SCALE 0.3f

void title_anime_start(OBJ2D*obj)
{
	AnimeData* animeData = NULL;

	switch (obj->state)
	{
	case 0:
		obj->scale = VECTOR2(START_UI_SCALE, START_UI_SCALE);
		obj->color = VECTOR4(1,1,1,1);
		obj->data = &spr_anime_start00;
		obj->state++;

		//break;

	case 1:

		switch (SCENE_TITLE->getSelect())
		{
		case 0:
			obj->color = VECTOR4(1, 1, 1, 1); break;
		case 1:
			obj->color = VECTOR4(0.5f, 0.5f, 0.5f, 1); break;
		}
		
		if (SCENE_TITLE->timer>START_UI_CNT && SCENE_TITLE->getState() == 2)
			obj->state++;
		break;
	case 2:
		animeData = animeStart;

		obj->animeUpdate(animeData);
		
		break;

	}

}

//初期位置と関数のアドレスを入れた配列
TITLE_UI_INIT_DATA TitleUiInitArray[] =
{
	{ title_anime_player_large, 400, 750 },
	{ title_anime_start,		1220, 750 },
	{ nullptr, },
};



void TitleUiManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化


	// 配列サイズの取得
	for (int i = 0; TitleUiInitArray[i].mover != nullptr; ++i) {
		auto &p = TitleUiInitArray[i];
		if (!p.mover) break;
		//設定された座標にセットしていく
		pTitleUiManager->searchSet(p.mover, VECTOR2(p.posX, p.posY));
	}
}

void TitleUiManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの初期化
}



void TitleUiManager::draw()
{
	OBJ2DManager::draw_not_scroll();   // OBJ2DManagerの初期化

	switch (SCENE_TITLE->getSelect())
	{
	case 0:
		spr_font01.draw(EXIT_FONT_POSX, EXIT_FONT_POSY, EXIT_FONT_SCALE, EXIT_FONT_SCALE, 0, 0.5f, 0.5f, 0.5f, 1); break;
	case 1:
		spr_font01.draw(EXIT_FONT_POSX, EXIT_FONT_POSY, EXIT_FONT_SCALE, EXIT_FONT_SCALE, 0, 1, 1, 1, 1); break;
	}

	spr_title.draw(TITLE_LOGO_POSX, system::SCREEN_HEIGHT / 5, TITLE_LOGO_SCALE, TITLE_LOGO_SCALE);

}