#include "All.h"

using namespace GameLib;

namespace
{
	AnimeData animeEffectEnemy[] = {
		{ &spr_effect_enemy_death00, 2 },
		{ &spr_effect_enemy_death01, 2 },
		{ &spr_effect_enemy_death02, 2 },
		{ &spr_effect_enemy_death03, 2 },
		{ &spr_effect_enemy_death04, 2 },
		{ nullptr, -1 },// 終了フラグ
	};

	AnimeData animeEffectMove00[] = {
	{ &spr_effect_move00,1},
	{ &spr_effect_move01,1},
	{ &spr_effect_move02,1},
	{ &spr_effect_move03,1},
	{ &spr_effect_move04,1},
	{ &spr_effect_move05,1},
	{ &spr_effect_move06,1},
	{ &spr_effect_move07,1},
	{ &spr_effect_move08,1},
	{ &spr_effect_move09,1},
	{ &spr_effect_move10,1},
	{ &spr_effect_move11,1},
	{ &spr_effect_move12,1},
	{ &spr_effect_move13,1},
	{ &spr_effect_move14,1},
	{ nullptr, -1 },// 終了フラグ

	};

	AnimeData animeEffectMove01[] = {

		{ &spr_effect_move15,4},
		{ &spr_effect_move16,4},
		{ &spr_effect_move17,4},
		{ &spr_effect_move18,4},
		{ &spr_effect_move19,4},
		{ &spr_effect_move20,4},
		{ &spr_effect_move21,4},
		{ nullptr,4 },
		{ nullptr, -1 },// 終了フラグ

	};

	//dashLv0
	AnimeData animeEffectMove02[] = {

		{ &spr_effect_dash00,2 },
		{ &spr_effect_dash01,2 },
		{ &spr_effect_dash02,2 },
		{ &spr_effect_dash03,2 },
		{ &spr_effect_dash04,2 },
		{ nullptr, -1 },// 終了フラグ

	};

	//dashLv1
	AnimeData animeEffectMove03[] = {

		{ &spr_effect_dash05,3 },
		{ &spr_effect_dash06,3 },
		{ &spr_effect_dash07,3 },
		{ &spr_effect_dash08,3 },
		{ &spr_effect_dash09,3 },
		{ nullptr, -1 },// 終了フラグ

	};


	//dashLv2
	AnimeData animeEffectMove04[] = {

		{ &spr_effect_dash10,3 },
		{ &spr_effect_dash11,3 },
		{ &spr_effect_dash12,3 },
		{ &spr_effect_dash13,3 },
		{ &spr_effect_dash14,3 },
		{ nullptr, -1 },// 終了フラグ

	};

	AnimeData animeEffectSpawn[] = {

		{ &spr_effect_spawn00,5 },
		{ &spr_effect_spawn01,5 },
		{ &spr_effect_spawn02,5 },
		{ &spr_effect_spawn03,5 },
		{ &spr_effect_spawn04,5 },
		{ &spr_effect_spawn05,5 },
		{ &spr_effect_spawn06,5 },
		{ &spr_effect_spawn07,5 },
		{ &spr_effect_spawn08,5 },
		{ &spr_effect_spawn09,5 },
		{ nullptr, -1 },// 終了フラグ

	};
	AnimeData animeEffectBarrier[] = {

		{ &spr_barrier00,4 },
		{ &spr_barrier01,4 },
		{ &spr_barrier02,4 },
		{ &spr_barrier03,4 },
		{ &spr_barrier04,4 },
		{ &spr_barrier05,4 },
		
		{ nullptr, -1 },// 終了フラグ

	};
	AnimeData animePowerup[] = {

		{ &spr_power_up_font00,3 },
		{ &spr_power_up_font01,3 },
		{ &spr_power_up_font02,3 },
		{ &spr_power_up_font03,3 },
		{ &spr_power_up_font04,3 },
		{ &spr_power_up_font05,3 },
		{ &spr_power_up_font06,3 },
		{ &spr_power_up_font07,3 },
		{ &spr_power_up_font08,3 },
		{ &spr_power_up_font09,3 },
		{ &spr_power_up_font10,3 },
		{ &spr_power_up_font11,3 },
		{ &spr_power_up_font12,3 },
		{ &spr_power_up_font13,3 },
		{ &spr_power_up_font14,3 },
		{ &spr_power_up_font00,3 },
		{ &spr_power_up_font01,3 },
		{ &spr_power_up_font02,3 },
		{ &spr_power_up_font03,3 },
		{ &spr_power_up_font04,3 },
		{ &spr_power_up_font05,3 },
		{ &spr_power_up_font06,3 },
		{ &spr_power_up_font07,3 },
		{ &spr_power_up_font08,3 },
		{ &spr_power_up_font09,3 },
		{ &spr_power_up_font10,3 },
		{ &spr_power_up_font11,3 },
		{ &spr_power_up_font12,3 },
		{ &spr_power_up_font13,3 },
		{ &spr_power_up_font14,3 },
		

		{ nullptr, -1 },// 終了フラグ

	};
	AnimeData animeSpeedup[] = {

		{ &spr_speed_up_font00,3 },
		{ &spr_speed_up_font01,3 },
		{ &spr_speed_up_font02,3 },
		{ &spr_speed_up_font03,3 },
		{ &spr_speed_up_font04,3 },
		{ &spr_speed_up_font05,3 },
		{ &spr_speed_up_font06,3 },
		{ &spr_speed_up_font07,3 },
		{ &spr_speed_up_font08,3 },
		{ &spr_speed_up_font09,3 },
		{ &spr_speed_up_font10,3 },
		{ &spr_speed_up_font11,3 },
		{ &spr_speed_up_font12,3 },
		{ &spr_speed_up_font13,3 },
		{ &spr_speed_up_font14,3 },
		{ &spr_speed_up_font00,3 },
		{ &spr_speed_up_font01,3 },
		{ &spr_speed_up_font02,3 },
		{ &spr_speed_up_font03,3 },
		{ &spr_speed_up_font04,3 },
		{ &spr_speed_up_font05,3 },
		{ &spr_speed_up_font06,3 },
		{ &spr_speed_up_font07,3 },
		{ &spr_speed_up_font08,3 },
		{ &spr_speed_up_font09,3 },
		{ &spr_speed_up_font10,3 },
		{ &spr_speed_up_font11,3 },
		{ &spr_speed_up_font12,3 },
		{ &spr_speed_up_font13,3 },
		{ &spr_speed_up_font14,3 },
		

		{ nullptr, -1 },// 終了フラグ

	};

	AnimeData animeBarrierGet[] = {

		{ &spr_barrier_font00,3 },
		{ &spr_barrier_font01,3 },
		{ &spr_barrier_font02,3 },
		{ &spr_barrier_font03,3 },
		{ &spr_barrier_font04,3 },
		{ &spr_barrier_font05,3 },
		{ &spr_barrier_font06,3 },
		{ &spr_barrier_font07,3 },
		{ &spr_barrier_font08,3 },
		{ &spr_barrier_font09,3 },
		{ &spr_barrier_font10,3 },
		{ &spr_barrier_font11,3 },
		{ &spr_barrier_font12,3 },
		{ &spr_barrier_font13,3 },
		{ &spr_barrier_font14,3 },
		{ &spr_barrier_font00,3 },
		{ &spr_barrier_font01,3 },
		{ &spr_barrier_font02,3 },
		{ &spr_barrier_font03,3 },
		{ &spr_barrier_font04,3 },
		{ &spr_barrier_font05,3 },
		{ &spr_barrier_font06,3 },
		{ &spr_barrier_font07,3 },
		{ &spr_barrier_font08,3 },
		{ &spr_barrier_font09,3 },
		{ &spr_barrier_font10,3 },
		{ &spr_barrier_font11,3 },
		{ &spr_barrier_font12,3 },
		{ &spr_barrier_font13,3 },
		{ &spr_barrier_font14,3 },

		{ nullptr, -1 },// 終了フラグ

	};

}



void EffectManager::init()
{
	OBJ2DManager::init();
}

void EffectManager::update()
{
	OBJ2DManager::update();
}

void EffectManager::draw()
{
	OBJ2DManager::draw();
}

void effect_move_enemy_death(OBJ2D*obj)
{
	AnimeData* animeData = NULL;
	animeData = animeEffectEnemy;
	obj->scale = VECTOR2(0.8f, 0.8f);
	if (obj->animeUpdate(animeData))
		obj->clear();

}

void effect_move_player_spawn(OBJ2D*obj)
{
	AnimeData* animeData = NULL;
	animeData = animeEffectSpawn;
	obj->scale = VECTOR2(0.5, 0.5);
	if (obj->animeUpdate(animeData))
		obj->clear();
}

void effect_move_player00(OBJ2D*obj)
{

	obj->scale = VECTOR2(0.3f,0.3f);
	obj->position = VECTOR2(obj->parent->position.x, obj->parent->position.y);

	AnimeData* animeData = NULL;
	animeData = animeEffectMove00;
	if (obj->animeUpdate(animeData))
		obj->clear();
}

void effect_move_player01(OBJ2D*obj)
{
	obj->scale = VECTOR2(0.3f, 0.3f);

	AnimeData* animeData = NULL;
	animeData = animeEffectMove01;
	obj->animeUpdate(animeData);
	if (obj->parent->mover==0)
		obj->clear();

}
void effect_move_player02(OBJ2D*obj)
{
	obj->scale = VECTOR2(0.5f, 0.5f);
	AnimeData* animeData = NULL;

	switch (pPlayerManager->getOBJ2D(0)->param)
	{
	case 0:animeData = animeEffectMove02; break;
	case 1:animeData = animeEffectMove02; break;
	case 2:animeData = animeEffectMove03; break;
	case 3:animeData = animeEffectMove04; break;

	}

	obj->angle = obj->parent->angle+ToRadian(180);
	obj->position = VECTOR2(obj->parent->position.x, obj->parent->position.y);

	if (obj->animeUpdate(animeData))
		obj->clear();


}

void effect_move_powerup(OBJ2D*obj)
{
	obj->scale = VECTOR2(0.5f, 0.5f);
	AnimeData* animeData = NULL;

	animeData = animePowerup;

	obj->position = VECTOR2(obj->parent->position.x, obj->parent->position.y-80);

	if (obj->animeUpdate(animeData))
		obj->clear();


}
void effect_move_speedup(OBJ2D*obj)
{
	obj->scale = VECTOR2(0.5f, 0.5f);
	AnimeData* animeData = NULL;

	animeData = animeSpeedup;

	obj->position = VECTOR2(obj->parent->position.x , obj->parent->position.y-80);

	if (obj->animeUpdate(animeData))
		obj->clear();


}

void effect_move_barrier(OBJ2D*obj)
{
	obj->scale = VECTOR2(0.5f, 0.5f);
	AnimeData* animeData = NULL;

	animeData = animeBarrierGet;

	obj->position = VECTOR2(obj->parent->position.x, obj->parent->position.y-80);

	if (obj->animeUpdate(animeData))
		obj->clear();


}

void effect_move_protect(OBJ2D*obj)
{
	obj->scale = VECTOR2(1.5f, 1.5f);
	AnimeData* animeData = NULL;

	animeData = animeEffectBarrier;

	obj->position = VECTOR2(obj->parent->position.x-10, obj->parent->position.y-10);
	obj->animeUpdate(animeData);

	if (!pPlayerManager->isBarrier_flg)
		obj->clear();


}