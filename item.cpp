#include "all.h"



VECTOR2 getVec2Player(VECTOR2 src);

using namespace GameLib;


AnimeData animeHelpRabbit[] = {

	{ &spr_help_rabbit00, 6 },
	{ &spr_help_rabbit01, 6 },
	{ &spr_help_rabbit02, 6 },
	{ &spr_help_rabbit03, 7 },
	{ &spr_help_rabbit04, 7 },
	{ &spr_help_rabbit05, 7 },
	{ &spr_help_rabbit06, 7 },
	
	{ nullptr, -1 },// 終了フラグ

};


#define ITEM_SIZE 40
#define ITEM_WAIT 1

#define ITEM00_SCALE 0.4f
#define ITEM03_SCALE 2.5f


// パワーアップ
void ItemMove00(OBJ2D*obj)
{ 

	switch (obj->state)
	{

	case 0:
		obj->center = obj->position;
		{
			OBJ2D* child = pEffectManager->searchSet(effect_move_player01, obj->position);
			child->parent = obj;
		}
		obj->scale *= ITEM00_SCALE;
		obj->size = VECTOR2(ITEM_SIZE, ITEM_SIZE);
		obj->data = &spr_item_power;
		obj->wait = ITEM_WAIT;
		obj->state++;
		//break;

	case 1:
		Floating_Move(obj);
		obj->timer++;

		
		break;
	}
}


// バリア
void ItemMove01(OBJ2D* obj)
{
	switch (obj->state)
	{

	case 0:
		obj->center = obj->position;
		{
			OBJ2D* child = pEffectManager->searchSet(effect_move_player01, obj->position);
			child->parent = obj;		
		}
		obj->data = &spr_item_bariier;
		obj->scale *= ITEM00_SCALE;
		obj->size = VECTOR2(ITEM_SIZE, ITEM_SIZE);
		obj->wait = ITEM_WAIT;
		obj->state++;
		//break;

	case 1:
		Floating_Move(obj);
		obj->timer++;

		break;
	}
}


// 1UP
void ItemMove02(OBJ2D* obj)
{
	switch (obj->state)
	{

	case 0:
		obj->center = obj->position;
		{
			OBJ2D* child = pEffectManager->searchSet(effect_move_player01, obj->position);
			child->parent = obj;		
		}
		obj->scale *= ITEM00_SCALE;
		obj->data = &spr_item_1up;
		obj->size = VECTOR2(ITEM_SIZE, ITEM_SIZE);
		obj->wait = ITEM_WAIT;
		obj->state++;
		//break;

	case 1:

		Floating_Move(obj);
		obj->timer++;

		break;
	}
}


//救助町
void ItemMove03(OBJ2D* obj)
{
	AnimeData* animeData = NULL;

	switch (obj->state)
	{

	case 0:
		obj->center = obj->position;
		{
			OBJ2D* child = pEffectManager->searchSet(effect_move_player01,obj->position);
			child->parent = obj;		
		}
		obj->scale *= ITEM03_SCALE;
		obj->size = VECTOR2(ITEM_SIZE, ITEM_SIZE);
		obj->wait = ITEM_WAIT;
		obj->state++;
		//break;

	case 1:
		animeData = animeHelpRabbit;

		obj->animeUpdate(animeData);
		Floating_Move(obj);
		obj->timer++;

		break;
	}
}


MOVER ItemMoveArray[] =
{
	ItemMove00,
	ItemMove01,
	ItemMove02,
	ItemMove03
};







void ItemManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化

	STAGE_SCRIPT*pScript = pStageManager->getStageScript();


	// アイテムの読み込み
	char** mapItem = new char*[pBGManager->CHIP_NUM_Y];

	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
		mapItem[i] = new char[pBGManager->CHIP_NUM_X];
		SecureZeroMemory(mapItem[i], sizeof(char)*pBGManager->CHIP_NUM_X);
	}

	if (!pBGManager->loadMapData(pScript->fileNameItem, mapItem))
	{
		assert(!"アイテムデータ読み込み失敗");
	}

	// アイテム配置
	for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
	{
		for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
		{
			const int itemIndex = mapItem[y][x];
			if (itemIndex < 0) continue;
			searchSet(ItemMoveArray[itemIndex], VECTOR2(
				static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
				static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
			);

		}
	}

	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
	{
		delete[] mapItem[i];
	}
	delete[] mapItem;
}

void ItemManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの初期化
}

void ItemManager::draw()
{
	OBJ2DManager::draw();   // OBJ2DManagerの初期化
	/*if (DEBUG_NOW) {
		for (auto &obj : *pItemManager) {
			primitive::rect(obj.position.x - obj.size.x - pBGManager->getScrollPos().x, obj.position.y - obj.size.y - pBGManager->getScrollPos().y, obj.size.x * 2, obj.size.y * 2, 0, 0, 0, 1, 0, 0, 0.3f);
		}
	}*/
}



// プレイヤー方向単位ベクトル
VECTOR2 getVec2Player(VECTOR2 src)
{
	auto player = pPlayerManager->begin();
	VECTOR2	dst = (player->mover) ? player->position : VECTOR2(GameLib::system::SCREEN_WIDTH / 2, GameLib::system::SCREEN_HEIGHT / 2);
	dst -= src;
	return dst;
}

