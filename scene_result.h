#pragma once
//******************************************************************************
//
//
//      ゲームシーン
//
//
//******************************************************************************

//==============================================================================
//
//      Gameクラス
//
//==============================================================================

class Result : public Scene, public Singleton<Result>
{
public:
	//int score;

private:
	bool isPaused;

	int mode;

	static const int SELECT_MAX = 16;
	SELECT_DATA selectData[SELECT_MAX];
public:
	void init();
	void update();
	void draw();
	void uninit();
	bool isClear;//クリアしてるか否か

};

static Result* const SCENE_RESULT = Result::getInstance();

//******************************************************************************
