#include "all.h"

#include <string>
#include <bitset>
#include <DirectXMath.h>

using namespace GameLib;


void UiSpGage(OBJ2D*);
void UiStart(OBJ2D*);
void StartUi(OBJ2D* obj);

//UIのアニメデータ
namespace {
	AnimeData animeStartUi[] = {
		{ &spr_start_ui00, 5 },
		{ &spr_start_ui01, 5 },
		{ &spr_start_ui02, 5 },
		{ &spr_start_ui03, 5 },
		{ &spr_start_ui04, 5 },
		{ &spr_start_ui05, 5 },
		{ &spr_start_ui06, 5 },
		{ &spr_start_ui07, 5 },
		{ &spr_start_ui08, 5 },
		{ &spr_start_ui09, 5 },
		{ &spr_start_ui00, 5 },
		{ &spr_start_ui01, 5 },
		{ &spr_start_ui02, 5 },
		{ &spr_start_ui03, 5 },
		{ &spr_start_ui04, 5 },
		{ &spr_start_ui05, 5 },
		{ &spr_start_ui06, 5 },
		{ &spr_start_ui07, 5 },
		{ &spr_start_ui08, 5 },
		{ &spr_start_ui09, 5 },
		{ &spr_start_ui00, 5 },
		{ &spr_start_ui01, 5 },
		{ &spr_start_ui02, 5 },
		{ &spr_start_ui03, 5 },
		{ &spr_start_ui04, 5 },
		{ &spr_start_ui05, 5 },
		{ &spr_start_ui06, 5 },
		{ &spr_start_ui07, 5 },
		{ &spr_start_ui08, 5 },
		{ &spr_start_ui09, 5 },
		{ &spr_start_ui00, 5 },
		{ &spr_start_ui01, 5 },
		{ &spr_start_ui02, 5 },
		{ &spr_start_ui03, 5 },
		{ &spr_start_ui04, 5 },
		{ &spr_start_ui05, 5 },
		{ &spr_start_ui06, 5 },
		{ &spr_start_ui07, 5 },
		{ &spr_start_ui08, 5 },
		{ &spr_start_ui09, 5 },
		{ nullptr, -1 },// 終了フラグ

	};


	UI_INIT_DATA UiInitArray[] =
	{
		 { UiSpGage, 1280, 50 },
		 { UiStart, -1000, system::SCREEN_HEIGHT / 2 },
		 { StartUi, system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 3 },

		{ nullptr },
	};
}

namespace
{
	//数字の画像データ
	SpriteData Combo_Array[]
	{
		spr_number00,
		spr_number01,
		spr_number02,
		spr_number03,
		spr_number04,
		spr_number05,
		spr_number06,
		spr_number07,
		spr_number08,
		spr_number09
	};

	SpriteData Score_Array[]
	{
		spr_sc_number00,
		spr_sc_number01,
		spr_sc_number02,
		spr_sc_number03,
		spr_sc_number04,
		spr_sc_number05,
		spr_sc_number06,
		spr_sc_number07,
		spr_sc_number08,
		spr_sc_number09
	};
}


//描画位置やスケール関係
#define UI_RESQUE_POSX  1580
#define UI_RESQUE_POSY  30
#define UI_RESQUE_SCALE 3.0f
#define UI_COMBO_SCALEX 0.28f
#define UI_COMBO_SCALEY 0.25f
#define UI_START_SCALE		0.5f
#define UI_START_VELOCITY   0.8f
#define UI_START_INIT_SPEED 76.5f
#define UI_START_BACK_CNT	160
#define SCORE_FONT_POSX		620
#define SCORE_FONT_POSY		50
#define SCORE_POSX			230
#define SCORE_SCALE			0.32f
#define COMBO_NUM_POSX		930
#define COMBO_NUM_POSY		50
#define COMBO_NUM_SCALE		0.89f
#define COMBO_NUM_SCALE		0.89f
#define COMBO_BAR_POSX		1210.0f
#define COMBO_BAR_POSY		60.0f
#define COMBO_BAR_SCALEX	0.3f
#define COMBO_BAR_SCALEY	0.25f
#define COMBO_FRAME_SCALEX	0.65f
#define COMBO_FRAME_SCALEY	0.65f
#define COMBO_FRAME_POSX	840
#define COMBO_FRAME_POSY	50
#define ARROWSCALE			0.5f
#define DARK_COLOR			0.3f


void ResNum()
{
	//最初は暗く
	float color1=DARK_COLOR;
	float color2=DARK_COLOR;
	float color3=DARK_COLOR;
	
	//助けた数によって変化
	if(pPlayerManager->resqueNum>0)
		color1 = 1;
	if (pPlayerManager->resqueNum>1)
		color2 = 1;
	if (pPlayerManager->resqueNum>2)
		color3 = 1;

	spr_help_rabbit00.draw(UI_RESQUE_POSX,	  UI_RESQUE_POSY,UI_RESQUE_SCALE, UI_RESQUE_SCALE,0, color1, color1, color1, 1);
	spr_help_rabbit00.draw(UI_RESQUE_POSX+100,UI_RESQUE_POSY,UI_RESQUE_SCALE, UI_RESQUE_SCALE,0, color2, color2, color2, 1);
	spr_help_rabbit00.draw(UI_RESQUE_POSX+200,UI_RESQUE_POSY,UI_RESQUE_SCALE, UI_RESQUE_SCALE,0, color3, color3, color3, 1);

}
void StartUi(OBJ2D* obj)
{
	AnimeData* anime = nullptr;

		anime = animeStartUi;

		if (obj->animeUpdate(animeStartUi))
			obj->clear();
}
void UiSpGage(OBJ2D* obj)
{
	switch (obj->state)
	{
	case 0:
		obj->scale = VECTOR2(UI_COMBO_SCALEX, UI_COMBO_SCALEY);
		obj->data = &spr_ui00;
		obj->state++;
		//break;
	case 1:


		break;
	}
}

void UiCoGage(OBJ2D* obj)
{
	switch (obj->state)
	{
	case 0:
		obj->scale = VECTOR2(UI_COMBO_SCALEX, UI_COMBO_SCALEY);
		obj->data = &spr_ui02;
		obj->state++;
		//break;
	case 1:


		break;
	}
}





void UiStart(OBJ2D* obj)
{
	switch (obj->state)
	{
	case 0:
		obj->scale = VECTOR2(UI_START_SCALE, UI_START_SCALE);
		obj->data = &spr_font02;
		obj->velocity.x=-UI_START_VELOCITY;

		obj->speed.x = UI_START_INIT_SPEED;
		obj->state++;
		//break;
	case 1:

		obj->speed.x += obj->velocity.x;

		if (pStageManager->getTimer() <= UI_START_BACK_CNT)
		obj->position.x += obj->speed.x;

		if (pStageManager->getTimer() == START_TIME)
			obj->clear();
		break;
	}
}

void UiManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化


	//配列サイズの取得
	for (int i = 0; UiInitArray[i].mover != nullptr; ++i) {
		auto &p = UiInitArray[i];
		if (!p.mover) break;
		//設定された座標にセットしていく
		pUiManager->searchSet(p.mover, VECTOR2(p.posX, p.posY));
	}
}

void UiManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの初期化
}




void UiManager::draw()
{
	OBJ2DManager::draw_not_scroll();   // OBJ2DManagerの初期化

	//コンボゲージ
	if (SCENE_GAME->timer > START_TIME)
	{
		for (int i = 0; i < pPlayerManager->getOBJ2D(0)->param; i++)
			spr_ui01.draw(VECTOR2(COMBO_BAR_POSX + (float)i * 100.f, COMBO_BAR_POSY), VECTOR2(COMBO_BAR_SCALEX, COMBO_BAR_SCALEY), 0, VECTOR4(1, 1, 1, 1));
	}
	spr_ui03.draw(VECTOR2(COMBO_FRAME_POSX, COMBO_FRAME_POSY), VECTOR2(COMBO_FRAME_SCALEX, COMBO_FRAME_SCALEY), 0, VECTOR4(1, 1, 1, 1));


	//コンボ数
	int cnt = pPlayerManager->getOBJ2D(0)->comboCnt;
	Combo_Array[cnt % 10].draw(COMBO_NUM_POSX,    COMBO_NUM_POSY, COMBO_NUM_SCALE, COMBO_NUM_SCALE);
	Combo_Array[cnt / 10].draw(COMBO_NUM_POSX-30, COMBO_NUM_POSY, COMBO_NUM_SCALE, COMBO_NUM_SCALE);


	//スコア描画
	int score = pPlayerManager->getOBJ2D(0)->iWork[player::iWork::score];
	Score_Array[score			% 10].draw(SCORE_FONT_POSX,		 SCORE_FONT_POSY, SCORE_SCALE, SCORE_SCALE);
	Score_Array[score / 10		% 10].draw(SCORE_FONT_POSX-50,   SCORE_FONT_POSY, SCORE_SCALE, SCORE_SCALE);
	Score_Array[score / 100		% 10].draw(SCORE_FONT_POSX-50*2, SCORE_FONT_POSY, SCORE_SCALE, SCORE_SCALE);
	Score_Array[score / 1000	% 10].draw(SCORE_FONT_POSX-50*3, SCORE_FONT_POSY, SCORE_SCALE, SCORE_SCALE);
	Score_Array[score / 10000	% 10].draw(SCORE_FONT_POSX-50*4, SCORE_FONT_POSY, SCORE_SCALE, SCORE_SCALE);
	Score_Array[score / 100000	% 10].draw(SCORE_FONT_POSX-50*5, SCORE_FONT_POSY, SCORE_SCALE, SCORE_SCALE);
	spr_font06.draw(SCORE_POSX, COMBO_NUM_POSY, SCORE_SCALE, SCORE_SCALE);


	//プレイヤーの方向カーソル
	OBJ2D* pl = pPlayerManager->getOBJ2D(0);
	if (!pl->moveFlg)
		spr_arrow.draw(VECTOR2(pl->position.x - pBGManager->getScrollPos().x, pl->position.y - pBGManager->getScrollPos().y), VECTOR2(ARROWSCALE, ARROWSCALE),pl->fWork[player::fWork::fromPlShotAngle]);

	//救助数の描画
	ResNum();
}