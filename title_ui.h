#pragma once

#define TITLE_UI_MAX 64

struct TITLE_UI_INIT_DATA 
{
	MOVER mover;
	float posX;
	float posY;
};


class TitleUiManager : public OBJ2DManager, public Singleton<TitleUiManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return TITLE_UI_MAX; }
};

#define pTitleUiManager	(TitleUiManager::getInstance())