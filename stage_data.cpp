#include "All.h"

STAGE_SCRIPT stage1_script[] = 
{

	"./Data/Maps/map._プレイヤー.csv",

	"./Data/Maps/map._地形.csv",

	"./Data/Maps/map._障害物.csv",

	"./Data/Maps/map._エネミー.csv",

	"./Data/Maps/map._アイテム.csv",

	"./Data/Maps/map._扉.csv",


	nullptr
};


STAGE_SCRIPT stage2_script[] =
{

	
	"./Data/Maps/map2._プレイヤー.csv",

	"./Data/Maps/map2._地形.csv",

	"./Data/Maps/map2._障害物.csv",

	"./Data/Maps/map2._エネミー.csv",

	"./Data/Maps/map2._アイテム.csv",

	"./Data/Maps/map2._扉.csv",

	nullptr
};


STAGE_SCRIPT stage3_script[] =
{

	"./Data/Maps/map3._プレイヤー.csv",

	"./Data/Maps/map3._地形.csv",

	"./Data/Maps/map3._障害物.csv",

	"./Data/Maps/map3._エネミー.csv",

	"./Data/Maps/map3._アイテム.csv",

	"./Data/Maps/map3._扉.csv",


	nullptr
};


STAGE_SCRIPT stage4_script[] =
{

	"./Data/Maps/map4._プレイヤー.csv",

	"./Data/Maps/map4._地形.csv",

	"./Data/Maps/map4._障害物.csv",

	"./Data/Maps/map4._エネミー.csv",

	"./Data/Maps/map4._アイテム.csv",

	"./Data/Maps/map4._扉.csv",


	nullptr
};

STAGE_SCRIPT stage5_script[] =
{

	"./Data/Maps/map5._プレイヤー.csv",

	"./Data/Maps/map5._地形.csv",

	"./Data/Maps/map5._障害物.csv",

	"./Data/Maps/map5._エネミー.csv",

	"./Data/Maps/map5._アイテム.csv",

	"./Data/Maps/map5._扉.csv",


	nullptr
};

STAGE_SCRIPT stage6_script[] =
{

	"./Data/Maps/map6._プレイヤー.csv",
	"./Data/Maps/map6._地形.csv",
	"./Data/Maps/map6._障害物.csv",
	"./Data/Maps/map6._エネミー.csv",
	"./Data/Maps/map6._アイテム.csv",
	"./Data/Maps/map6._扉.csv",


	nullptr
};