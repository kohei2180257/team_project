//******************************************************************************
//
//
//      ゲーム
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;


#define SELECT_MAX	(2)

SELECT_DATA menuOverTbl[] =
{
	{ &spr_font03, VECTOR2(470,  900) },
	{ &spr_font05, VECTOR2(1450, 900) },
};
SELECT_DATA menuClearTbl[] =
{
	{ &spr_font00, VECTOR2(600,  900) },
	{ &spr_font05_clear, VECTOR2(1450, 900) },
};
SELECT_DATA menuClearEndTbl[] =
{
	{ &spr_font05_clear, VECTOR2(450,  900) },
	{ &spr_font01_clear, VECTOR2(1450, 900) },
};

//--------------------------------
//  初期化処理
//--------------------------------
void Result::init()
{
	Scene::init();	    // 基底クラスのinitを呼ぶ
	isPaused = false;   // ポーズフラグの初期化
	for (auto & p : selectData)
	{
		p.scale = VECTOR2(1, 1);
	}
}

//--------------------------------
//  更新処理
//--------------------------------
void Result::update()
{
	using namespace input;

	switch (state)
	{
	case 0:
		//////// 初期設定 ////////

		//isClear = true;

		if (!isClear)
		{
			mode = 0;
			music::load(WINBGM, L"./Data/Musics/gameover.wav", 1.0f);
		}
		else if (pStageManager->stageNo + 1 < STAGE_MAX)
		{
			mode = 1;
			music::load(WINBGM, L"./Data/Musics/win.wav", 1.0f);

		}
		else
		{
			mode = 2;
			music::load(WINBGM, L"./Data/Musics/win.wav", 1.0f);
		}
		music::play(WINBGM,true);

		timer = 0;
		GameLib::setBlendMode(Blender::BS_ALPHA);   // 通常のアルファ処理

		// テクスチャの読み込み
		texture::load(loadTextureResult);
		stick_now = false;
		state++;    // 初期化処理の終了
	
		// break;      // 意図的なコメントアウト

	case 1:
		//////// 通常時の処理 ////////

		timer++;

		if (TRG(0) & PAD_LEFT) {
			select = (select + (SELECT_MAX - 1)) % SELECT_MAX;
			sound::play(SOUND::SOUND_SELECT);
		}
		if (TRG(0) & PAD_RIGHT) {
			select = (select + 1) % SELECT_MAX;
			sound::play(SOUND::SOUND_SELECT);
		}

		if ((GET_LEFT_STICK_X>0) && !stick_now) {
			stick_now = true;
			select = (select + (SELECT_MAX - 1)) % SELECT_MAX;
			sound::play(SOUND::SOUND_SELECT);
		}
		else if ((GET_LEFT_STICK_X<0) && !stick_now) {
			select = (select + 1) % SELECT_MAX;
			stick_now = true;
			sound::play(SOUND::SOUND_SELECT);
		}

		if (!GET_LEFT_STICK_X)
			stick_now = false;


		// 選択するとスケールが変化
		for (int i = 0; i < SELECT_MAX; ++i) {
			selectData[i].scale = VECTOR2(0.4f, 0.4f);
			selectData[i].color = VECTOR4(0.4f, 0.4f, 0.4f, 1.0f);
		}
		selectData[select].scale = VECTOR2(0.5f, 0.5f);
		selectData[select].color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);


		if (TRG(0)&PAD_TRG1) {
			sound::play(SOUND::SOUND_DECIDE);

			switch (select) {
			case 0:
				switch (mode)
				{
				case 0:
					SCENE_RESULT->changeScene(SCENE_GAME);
					break;
				case 1:
					SCENE_RESULT->changeScene(SCENE_GAME);
					pStageManager->stageNo++;
					break;
				case 2:
					SCENE_RESULT->changeScene(SCENE_TITLE);

					break;
				}

				break;

			case 1:
				SCENE_RESULT->changeScene(SCENE_TITLE);


				switch (mode)
				{
				case 0:
					SCENE_RESULT->changeScene(SCENE_TITLE);
					break;
				case 1:
					SCENE_RESULT->changeScene(SCENE_TITLE);
					pStageManager->stageNo++;
					break;
				case 2:
					exit(0);
					break;
				}

				break;
			}

			state = 0;
		}
		break;
	}
}

SpriteData resultScore_Array[]
{
	spr_sc_number00,
	spr_sc_number01,
	spr_sc_number02,
	spr_sc_number03,
	spr_sc_number04,
	spr_sc_number05,
	spr_sc_number06,
	spr_sc_number07,
	spr_sc_number08,
	spr_sc_number09
};

#define SCORE_POS_RES	(system::SCREEN_WIDTH / 2)
#define SCORE_POS_RES_Y (700)
//--------------------------------
//  描画処理
//--------------------------------
void Result::draw()
{
	// 画面クリア
	GameLib::clear(VECTOR4(0, 0, 0, 1));

	int score = pPlayerManager->getOBJ2D(0)->iWork[player::iWork::score];

	switch (mode)
	{
	case 0: // GAME OVER

		spr_gameover.draw(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 2, 1.5f, 1.5f);
		spr_font07.draw(system::SCREEN_WIDTH / 2 - 50, 250, 0.8f, 0.8f);


		for (int i = 0; i < 2; ++i) {
			menuOverTbl[i].pSprData->draw(menuOverTbl[i].position, selectData[i].scale, 0, selectData[i].color);
		}
		break;
	case 1: // STAGE CLEAR
		spr_clear_bg.draw(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 2, 1.5f, 1.5f);
		
		spr_font06.draw(SCORE_POS_RES - 200, SCORE_POS_RES_Y, 0.5f, 0.5f);

		spr_font04.draw(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 3, 0.5f, 0.5f);


		resultScore_Array[score % 10].draw(			SCORE_POS_RES + 250,   SCORE_POS_RES_Y, 0.5f, 0.5f);
		resultScore_Array[score / 10 % 10].draw(	SCORE_POS_RES + 200,  SCORE_POS_RES_Y, 0.5f, 0.5f);
		resultScore_Array[score / 100 % 10].draw(	SCORE_POS_RES + 150, SCORE_POS_RES_Y, 0.5f, 0.5f);
		resultScore_Array[score / 1000 % 10].draw(	SCORE_POS_RES + 100, SCORE_POS_RES_Y, 0.5f, 0.5f);
		resultScore_Array[score / 10000 % 10].draw(	SCORE_POS_RES + 50, SCORE_POS_RES_Y, 0.5f, 0.5f);
		resultScore_Array[score / 100000 % 10].draw(SCORE_POS_RES + 0, SCORE_POS_RES_Y, 0.5f, 0.5f);
		for (int i = 0; i < 2; ++i) {
			menuClearTbl[i].pSprData->draw(menuClearTbl[i].position, selectData[i].scale,0, selectData[i].color);
		}
		break;
	case 2: // GAME CLEAR
		spr_clear_bg.draw(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 2, 1.5f, 1.5f);

		spr_font06.draw(SCORE_POS_RES - 200, SCORE_POS_RES_Y, 0.5f, 0.5f);

		spr_font04.draw(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 3, 0.5f, 0.5f);


		resultScore_Array[score % 10].draw(SCORE_POS_RES + 250, SCORE_POS_RES_Y, 0.5f, 0.5f);
		resultScore_Array[score / 10 % 10].draw(SCORE_POS_RES + 200, SCORE_POS_RES_Y, 0.5f, 0.5f);
		resultScore_Array[score / 100 % 10].draw(SCORE_POS_RES + 150, SCORE_POS_RES_Y, 0.5f, 0.5f);
		resultScore_Array[score / 1000 % 10].draw(SCORE_POS_RES + 100, SCORE_POS_RES_Y, 0.5f, 0.5f);
		resultScore_Array[score / 10000 % 10].draw(SCORE_POS_RES + 50, SCORE_POS_RES_Y, 0.5f, 0.5f);
		resultScore_Array[score / 100000 % 10].draw(SCORE_POS_RES + 0, SCORE_POS_RES_Y, 0.5f, 0.5f);
		for (int i = 0; i < 2; ++i) {
			menuClearEndTbl[i].pSprData->draw(menuClearEndTbl[i].position, selectData[i].scale,0, selectData[i].color);
		}
		break;
	}
}
#undef SCORE_POS_RES
#undef SCORE_POS_RES_Y

//--------------------------------
//  終了処理
//--------------------------------
void Result::uninit()
{
	// テクスチャの解放
	texture::releaseAll();
	// 音楽のクリア
	music::stop(WINBGM);
	music::unload(WINBGM);
}

//******************************************************************************
#undef SELECT_MAX
