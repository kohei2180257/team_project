#ifndef INCLUDED_ALL
#define INCLUDED_ALL

//******************************************************************************
//
//
//      all.h
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "./GameLib/game_lib.h"
#include "./GameLib/template.h"
#include "./GameLib/input_manager.h"
#include "./GameLib/obj2d_data.h"

#include "my_util.h"
#include "sprite_data.h"
#include "common.h"
#include "obj2d.h"
#include "ui.h"
#include "title_ui.h"
#include "demo.h"
#include "item.h"
#include "judge.h"
#include "bg.h"
#include "player.h"
#include "enemy.h"
#include "enemy_shot.h"
#include "blackhole.h"
#include "gimmick_terr.h"
#include "door.h"
#include "stage.h"
#include "effect.h"
#include "blackout.h"
#include "scene.h"
#include "time.h"
#include "scene_demo.h"
#include "scene_title.h"
#include "scene_game.h"
#include "scene_result.h"
#include "stage_data.h"
#include "flg.h"




//******************************************************************************

#endif // !INCLUDED_ALL