#pragma once

#define STAGE_MAX 6
#define START_TIME 180

// 敵キャラ発生データ（いずれステージスクリプトになる）
class STAGE_SCRIPT
{
public:
	const char* fileNamePlayer;
	const char* fileNameTerrain;
	const char* fileNameGTerr;
	const char* fileNameEnemy;
	const char* fileNameItem;
	const char* fileNameDoor;

};


class StageManager : public OBJ2DManager, public Singleton<StageManager>
{
private:

	
	int				state;
	int				timer;
	int				cnt;
	char*			str_start;
	STAGE_SCRIPT*	pScript;

public:
	int				stageNo=0;
	int				scroll_speed;
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
					
	STAGE_SCRIPT* getStageScript(){ return pScript; }
	int getSize() { return STAGE_MAX; }
	int getTimer() { return timer; }
	int getCnt() { return cnt; }

};

#define pStageManager	(StageManager::getInstance())

