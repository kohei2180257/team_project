//******************************************************************************
//
//
//      タイトル
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"


#define TITLE_END_TIME 1800
#define TITLE_INTERBAL 140

using namespace GameLib;



void Title::init()
{
	timer = 0;  // タイマーを初期化

	state = 0;
	Scene::init();	    // 基底クラスのinitを呼ぶ
}

//--------------------------------
//  更新処理
//--------------------------------
void Title::update()
{
    using namespace input;

    switch (state)
    {
    case 0:

        //////// 初期設定 ////////
        timer = 0;  // タイマーを初期化
        GameLib::setBlendMode(Blender::BS_ALPHA);   // 通常のアルファ処理
		texture::load(loadTextureTitle);
		music::load(TITLEBGM, L"./Data/Musics/title.wav", 1.0f);
		music::play(TITLEBGM,true);
		
		sound::load(L"./Data/Sounds/SE.xwb", 1.0f);

		stick_now = false;

		pStageManager->stageNo = 0;
		pTitleUiManager->init();


        state++;    // 初期化処理の終了
        //break;    // 意図的なコメントアウト

    case 1:
        //////// 通常時の処理 ////////
		++timer;

		if(timer>TITLE_END_TIME)
			changeScene(SCENE_DEMO);    // ゲームシーンに切り替え


		if ((GET_LEFT_STICK_Y>0)&&!stick_now) {
			stick_now = true;
			select = (select + (SELECT_MAX - 1)) % SELECT_MAX;
			sound::play(SOUND::SOUND_SELECT);
		}
		else if ((GET_LEFT_STICK_Y<0)&&!stick_now) {
			select = (select + 1) % SELECT_MAX;
			stick_now = true;
			sound::play(SOUND::SOUND_SELECT);
		}
		
		if(!GET_LEFT_STICK_Y)
			stick_now = false;


		//ゲームへ
		if ((TRG(0)& PAD_TRG1)&&(select==0)) {         // PAD_TRG1が押されたら
			state++;
			timer = 0;
			sound::play(SOUND_DECIDE);
		}
		//終了
		if ((TRG(0)& PAD_TRG1) && (select == 1))
		{  // PAD_TRG1が押されたら
			exit(0);
		}
		pTitleUiManager->update();
        break;

	case 2:

		if (timer == FRAME_RATE)
			sound::play(SOUND::SOUND_DRILL);


		if (++timer > TITLE_INTERBAL)    // タイマーを足す
		{
			changeScene(SCENE_GAME);    // ゲームシーンに切り替え
		}

		pTitleUiManager->update();
		break;
    }


	


}

//--------------------------------
//  描画処理
//--------------------------------
void Title::draw()
{
    // 画面クリア
    GameLib::clear(VECTOR4(0.0f, 0.0f, 0.0f, 1));
	spr_titlebg.draw(0,0);

    // "Push Start Button" 点滅
    if ((timer / FRAME_RATE) % 2)
    {
		font::textOut(4, "Push A Button", VECTOR2(TEXTOUT_POSX, TEXTOUT_POSY*1.5f), VECTOR2(TEXTOUT_SCALE, TEXTOUT_SCALE), VECTOR4(1, 1, 1, 1));
	}


	pTitleUiManager->draw();

	
}


void Title::uninit()
{
	texture::releaseAll();
	music::unload(TITLEBGM);
	
}
//******************************************************************************
