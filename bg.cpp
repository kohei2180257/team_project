//******************************************************************************
//
//
//      BGクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

#include <string>
#include <bitset>

using namespace GameLib;

//--------------------------------
//  コンストラクタ
//--------------------------------
BG::BG() : scroll(VECTOR2(0, 0)), terr(),timer(0)
{
	mapDelete();

}

//--------------------------------
//  デストラクタ
//--------------------------------
BG::~BG()
{
}

//--------------------------------
//  初期設定
//--------------------------------
void BG::init()
{
	scroll = VECTOR2(0, 0);
	// BG用データのクリア
	clear();

	STAGE_SCRIPT*pScript = pStageManager->getStageScript();

	// マップ全体のサイズを取得
	if (!allMapCount(pScript->fileNameTerrain))
	{
		assert(!"マップ全体のサイズのロードに失敗");
	}

	terr = new char*[CHIP_NUM_Y];

	for (int i = 0; i < CHIP_NUM_Y; ++i)
	{

		terr[i] = new char[CHIP_NUM_X];

		SecureZeroMemory(terr[i], sizeof(char)*pBGManager->CHIP_NUM_X);

	}

	// マップデータのロード
	

	if (!loadMapData(pScript->fileNameTerrain, terr))
	{
		assert(!"地形データのロードに失敗");
	}

	timer = 0;
	
}

//--------------------------------
//  メンバ変数のクリア
//--------------------------------
void BG::clear()
{
	scroll = {};                                // スクロール座標（画面左上の位置のワールド座標）
}

//--------------------------------
//  地形データ、背景データ削除
//--------------------------------
void BG::mapDelete()
{
	if (terr) {
		for (int i = 0; i < CHIP_NUM_Y; ++i)
			delete[] terr[i];
		delete[] terr;
	}

	terr = nullptr;
}

//-----------------------------------
//      マップサイズ取得
//-----------------------------------
bool BG::allMapCount(const char* file_name)
{
	// マップデータ読み込み
	std::ifstream inputFile_terr(file_name);
	if (inputFile_terr.fail()) return false;


	// ラムダ式関数定義
	auto split = [](const char* src, const char delim, int *x)
	{
		std::istringstream stream{ src };
		std::string output;
		int cnt = 0;
		while (std::getline(stream, output, delim))
		{
			cnt++;
		}
		*x = cnt;
	};


	const int bufSize = 2048;
	char buf[bufSize];
	int y = 0;
	while (inputFile_terr.getline(buf, bufSize - 1))
	{
		split(buf, ',', &CHIP_NUM_X);
		y++;
	}

	CHIP_NUM_Y = y;

	WIDTH = (float)((CHIP_SIZE)*(CHIP_NUM_X));
	HEIGHT = (float)((CHIP_SIZE)*(CHIP_NUM_Y));

	return true;

}

//--------------------------------
//  マップデータのロード
//--------------------------------
bool BG::loadMapData(const char* fileName, char** map)
{
	std::ifstream inputFile(fileName);
	if (inputFile.fail()) return false;

	auto split = [](const char* src, const char delim, const int Y, char** out)
	{
		std::istringstream stream{ src };
		std::string output;
		int cnt = 0;
		while (std::getline(stream, output, delim))
		{
			out[Y][cnt] = static_cast<char>(std::stoi(output));
			cnt++;
		}
	};

	const int bufSize = 2048;

	char* buf = new char[bufSize];

	int y = 0;
	while (inputFile.getline(&buf[0], bufSize - 1))
	{
		split(buf, ',', y, map);//&map[y++][0]とmap[y++]は同じ意味
		y++;
	}

	delete[] buf;

	return true;
}


//--------------------------------
//  更新処理
//--------------------------------
void BG::update()
{
	timer++;
	scrollMap();

}

//--------------------------------
//  スクロール座標の移動（clampを使用）
//--------------------------------
void BG::scrollMap()
{
	

	// 定数
	static const float SCROLL_X_MIN = 0.0f;
	static const float SCROLL_X_MAX = WIDTH - system::SCREEN_WIDTH;
	static const float SCROLL_Y_MIN = 0.0f;
	static const float SCROLL_Y_MAX = HEIGHT - system::SCREEN_HEIGHT;

	// 変数
	auto pl = *pPlayerManager->begin();
	const float scrollLimitR = pl.position.x + pl.size.x - system::SCREEN_WIDTH + SCROLL_MERGIN_X;
	const float scrollLimitL = pl.position.x - pl.size.x - SCROLL_MERGIN_X;
	const float scrollLimitD = pl.position.y - system::SCREEN_HEIGHT + SCROLL_MERGIN_Y;
	const float scrollLimitU = pl.position.y - pl.size.y - SCROLL_MERGIN_Y;

	// clamp
	//scroll.x = clamp(scroll.x, scrollLimitR, scrollLimitL);
	scroll.y =0;
	//scroll.x = clamp(scroll.x, SCROLL_X_MIN, SCROLL_X_MAX);
	scroll.y =0;

	if (pStageManager->getCnt() > 180)
		return;

	if (pl.mover == player_move_finish)
		scroll.x += pl.speed.x;
	else
	scroll.x = (float)timer*pStageManager->scroll_speed;


}

//--------------------------------
//  スクロール座標を任意に変更
//--------------------------------
void BG::setScrollPos(float x, float y)
{
	scroll.x = x;
	scroll.y = y;
}


//--------------------------------
//  地形描画
//--------------------------------
void BG::drawTerrain()
{
	draw(TEXNO::MAP_TERRAIN, terr);

}
void BG::drawBack()
{

	spr_bg.draw(0,0, system::SCREEN_WIDTH / 1920.f, system::SCREEN_WIDTH / 1920.f);
}
//--------------------------------
//  BGデータ描画（ソースコード整理したバージョン）
//--------------------------------
void BG::draw(int texNo, char** map)
{
	// 定数の定義（2度と内容が変更されない）
	static const int LOOP_Y = static_cast<int>(system::SCREEN_HEIGHT) / CHIP_SIZE + 5;  // Y方向ループ回数
	static const int LOOP_X = static_cast<int>(system::SCREEN_WIDTH) / CHIP_SIZE + 5;   // X方向ループ回数


																						// 変数の定義（scrollの値によって値が更新される。constがついているので、値が変更できない）
	const int divX = static_cast<int>(scroll.x) / CHIP_SIZE; // division x
	const int divY = static_cast<int>(scroll.y) / CHIP_SIZE; // division y
	const int remX = static_cast<int>(scroll.x) % CHIP_SIZE; // remainder x
	const int remY = static_cast<int>(scroll.y) % CHIP_SIZE; // remainder y

	texture::begin(texNo);

	for (int y = 0; y < LOOP_Y; y++)
	{
		for (int x = 0; x < LOOP_X; x++)
		{
			//(x,y)それぞれマップ配列の 縦横の最大要素数 を超えないようにする
			if (divY + y >= CHIP_NUM_Y || divX + x >= CHIP_NUM_X) continue;

			char chip = map[divY + y][divX + x];    // マップチップのインデックス取得
			if (-1 == chip) continue;               // -1であれば空白

			VECTOR2 pos(static_cast<float>(x * CHIP_SIZE - remX), static_cast<float>(y * CHIP_SIZE - remY));
			const VECTOR2 scale(1, 1);
			const VECTOR2 texPos(static_cast<float>(chip % CHIP_NUM_PER_LINE * CHIP_SIZE), static_cast<float>(chip / CHIP_NUM_PER_LINE * CHIP_SIZE));
			const VECTOR2 texSize(static_cast<float>(CHIP_SIZE), static_cast<float>(CHIP_SIZE));

			if (SCENE_GAME->stop_fg) {
				pos.x -= sway;
			}

			texture::draw(texNo, pos, scale, texPos, texSize);
		}
	}

	texture::end(texNo);
}

//******************************************************************************
//
//      あたり判定
//
//******************************************************************************

//--------------------------------
//  マップ上の指定した座標の部分のマップチップのインデックスを返す
//--------------------------------
int BG::getData(char** map, float x, float y)
{
	int divX = static_cast<int>(x) >> 5;      // x方向のインデックス
	int divY = static_cast<int>(y) >> 5;      // y方向のインデックス

	if (divX < 0 || divX >= CHIP_NUM_X)return -1;
	if (divY < 0 || divY >= CHIP_NUM_Y)return -1;

	return map[divY][divX];
}

//--------------------------------
//  指定した地点の地形の属性を取得
//--------------------------------
BG::TR_ATTR BG::getTerrainAttr(float x, float y)
{
	// インデックス取得
	int index = getData(terr, x, y);

	// インデックスが-1であればTR_NONEを返す
	if (index < 0) return TR_NONE;

	// x方向のインデックス
	int remX = index % CHIP_NUM_PER_LINE;

	// y方向のインデックス
	int divY = index / CHIP_NUM_PER_LINE;

	// 添字の範囲チェック
	assert(remX >= 0 && remX < CHIP_NUM_PER_LINE);
	assert(divY >= 0 && divY < CHIP_LINE_NUM);

	// リターン
	return terrainAttr[divY][remX];
}

//--------------------------------
//  指定されたy座標がマップチップの上側４分の１に含まれるか
//--------------------------------
bool BG::isUpperQuater(float y)
{
	return wrap(static_cast<int>(y), 0, CHIP_SIZE) < CHIP_SIZE >> 2;
}

//--------------------------------
//  下方向のブロックに対するあたり
//--------------------------------
bool BG::isHitDown(float x, float y)
{
	switch (getTerrainAttr(x, y))                       // 地形の属性を取得する
	{
	case TR_ATTR::ALL_BLOCK: return true;               // 全て壁の地形であった

		break;
	case TR_ATTR::DAMAGE_FLOOR: return true;               // 全て壁の地形であった
		break;
	//case TR_ATTR::ALL_BLOCK: return true;               // 全て壁の地形であった
		//break;
	default:

		break;
	}
	return false;                                       // 地形ではなかった場合
}

//--------------------------------
//  全て壁であるブロックかどうか
//--------------------------------
bool BG::isHitAll(float x, float y)
{
	switch (getTerrainAttr(x, y))                       // 地形の属性を取得する
	{
	case TR_ATTR::ALL_BLOCK: return true;               // 全て壁の地形であった

		break;
	case TR_ATTR::DAMAGE_FLOOR: return true;               // 全て壁の地形であった
		break;
	//case TR_ATTR::ALL_BLOCK: return true;               // 全て壁の地形であった
		//break;
	default:

		break;
	}
	return false;
}

//--------------------------------
//  下方向に壁にめり込んでいるかどうかを判定
//--------------------------------
bool BG::isFloor(float x, float y, float width)
{
	for (; width > 0; width -= CHIP_SIZE)               // widthをCHIP_SIZE分減らしていく
	{
		if (isHitDown(x - width, y)) return true;       // 左端から
		if (isHitDown(x + width, y)) return true;       // 右端から
	}
	return isHitDown(x, y);                             // 最後に真ん中で判定
}

//--------------------------------
//  下方向にめり込んでいた場合、y座標を修正する
//--------------------------------
void BG::mapHoseiDown(OBJ2D* obj)
{
	float y = obj->position.y;                          // わかりやすく書くためいったんyに代入
	y -= wrap(y, 0.0f, static_cast<float>(CHIP_SIZE));  // 0.0fからCHIP_SIZEまでの間をラップアラウンドさせる
	obj->position.y = y - ADJUST_Y;                     // 少し浮かせる
	obj->speed.y = (std::min)(obj->speed.y, 0.0f);      // 地面にあたったので速度が止まる
}

//--------------------------------
//  天井にあたっているか
//--------------------------------
bool BG::isCeiling(float x, float y, float width)
{
	for (; width > 0; width -= CHIP_SIZE)               // widthをCHIP_SIZE分減らしていく
	{
		if (isHitAll(x - width, y)) return true;        // 左端から
		if (isHitAll(x + width, y)) return true;        // 右端から
	}
	return isHitAll(x, y);                              // 最後に真ん中で判定
}

//--------------------------------
//  上方向補正処理
//--------------------------------
void BG::mapHoseiUp(OBJ2D* obj)
{
	float y = obj->position.y - obj->size.y;
	y -= wrap(y, static_cast<float>(-CHIP_SIZE), 0.0f);
	obj->position.y = y + obj->size.y;
	if (obj->speed.y < 0)
		obj->speed.y = 0.0f;                    // 天井にあたったので止まる
}

//--------------------------------
//  横方向に壁にあたっているかどうか
//--------------------------------
bool BG::isWall(float x, float y, float height)
{

	for (; height > 0; height -= CHIP_SIZE) {
		if (isHitAll(x, y - height))
			return true;
	}

	return isHitAll(x, y);

}


//--------------------------------
//  右方向補正処理
//--------------------------------
void BG::mapHoseiRight(OBJ2D* obj)
{
	float x = obj->position.x + obj->size.x;
	x -= wrap(x, 0.0f, static_cast<float>(CHIP_SIZE));
	obj->position.x = x - obj->size.x - ADJUST_X;
	obj->speed.x = 0.0f;
}

//--------------------------------
//  左方向補正処理
//--------------------------------
void BG::mapHoseiLeft(OBJ2D* obj)
{
	float x = obj->position.x - obj->size.x;
	x -= wrap(x, static_cast<float>(-CHIP_SIZE), 0.0f);
	obj->position.x = x + obj->size.x + ADJUST_X;
	obj->speed.x = 0.0f;
}

//画面揺れ
#define SWAY_PIXEL 2
void BG::GameSway(VECTOR2 pos)
{

	sway *= -1;
	pos.x += sway * SWAY_PIXEL;

}
