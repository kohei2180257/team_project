#pragma once

//******************************************************************************
//
//
//      シーン
//
//
//******************************************************************************

#define TEXTOUT_POSX  690
#define TEXTOUT_POSY  670
#define TEXTOUT_SCALE 2.0f


struct SELECT_DATA
{
	GameLib::SpriteData*		pSprData;
	VECTOR2			position;
	VECTOR2			scale;

	std::string		charString;
	int				size;
	VECTOR4			color;
};


//==============================================================================
//
//      Sceneクラス
//
//==============================================================================
class Scene
{
protected:
	int stop_cnt;
    int state;          // 状態
    Scene* nextScene;   // 次のシーン
	int select;
	bool stick_now;
	static const int SELECT_MAX = 16;
	SELECT_DATA selectData[SELECT_MAX];

public:
	int timer;          // タイマー

	bool stop_fg;

    Scene* execute();   // 実行処理

    virtual void init();		// 初期化処理
    virtual void uninit();   // 終了処理
    virtual void update() {};   // 更新処理
    virtual void draw()   {};   // 描画処理

    void changeScene(Scene *scene) { nextScene = scene; }   // シーン変更処理
    Scene *getScene() const { return nextScene; }           // nextSceneのゲッター
	void stop_fg_on() { stop_fg = true; return; }
	int getSelect() { return select; }
	int getState() { return state; }

};

//******************************************************************************
//
//
//      シーン管理
//
//
//******************************************************************************

//==============================================================================
//
//      SceneManagerクラス
//
//==============================================================================
class SceneManager
{
public:
    void execute(Scene *);  // 実行処理
};



//******************************************************************************

