#include"all.h"

#define DOORSIZE 60
//クリア判定用
void DoorMove00(OBJ2D*obj)
{

	obj->size = VECTOR2(DOORSIZE, DOORSIZE);
	//ゴール判定
	
}



MOVER DoorMoveArray[] =
{
	DoorMove00,
};



void DoorManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化

	STAGE_SCRIPT*pScript = pStageManager->getStageScript();


	//扉の読み込み
	char** mapDoor = new char*[pBGManager->CHIP_NUM_Y];

	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
		mapDoor[i] = new char[pBGManager->CHIP_NUM_X];
		SecureZeroMemory(mapDoor[i], sizeof(char)*pBGManager->CHIP_NUM_X);
	}

	if (!pBGManager->loadMapData(pScript->fileNameDoor, mapDoor))
	{
		assert(!"扉データ読み込み失敗");
	}

	// アイテム配置
	for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
	{
		for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
		{
			const int DoorIndex = mapDoor[y][x];
			if (DoorIndex < 0) continue;
			searchSet(DoorMoveArray[DoorIndex], VECTOR2(
				static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
				static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
			);

		}
	}

	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
	{
		delete[] mapDoor[i];
	}
	delete[] mapDoor;
}

void DoorManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの初期化
}

void DoorManager::draw()
{
	OBJ2DManager::draw();   // OBJ2DManagerの初期化
	
}


