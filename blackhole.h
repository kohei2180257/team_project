#pragma once

// ラベル定義
#define HOLE_MAX		10

// EffectManagerクラス
class HoleManager : public OBJ2DManager, public Singleton<HoleManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return HOLE_MAX; }
};

#define pHoleManager	(HoleManager::getInstance())

void hole_move(OBJ2D*obj);