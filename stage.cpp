#include"all.h"

using namespace GameLib::input;


// ステージデータ
STAGE_SCRIPT* stage_script[] = {

	stage1_script,
	stage2_script,
	stage3_script,
	stage4_script,
	stage5_script,
	stage6_script,


	nullptr,
};

#define SCROOL_SPEED_1 3
#define SCROOL_SPEED_2 4
#define SCROOL_SPEED_3 5

void StageManager::init()
{
	state=0;
	timer=0;
	cnt=0;
	str_start;

	if(stageNo==0)
		scroll_speed = SCROOL_SPEED_1;
	else if (stageNo < 5)//5ステージから速度アップ
		scroll_speed = SCROOL_SPEED_2;
	else 
		scroll_speed = SCROOL_SPEED_3;

	pScript=nullptr;
}

void StageManager::update()
{

	str_start = nullptr;

	// ステージクリア対応にする
	switch (state) {

	case 0:			// ステージ初期化


		pScript = stage_script[stageNo];



		pBGManager->init();

		pGimmickTerrManager->init();

		pPlayerManager->init();

		pItemManager->init();

		pDoorManager->init();

		pEnemyManager->init();

		pEnemyShotManager->init();

		pBlackoutManager->init();

		pEffectManager->init();

		pHoleManager->init();

		pUiManager->init();

		pEffectManager->searchSet(effect_move_player_spawn, VECTOR2(pPlayerManager->getOBJ2D(0)->position.x, pPlayerManager->getOBJ2D(0)->position.y-80));
		GameLib::sound::play(SOUND::SOUND_SPAWN);

		state++;


		//break;
	case 1:			// ステージ進行
		timer++;

		
		
		{
			OBJ2D* pl = pPlayerManager->getOBJ2D(0);

			// ステージクリアチェック
			if (pBitFlg->Flg_Check(pl->iWork[player::goalFlg], player::isGoal)||pBitFlg->Flg_Check(pl->iWork[player::goalFlg], player::isDead))
			{
				//死んでたら
				if (pBitFlg->Flg_Check(pl->iWork[player::goalFlg], player::isDead)) {
					pl->clear();
					SCENE_RESULT->isClear = false;
					pBlackoutManager->searchSet(blackout_move00, VECTOR2(0,0));
					state++;
				}
				//ゴールしてたら
				else if (++cnt > 200) {
					SCENE_RESULT->isClear = true;
					pBlackoutManager->searchSet(blackout_move00, VECTOR2(0,0));
					state++;
				}

			}
		}
		break;

	case 2:			// ステージ進行
		//暗転
		pBlackoutManager->update();


		if (pBlackoutManager->getOBJ2D(0)->flg)//終了したら
		{
			SCENE_GAME->changeScene(SCENE_RESULT);    //リザルトに切り替え
			state = 0;
		}
		break;
	}



	if (timer < START_TIME)
	{

		pItemManager->update();

		pDoorManager->update();

		pGimmickTerrManager->update();

		pPlayerManager->update();
		
		pHoleManager->update();

		pEffectManager->update();

		pUiManager->update();
	}

	else
	{
		pBGManager->update();

		pGimmickTerrManager->update();

		pItemManager->update();

		pDoorManager->update();

		pHoleManager->update();

		pPlayerManager->update();

		pEnemyManager->update();

		pEnemyShotManager->update();

		pEffectManager->update();

		pUiManager->update();
	}
}

void StageManager::draw()
{

	if (timer < START_TIME)
	{
		pBGManager->drawBack();

		pBGManager->drawTerrain();

		pGimmickTerrManager->draw();

		pPlayerManager->draw();

		pEffectManager->draw();

		pDoorManager->draw();

		pItemManager->draw();

		pEnemyShotManager->draw();

		pEnemyManager->draw();


		pHoleManager->draw();

		pUiManager->draw();

		pBlackoutManager->draw();


	}
	
	else
	{
		pBGManager->drawBack();

		pBGManager->drawTerrain();

		pGimmickTerrManager->draw();

		pPlayerManager->draw();

		pEffectManager->draw();

		pDoorManager->draw();

		pItemManager->draw();

		pEnemyManager->draw();

		pEnemyShotManager->draw();

		pHoleManager->draw();

		pUiManager->draw();

		pBlackoutManager->draw();
	}

}