#pragma once

//==============================================================================
//
//     BitFlgクラス
//
//==============================================================================

class BitFlg : public Singleton<BitFlg>
{
public:
	//  int & flg       変更するフラグ変数
	//  int flgLabel    フラグのラベル
	void Flg_On(int & flg, int flgLabel) { flg |= flgLabel; }					// フラグオン
	void Flg_Off(int & flg, int flgLabel) { flg &= ~flgLabel; }					// フラグオフ
	void Flg_Change(int& flag, int flgLabel) { flag ^= flgLabel; }				// フラグ反転
	bool Flg_Check(int flag, int flglabel) { return ((flag & flglabel) != 0); }	// フラグ確認
	void All_Flg_Off(int & flag) { flag = 0; }									// フラグ一括オフ

private:

};


// インスタンス取得マクロ
#define pBitFlg		(BitFlg::getInstance())


