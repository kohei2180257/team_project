#pragma once

#define ITEM_MAX 100

enum
{
	ITEM_CLEAR = 0,
	ITEM_THROUGH,
	ITEM_BOMB,
	ITEM_PRISE,
};





void ItemMove00(OBJ2D*obj);
void ItemMove01(OBJ2D*obj);
void ItemMove02(OBJ2D*obj);
void ItemMove03(OBJ2D*obj);

class ItemManager : public OBJ2DManager, public Singleton<ItemManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return ITEM_MAX; }
};

#define pItemManager	(ItemManager::getInstance())



