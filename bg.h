#pragma once

//******************************************************************************
//
//
//      bg.h
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "./GameLib/template.h"

#define SCROLL_SPEED (3)

//==============================================================================
//
//      BGクラス
//
//==============================================================================

class BG : public Singleton<BG>
{
public:
	//------< 定数 >------------------------------------------------------------
	static const int CHIP_SIZE = 32;                          // %演算子を使用するためint型を使用する
	static const int CHIP_LINE_NUM = 4;                       // マップチップが４行
	static const int CHIP_NUM_PER_LINE = 4;                   // マップチップの１列が８個

	static constexpr float SCROLL_MERGIN_X = 300.f;         // この数値より画面端に近づいたらスクロールする（横）
	static constexpr float SCROLL_MERGIN_Y = 20.f;         // この数値より画面端に近づいたらスクロールする（縦）
	static constexpr float ADJUST_Y = 0.125f;               // あたり判定での位置調整用（縦）
	static constexpr float ADJUST_X = 0.0125f;              // あたり判定での位置調整用（横）
	static constexpr float AREA_LIMIT = 256.0f;             // これ以上エリアの外に出たらOBJ2Dが消滅する

	int sway = 10;
	int timer;
public:
	//------<マップ描画用>------------------------------------------------------
	int CHIP_NUM_X;                                         // マップの横方向のチップ数
	int CHIP_NUM_Y;                                         // マップの縦方向のチップ数

	float WIDTH;                                            // マップの幅（ドット）
	float HEIGHT;                                           // マップの高さ（ドット）

															// 地形(Terrain)の属性
	enum TR_ATTR
	{
		TR_NONE = -1,   //-1:何もなし
		ALL_BLOCK,      // 0:四方ブロック
		DAMAGE_FLOOR,   // 1:ダメージ床

	};



private:

	// 地形チップテクスチャの各部分の属性を定義する
	const TR_ATTR terrainAttr[CHIP_LINE_NUM][CHIP_NUM_PER_LINE] = {
		{ TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK},
		{ TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK},
		{ TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK},
		{ TR_ATTR::ALL_BLOCK,   TR_ATTR::DAMAGE_FLOOR,   TR_ATTR::ALL_BLOCK,   TR_ATTR::TR_NONE },
	};

	

	//------< 変数 >------------------------------------------------------------
	VECTOR2 scroll;                                         // 現在表示されている左上の地点の座標

	char** terr;                                            // 地形データ

public:
	BG();
	~BG();

	// 初期化
	void init();

	// 更新
	void update();

	// 描画
	void drawTerrain();                                     // 地形描画
	void drawBack();
															// スクロール位置取得
	float getScrollX() { return scroll.x; }
	float getScrollY() { return scroll.y; }
	const VECTOR2& getScrollPos() { return scroll; }

	// マップ全体のサイズを取得
	bool allMapCount(const char* file_name);

	// マップデータのロード
	bool BG::loadMapData(const char* file_name, char** map);

	// scrollセット
	void setScrollPos(float x, float y);

	void GameSway(VECTOR2 pos);//画面揺れ


	// あたり判定

	// 下方向
	bool isFloor(float, float, float);      // 床にめり込んでいるか
	void mapHoseiDown(OBJ2D*);              // 下方向補正処理

											// 上方向
	bool isCeiling(float, float, float);    // 天井にあたっているか
	void mapHoseiUp(OBJ2D*);                // 上方向補正処理

											// 横方向
	bool isWall(float, float, float);       // 横方向に壁にめり込んでいるか
	void mapHoseiRight(OBJ2D*);             // 右方向補正処理
	void mapHoseiLeft(OBJ2D*);              // 左方向補正処理

											// 抵抗

	TR_ATTR getTerrainAttr(float, float);

	// char(*getTerrain())[BG::CHIP_NUM_X]{ return terrain; }

private:
	// クリア
	void clear();

	// 地形データ、背景データ削除
	void mapDelete();									//	newしたものはdelete

														// BG、Terrain共通の描画関数
	void draw(int, char**);  // 描画処理（ソースコード整理したバージョン）

							 // マップスクロール用
	void scrollMap();// マップスクロールの値更新

	bool isHitDown(float, float);
	bool isHitAll(float, float);

	bool isUpperQuater(float);
	int getData(char**, float, float);
};

//------< インスタンス取得 >-----------------------------------------------------

static BG* const pBGManager = BG::getInstance();

//******************************************************************************

