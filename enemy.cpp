#include"all.h"

#include <string>
using namespace GameLib;

#define BOM_STOP_FRAME 8
#define VACUUM_DISTANCE 700


namespace {
	AnimeData animeEnemyFloat[] = {
		{ &spr_enemy_float00, 4 },
		{ &spr_enemy_float01, 4 },
		{ &spr_enemy_float02, 4 },
		{ &spr_enemy_float03, 4 },
		{ nullptr, -1 },// 終了フラグ
	};
	AnimeData animeEnemyMove[] = {
		{ &spr_enemy_move00, 10 },
		{ &spr_enemy_move01, 10 },
		{ &spr_enemy_move02, 10 },
		{ &spr_enemy_move03, 10 },
		{ nullptr, -1 },// 終了フラグ
	}; 
	AnimeData animeEnemyDash[] = {
		{ &spr_enemy_dash00, 6},
		{ &spr_enemy_dash01, 6},
		{ &spr_enemy_dash02, 6},
		{ &spr_enemy_dash03, 6},
		{ nullptr, -1 },// 終了フラグ
	}; 
	AnimeData animeEnemyShot[] = {
		{ &spr_enemy_shot00, 4 },
		{ &spr_enemy_shot01, 4 },
		{ &spr_enemy_shot02, 4 },
		{ &spr_enemy_shot03, 4 },
		{ nullptr, -1 },// 終了フラグ
	};
	AnimeData animeEnemyShotAttack[] = {
		{ &spr_enemy_shot_begin00,4 },
		{ &spr_enemy_shot_begin01,4 },
		{ &spr_enemy_shot_begin02,4 },
		{ &spr_enemy_shot_begin03,4 },
		{ &spr_enemy_shot_begin04,4 },
		{ &spr_enemy_shot_begin05,4 },
		{ &spr_enemy_shot_begin06,4 },
		{ &spr_enemy_shot_begin07,4 },
		{ &spr_enemy_shot_begin08,4 },
		{ nullptr, -1 },// 終了フラグ
	};
	AnimeData animeEnemyShot2[] = {
		{ &spr_enemy_shot04, 4 },
		{ &spr_enemy_shot05, 4 },
		{ &spr_enemy_shot06, 4 },
		{ &spr_enemy_shot07, 4 },
		
		{ nullptr, -1 },// 終了フラグ
	};
	AnimeData animeEnemyShotAttack2[] = {
		{ &spr_enemy_shot2_begin00, 4 },
		{ &spr_enemy_shot2_begin01, 4 },
		{ &spr_enemy_shot2_begin02, 4 },
		{ &spr_enemy_shot2_begin03, 4 },
		{ &spr_enemy_shot2_begin04,	4 },
		{ &spr_enemy_shot2_begin05,	4 },
		{ &spr_enemy_shot2_begin06,	4 },
		{ &spr_enemy_shot2_begin07,	4 },
		{ &spr_enemy_shot2_begin08,	4 },
		{ nullptr, -1 },// 終了フラグ
	};

	AnimeData animeEnemySwitch[] = {
		{ &spr_switch00, 1 },
		{ &spr_switch01, 1 },
		{ &spr_switch02, 1 },
		

		{ nullptr, -1 },// 終了フラグ
	};
	AnimeData animeEnemySwitch2[] = {
		{ &spr_switch03, 1 },
		{ &spr_switch04, 1 },
		{ &spr_switch05, 1 },


		{ nullptr, -1 },// 終了フラグ
	};

	AnimeData animeParticle[] = {
		{ &spr_particle00, 8 },
		{ &spr_particle01, 8 },
		{ &spr_particle02, 8 },
		{ &spr_particle03, 8 },


		{ nullptr, -1 },// 終了フラグ
	};

	AnimeData animeSpark[] = {
		{ &spr_spark00, 3 },
		{ &spr_spark01, 3 },
		{ &spr_spark02, 3 },
		{ &spr_spark03, 3 },
		{ &spr_spark04, 3 },
		{ &spr_spark05, 3 },
		{ &spr_spark06, 3 },
		{ &spr_spark07, 3 },
		{ &spr_spark08, 3 },
		{ &spr_spark09, 3 },
		{ &spr_spark10, 3 },
		{ &spr_spark11, 3 },
		{ nullptr, -1 },// 終了フラグ
	};

}


//一定距離になるとブラックホールに吸い込まれる
void ToBlackHole(OBJ2D*obj)
{
	float distanceToBlackHole = obj->position.x - pHoleManager->getOBJ2D(0)->position.x;

	if (distanceToBlackHole < VACUUM_DISTANCE)
	{
		obj->velocity.x += 0.2f;
		obj->speed.x -= (obj->velocity.x*obj->velocity.x);
	}
}



//漂うだけの敵
void enemy_move00(OBJ2D*obj)
{
	switch (obj->state) {

	case 0:
		obj->size = VECTOR2(ENEMY00_SIZEX, ENEMY00_SIZEY);
		obj->scale *= ENEMY_SCLAE;
		obj->center = obj->position;
		obj->wait = ENEMY_WAIT;
		obj->state++;
		//break;

	case 1:


		Floating_Move(obj);
		obj->timer++;
		AnimeData*anime = nullptr;
		anime=animeEnemyFloat;
		obj->animeUpdate(anime);
		break;
	}
	ToBlackHole(obj);

	if (judgeCheck2(*obj, *pHoleManager->getOBJ2D(0)))
		obj->clear();
}



//上下に動く敵
void enemy_move01(OBJ2D*obj)
{


	ToBlackHole(obj);
	AnimeData*anime = nullptr;
	anime = animeEnemyMove;
	obj->animeUpdate(anime);

	switch (obj->state) {

	case 0:
		obj->size = VECTOR2(ENEMY01_SIZEX, ENEMY01_SIZEY);
		obj->scale *= ENEMY_SCLAE;
		obj->state++;
		obj->speed.y = ENEMY01_SPEED;
		obj->cnt= ENEMY01_MOVECNT;//二秒ごとに方向転換
		//break;

	case 1:

		obj->position.y += obj->speed.y;
		if (--obj->cnt <= 0)
		{
			obj->cnt = ENEMY01_MOVECNT;
			obj->state++;

		}
		break;
	case 2:
		obj->position.y -= obj->speed.y;
		if (--obj->cnt <= 0)
		{
			obj->cnt = ENEMY01_MOVECNT;
			obj->state--;

		}
		break;
	}
	ToBlackHole(obj);
	if (judgeCheck2(*obj, *pHoleManager->getOBJ2D(0)))
		obj->clear();
}


//向かってくる敵
void enemy_move02(OBJ2D*obj)
{
	ToBlackHole(obj);
	AnimeData*anime = nullptr;
	anime = animeEnemyDash;
	obj->animeUpdate(anime);

	switch (obj->state) {

	case 0:
		obj->size = VECTOR2(ENEMY02_SIZEX, ENEMY02_SIZEY);
		obj->scale *= ENEMY_SCLAE;
		obj->state++;
		obj->speed.x = -ENEMY02_SPEED;
		obj->cnt = ENEMY02_MOVECNT;
		//break;

	case 1:

		if(obj->position.x - pPlayerManager->getOBJ2D(0)->position.x < ENEMY_MOVE_DISTANCE)
			obj->state++;
		
		break;

	case 2:
		obj->position.x += obj->speed.x;
		break;
	}
	ToBlackHole(obj);
	if (judgeCheck2(*obj, *pHoleManager->getOBJ2D(0)))
		obj->clear();
}



//横に弾を撃ってくる敵
void enemy_move03(OBJ2D*obj)
{
	AnimeData*anime = nullptr;


	switch (obj->state) {

	case 0:
		obj->size = VECTOR2(ENEMY03_SIZEX, ENEMY03_SIZEX);
		obj->scale *= ENEMY_SCLAE;
		obj->center = obj->position;
		obj->wait = ENEMY_WAIT;
		obj->cnt = ENEMY03_MOVECNT;
		obj->param = 0;//フレームカウントとして使用
		obj->state++;

		//break;

	case 1:

		obj->timer++;

		anime = animeEnemyShot;

		obj->animeUpdate(anime);

		if ((--obj->cnt < 0) && (obj->position.x- pPlayerManager->getOBJ2D(0)->position.x < ENEMY_MOVE_DISTANCE))
			obj->state++;

		break;

	case 2:
		anime = animeEnemyShotAttack;
		if (++obj->param == ENEMY03_SHOTCNT)
		pEnemyShotManager->searchSet(enemyshot_move00, VECTOR2(obj->position.x - 10, obj->position.y - 20));

		if (obj->animeUpdate(anime))
		{
			obj->param = 0;
			obj->cnt = ENEMY03_MOVECNT;
			obj->state--;
		}

		break;
	}
	ToBlackHole(obj);
	Floating_Move(obj);

	if (judgeCheck2(*obj, *pHoleManager->getOBJ2D(0)))
		obj->clear();
}




//縦に弾を撃ってくる敵
void enemy_move04(OBJ2D*obj)
{
	AnimeData*anime = nullptr;


	switch (obj->state) {

	case 0:
		obj->size = VECTOR2(ENEMY04_SIZEX, ENEMY04_SIZEY);
		obj->scale *= ENEMY_SCLAE;
		obj->center = obj->position;
		obj->wait = ENEMY_WAIT;
		obj->cnt = ENEMY04_MOVECNT;
		obj->param=0;//フレームカウントとして使用
		obj->state++;

		//break;

	case 1:

		obj->timer++;

		anime = animeEnemyShot2;

		obj->animeUpdate(anime);

		if ((--obj->cnt < 0) && (obj->position.x - pPlayerManager->getOBJ2D(0)->position.x < ENEMY_MOVE_DISTANCE))
			obj->state++;

		break;

	case 2:
		anime = animeEnemyShotAttack2;
		if (++obj->param == ENEMY04_SHOTCNT)
		pEnemyShotManager->searchSet(enemyshot_move01, obj->position);

		if (obj->animeUpdate(anime))
		{
			obj->param = 0;
			obj->cnt = ENEMY04_MOVECNT;
			obj->state--;
		}

		break;
	}
	ToBlackHole(obj);
	Floating_Move(obj);

	if (judgeCheck2(*obj, *pHoleManager->getOBJ2D(0)))
	{
		sound::play(SOUND::SOUND_DESTROY);

		obj->clear();
	}
}




//スイッチ
void enemy_move05(OBJ2D*obj)
{

	AnimeData*anime = nullptr;

	switch (obj->state) {

	case 0:
		obj->size = VECTOR2(ENEMY05_SIZEX, ENEMY05_SIZEY);
		obj->scale *= ENEMY_SWITCH_SCLAE;
		obj->type = enemy::Type::GIMMICK;
		obj->center = obj->position;
		obj->wait = ENEMY_WAIT;
		obj->data = &spr_switch00;
		obj->state++;
		//break;

	case 1:

		Floating_Move(obj);

		obj->timer++;
		anime = animeEnemySwitch;

		if (judgeCheck2(*pPlayerManager->getOBJ2D(0), *obj) && pPlayerManager->getOBJ2D(0)->moveFlg)
		{
			SCENE_GAME->HitStopTrg(BOM_STOP_FRAME);

			obj->state++;
		}
		break;


	case 2:


		anime = animeEnemySwitch2;

		if (obj->animeUpdate(anime))
		{
			pGimmickTerrManager->erase_flg = true;
			obj->clear();

		}
		break;
	}

	//スイッチだけブラックホールに吸い込まれにくく（難易度調整のため）
	float distanceToBlackHole = obj->position.x - pHoleManager->getOBJ2D(0)->position.x;

	if (distanceToBlackHole < 500)
	{
		obj->velocity.x += 0.2f;
		obj->speed.x -= (obj->velocity.x*obj->velocity.x);
	}
}

//敵が死んだ後の粒子
void enemy_move06(OBJ2D*obj)
{

	AnimeData*anime = nullptr;

	switch (obj->state) {

	case 0:
		obj->size = VECTOR2(ENEMY06_SIZEX, ENEMY06_SIZEY);
		obj->type = enemy::Type::GIMMICK;
		obj->center = obj->position;
		obj->color.w = 0.5f;
		obj->wait = ENEMY_WAIT;
		obj->state++;
		//break;

	case 1:

		Floating_Move(obj);

		obj->timer++;
		anime = animeParticle;

		
		obj->animeUpdate(anime);
		
		break;
	}

	if (judgeCheck2(*obj, *pHoleManager->getOBJ2D(0)))
		obj->clear();

	ToBlackHole(obj);

}


//ビリビリ床
void enemy_move07(OBJ2D*obj)
{

	AnimeData*anime = nullptr;

	switch (obj->state) {

	case 0:
		obj->size = VECTOR2(ENEMY07_SIZEX, ENEMY07_SIZEY);
		obj->scale *= ENEMY_SCLAE;
		obj->type = enemy::Type::GIMMICK;
		obj->center = obj->position;
		obj->wait = ENEMY_WAIT;
		obj->state++;
		//break;

	case 1:


		anime = animeSpark;


		obj->animeUpdate(anime);

		break;
	}


}

//スイッチその2
void enemy_move08(OBJ2D*obj)
{

	AnimeData*anime = nullptr;

	switch (obj->state) {

	case 0:
		obj->size = VECTOR2(ENEMY08_SIZEX, ENEMY08_SIZEY);
		obj->scale *= ENEMY_SWITCH_SCLAE;
		obj->type = enemy::Type::GIMMICK;
		obj->center = obj->position;
		obj->wait = ENEMY_WAIT;
		obj->state++;
		obj->damageTimer;
		obj->data = &spr_switch00;
		//break;

	case 1:

		Floating_Move(obj);

		obj->timer++;
		anime = animeEnemySwitch;

		if (judgeCheck2(*pPlayerManager->getOBJ2D(0), *obj) && pPlayerManager->getOBJ2D(0)->moveFlg)
		{
			SCENE_GAME->HitStopTrg(BOM_STOP_FRAME);

			obj->state++;
		}
		break;


	case 2:


		anime = animeEnemySwitch2;

		if (obj->animeUpdate(anime))
		{
			pGimmickTerrManager->erase_flg2 = true;
			obj->clear();

		}
		break;
	}
	float distanceToBlackHole = obj->position.x - pHoleManager->getOBJ2D(0)->position.x;

	if (distanceToBlackHole < 500)
	{
		obj->velocity.x += 0.2f;
		obj->speed.x -= (obj->velocity.x*obj->velocity.x);
	}
}

MOVER EnemyMoveArray[] = { enemy_move00 ,enemy_move01,enemy_move02, enemy_move03,enemy_move04,enemy_move05,enemy_move07,enemy_move08 };



void EnemyManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化

	STAGE_SCRIPT*pScript = pStageManager->getStageScript();

	// 敵データの読み込み
	char** mapEnemy = new char*[pBGManager->CHIP_NUM_Y];
	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
		mapEnemy[i] = new char[pBGManager->CHIP_NUM_X];
		SecureZeroMemory(mapEnemy[i], sizeof(char)*pBGManager->CHIP_NUM_X);

	}

	if (!pBGManager->loadMapData(pScript->fileNameEnemy, mapEnemy))
	{
		assert(!"敵データ読み込み失敗");
	}

	// 敵配置
	for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
	{
		for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
		{
			const int enemyIndex = mapEnemy[y][x];
			if (enemyIndex < 0) continue;
			OBJ2D* pEnemy = searchSet(EnemyMoveArray[enemyIndex], VECTOR2(
				static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
				static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
			);

		}
	}

	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
	{
		delete[] mapEnemy[i];
	}
	delete[] mapEnemy;
}

void EnemyManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの更新
}

void EnemyManager::draw()
{
	OBJ2DManager::draw();   // OBJ2DManagerの描画

	//if (DEBUG_NOW) {
	//	for (auto &obj : *pEnemyManager) {
	//		primitive::rect(obj.position.x - obj.size.x - pBGManager->getScrollPos().x, obj.position.y - obj.size.y - pBGManager->getScrollPos().y, obj.size.x * 2, obj.size.y * 2, 0, 0, 0, 1, 0, 0, 0.3f);
	//	}
	//}
}
