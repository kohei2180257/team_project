#include"all.h"


GameLib::AnimeData animeDemo[] = {
	{ &spr_demo00, 450 },
	{ &spr_demo02, 450 },
	{ &spr_demo04, 450 },
	{ &spr_demo06, 450 },

	{ nullptr, -1 },// 終了フラグ

};

GameLib::AnimeData animeDemo2[] = {
	{ &spr_demo01, 450 },
	{ &spr_demo03, 450 },
	{ &spr_demo05, 450 },
	{ &spr_demo07, 450 },
	{ nullptr, -1 },// 終了フラグ

};


#define DEMO_REFLESH_CNT 450
#define DEMO_COLOR_ALPHA 200.0f


//クリア判定用
void DemoMove01(OBJ2D*obj)
{
	GameLib::AnimeData* anime = nullptr;

	if (++obj->timer % DEMO_REFLESH_CNT == 0)
		obj->state--;

	switch (obj->state)
	{
	case 0:
		obj->position = VECTOR2(GameLib::system::SCREEN_WIDTH / 2, GameLib::system::SCREEN_HEIGHT / 2);

		obj->color.w = 0;

		obj->state++;
		break;

	case 1:

		obj->color.w += 1.0f / DEMO_COLOR_ALPHA;


		

		if (obj->color.w > 1)
			obj->color.w = 1;

		break;
	}

	anime = animeDemo2;

	if (obj->animeUpdate(anime))
		obj->clear();
}
//クリア判定用
void DemoMove00(OBJ2D*obj)
{
	GameLib::AnimeData* anime = nullptr;



	obj->position = VECTOR2(GameLib::system::SCREEN_WIDTH / 2, GameLib::system::SCREEN_HEIGHT / 2);
	anime = animeDemo;

	if (obj->animeUpdate(anime))
		obj->clear();

}

void DemoManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化

	obj_w[1].mover = DemoMove00;
	obj_w[0].mover = DemoMove01;

	
}

void DemoManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの初期化
}

void DemoManager::draw()
{
	OBJ2DManager::draw_not_scroll();   // OBJ2DManagerの初期化

}


