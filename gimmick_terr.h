#pragma once


//------< インクルード >---------------------------------------------------------
#include "./GameLib/template.h"

//==============================================================================
//
//      BGクラス
//
//==============================================================================



class GimmickTerrManager : public OBJ2DManager, public Singleton<GimmickTerrManager>
{
public:
	//------< 定数 >------------------------------------------------------------
	static const int CHIP_SIZE = 96;                          // %演算子を使用するためint型を使用する
	static const int CHIP_LINE_NUM = 4;                       // マップチップが４行
	static const int CHIP_NUM_PER_LINE = 8;                   // マップチップの１列が８個

	static const int MINI_MAP_POS_X = 100;					// ミニマップを0, 0の位置からどれだけずらした位置に表示するか
	static const int MINI_MAP_POS_Y = 90;


	static const int CHIP_MINI_SCALING = 16;				// CHIP_SIZEをこの値で割ることでミニマップ用のサイズに変更する


	static constexpr float SCROLL_MERGIN_X = 0.f;         // この数値より画面端に近づいたらスクロールする（横）
	static constexpr float SCROLL_MERGIN_Y = 0.f;         // この数値より画面端に近づいたらスクロールする（縦）
	static constexpr float ADJUST_Y = 0.125f;               // あたり判定での位置調整用（縦）
	static constexpr float ADJUST_X = 0.0125f;              // あたり判定での位置調整用（横）
	static constexpr float AREA_LIMIT = 256.0f;             // これ以上エリアの外に出たらOBJ2Dが消滅する

public:
	//------<マップ描画用>------------------------------------------------------
	int CHIP_NUM_X;                                         // マップの横方向のチップ数
	int CHIP_NUM_Y;                                         // マップの縦方向のチップ数

	float WIDTH;                                            // マップの幅（ドット）
	float HEIGHT;                                           // マップの高さ（ドット）

															// 地形(Terrain)の属性
	enum TR_ATTR
	{
		TR_NONE = -1,   // -1:何もなし
		ALL_BLOCK,      //  0:四方ブロック
		ALL_BLOCK2,      //  0:四方ブロック

	};



private:

	// 地形チップテクスチャの各部分の属性を定義する
	const TR_ATTR terrainAttr[CHIP_LINE_NUM][CHIP_NUM_PER_LINE] = {
		{ TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK2,   TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK, TR_ATTR::ALL_BLOCK, TR_ATTR::ALL_BLOCK, TR_ATTR::ALL_BLOCK, },
		{ TR_ATTR::TR_NONE,		TR_ATTR::TR_NONE,	  TR_ATTR::TR_NONE,		TR_ATTR::TR_NONE,	  TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE, },
		{ TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE, },
		{ TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE, },
	};

	
	//------< 変数 >------------------------------------------------------------
	VECTOR2 scroll;                                         // 現在表示されている左上の地点の座標

	char** terr;                                            // 地形データ
	

public:
    GimmickTerrManager();

	~GimmickTerrManager();
	// 初期化
	void init();

	// 更新
	void update();
	void draw();  // 描画処理（ソースコード整理したバージョン）

	
															// スクロール位置取得
	float getScrollX() { return scroll.x; }
	float getScrollY() { return scroll.y; }
	const VECTOR2& getScrollPos() { return scroll; }

	// マップ全体のサイズを取得
	bool allMapCount(const char* file_name);


	// マップデータのロード
	bool GimmickTerrManager::loadMapData(const char* file_name, char** map);

	// 下方向
	bool isFloor(float, float, float);      // 床にめり込んでいるか
	void mapHoseiDown(OBJ2D*);              // 下方向補正処理

											// 上方向
	bool isCeiling(float, float, float);    // 天井にあたっているか
	void mapHoseiUp(OBJ2D*);                // 上方向補正処理

											// 横方向
	bool isWall(float, float, float);       // 横方向に壁にめり込んでいるか
	void mapHoseiRight(OBJ2D*);             // 右方向補正処理
	void mapHoseiLeft(OBJ2D*);              // 左方向補正処理

											// 抵抗

	TR_ATTR getTerrainAttr(float, float);

	int getSize() { return 1000; }


	bool erase_flg;											//消滅
	bool erase_flg2;											//消滅

private:
	// クリア
	void clear();

	// 地形データ、背景データ削除
	void mapDelete();									//	newしたものはdelete

														// BG、Terrain共通の描画関数


	bool isHitDown(float, float);
	bool isHitAll(float, float);

	bool isUpperQuater(float);
	int getData(char**, float, float);


};

//------< インスタンス取得 >-----------------------------------------------------

#define pGimmickTerrManager  (GimmickTerrManager::getInstance())

//******************************************************************************

