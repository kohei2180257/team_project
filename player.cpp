//******************************************************************************
//
//
//      プレイヤークラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

#include <string>
#include <bitset>
#include <DirectXMath.h>

using namespace GameLib;





//******************************************************************************
//
//      プレイヤー移動処理
//
//******************************************************************************


//------< プレイヤーのアニメデータ >-----------------------------------------------
namespace
{

	//------< プレイヤーのアニメデータ >------------------------------------------

	AnimeData animePlayerN[] = {
		{ &spr_player00, 5 },
		{ &spr_player01, 5 },
		{ &spr_player02, 5 },
		{ &spr_player03, 5 },
		{ &spr_player04, 5 },
		{ nullptr, -1 },// 終了フラグ

	};

	//攻撃モーション
	AnimeData animePlayerA[] = {
		{ &spr_player05, 2 },
		{ &spr_player06, 2 },
		{ &spr_player07, 2 },
		{ &spr_player08, 3 },
		{ &spr_player09, 3 },
		{ &spr_player10, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ &spr_player12, 3 },
		{ &spr_player11, 3 },
		{ nullptr, -1 },// 終了フラグ

	};
	//強化版
	AnimeData animePlayerA2[] = {
		{ &spr_player18, 2 },
		{ &spr_player19, 2 },
		{ &spr_player20, 2 },
		{ &spr_player21, 3 },
		{ &spr_player22, 3 },
		{ &spr_player23, 3 },
		{ &spr_player24, 3 },
		{ &spr_player25, 3 },
		{ &spr_player26, 3 },
		{ &spr_player25, 3 },
		{ &spr_player26, 3 },
		{ &spr_player25, 3 },
		{ &spr_player26, 3 },
		{ &spr_player25, 3 },
		{ &spr_player26, 3 },
		{ &spr_player25, 3 },
		{ &spr_player26, 3 },

		{ nullptr, -1 },// 終了フラグ

	};
	//攻撃ループ
	AnimeData animePlayerA2ROOP[] = {
		
		{ &spr_player25, 3 },
		{ &spr_player26, 3 },

		{ nullptr, -1 },// 終了フラグ

	};

	//ダメージモーション
	AnimeData animePlayerD[] = {
		{ &spr_player14, 2 },
		{ &spr_player15, 2 },
		{ &spr_player16, 2 },
		{ &spr_player14, 2 },
		{ &spr_player15, 2 },
		{ &spr_player16, 2 },
		{ &spr_player14, 2 },
		{ &spr_player15, 2 },
		{ &spr_player16, 2 },
		{ &spr_player14, 2 },
		{ &spr_player15, 2 },
		{ &spr_player16, 2 },

		{ nullptr, -1 },// 終了フラグ
	};
}



void PlayerSpeedDiside(OBJ2D* obj)
{

	//コンボ数によってスピードを変化させる
	if (obj->comboCnt < COMBOLEVEL_1)
		obj->param = 0;
	if (obj->comboCnt >= COMBOLEVEL_1)
		obj->param = 1;
	if (obj->comboCnt >= COMBOLEVEL_2)
		obj->param = 2;
	if (obj->comboCnt >= COMBOLEVEL_3)
		obj->param = 3;


}

//スコア加算
void AddScore(OBJ2D* obj, int addScore)
{
	int& plScore = obj->iWork[player::iWork::score];

	//コンボ数によって加算量アップ
	plScore += addScore *(obj->comboCnt+1);

	//カンスト
	if (plScore > 999999)
		plScore = 999999;
}

//--------------------------------
//  当たり判定
//--------------------------------
void PlayerHitCheck(OBJ2D* obj)
{
	//電撃床との当たり判定
	for (auto &enemy : *pEnemyManager)
	{
		if (enemy.mover != enemy_move07)
			continue;
		if (judgeCheck2(*obj, enemy))
		{
			obj->slow = true;//触れている間スピードダウン
			break;
		}
		else obj->slow = false;
	}



	// エネミー
	for (auto &enemy : *pEnemyManager) {
		if (judgeCheck2(*obj, enemy))
		{
			if (enemy.type==enemy::Type::GIMMICK)//ギミック以外と当たり判定
				continue;
			// ダメージ無敵中なら処理を行わない
			if (!obj->moveFlg&&obj->damageTimer != 0) {
				continue;
			}
			obj->dashCnt += PLUS_DASH_CNT;//敵に当たるとカウントプラス
			SCENE_GAME->HitStopTrg(ENEMY_STOP_FRAME);//ヒットストップ

			// ダッシュ中
			if (obj->moveFlg) {
				sound::play(SOUND::SOUND_DESTROY);//敵撃破音
				// コンボ増加
				if(obj->comboCnt<99)++obj->comboCnt;
					

				//プレイヤーの座標にコンボカウントごとにエフェクトを生成
				switch (obj->comboCnt)
				{
				case COMBOLEVEL_1:
				{
					OBJ2D* child = pEffectManager->searchSet(effect_move_player00, VECTOR2(0, 0));
					child->parent = obj;
					sound::play(SOUND::SOUND_SPEED_UP);
					{
						//スピードアップの文字エフェクト
						OBJ2D*child2 = pEffectManager->searchSet(effect_move_speedup, VECTOR2(obj->position.x, obj->position.y));

						child2->parent = obj;
					}
				}
				break;

				case COMBOLEVEL_2:
				{
					OBJ2D* child = pEffectManager->searchSet(effect_move_player00, VECTOR2(0, 0));
					child->parent = obj;
					sound::play(SOUND::SOUND_SPEED_UP);
					{
						//スピードアップの文字エフェクト
						OBJ2D*child2 = pEffectManager->searchSet(effect_move_speedup, VECTOR2(obj->position.x, obj->position.y));

						child2->parent = obj;
					}
				}
				break;

				case COMBOLEVEL_3:
				{
					OBJ2D* child = pEffectManager->searchSet(effect_move_player00, VECTOR2(0, 0));
					child->parent = obj;
					sound::play(SOUND::SOUND_SPEED_UP);
					{
						//スピードアップの文字エフェクト
						OBJ2D*child2 = pEffectManager->searchSet(effect_move_speedup, VECTOR2(obj->position.x, obj->position.y));

						child2->parent = obj;
					}
				}
				break;

				}

				//敵によってスコア加算
				if (enemy.mover == enemy_move00)
					AddScore(obj, ADDSCORE_ENEMY00);
				if (enemy.mover == enemy_move01)
					AddScore(obj, ADDSCORE_ENEMY01);
				if (enemy.mover == enemy_move02)
					AddScore(obj, ADDSCORE_ENEMY02);
				if (enemy.mover == enemy_move03)
					AddScore(obj, ADDSCORE_ENEMY03);
				if (enemy.mover == enemy_move04)
					AddScore(obj, ADDSCORE_ENEMY03);

				// 接触したエネミーを消去
				pEnemyManager->searchSet(enemy_move06, enemy.position);//塵生成
				pEffectManager->searchSet(effect_move_enemy_death, enemy.position);
				enemy.clear();
				
			}

			// ダッシュしてないときはダメージを受けるだけ
			else {
				if (!pPlayerManager->isBarrier_flg&&obj->damageTimer <= 0) {
					obj->damageTimer = DAMAGE_FRAME;//のけぞり時間設定
					obj->comboCnt = 0;
					pPlayerManager->isPowerup_flg = false;//パワーアップ解除
					sound::play(SOUND::SOUND_DAMAGED);
					pEffectManager->searchSet(effect_move_enemy_death, enemy.position);//敵破壊エフェクト
					enemy.clear();
				}
				else if (pPlayerManager->isBarrier_flg)
				{
					obj->damageTimer = MUTEKI_FRAME;//バリアが剥がれたら一定時間無敵
				}
				pPlayerManager->isBarrier_flg = false;//バリア解除

			}
		}
	}
	// エネミー弾との当たり判定(ダッシュ中でもダメージ喰らう)
	for (auto &enemyshot : *pEnemyShotManager) {
		if (judgeCheck2(*obj, enemyshot))
		{
			
			if (!pPlayerManager->isBarrier_flg&&obj->damageTimer <= 0) {
				obj->damageTimer = DAMAGE_FRAME;//のけぞり時間設定
				obj->comboCnt = 0;
				pPlayerManager->isPowerup_flg = false;//パワーアップ解除
				sound::play(SOUND::SOUND_DAMAGED);//ダメージ音
			}
			else if (pPlayerManager->isBarrier_flg)
			{
				obj->damageTimer = MUTEKI_FRAME;//バリアが剥がれたら一定時間無敵
			}


				// 接触したエネミーを消去
			pPlayerManager->isBarrier_flg = false;//バリア解除
			enemyshot.clear();//敵の弾を消しておく


			}

			
		}

	//アイテムとの当たり判定
	for (auto &item : *pItemManager)
	{

		if (judgeCheck2(*obj, item))
		{
			sound::play(SOUND::SOUND_GET);//アイテム取得音

			if (item.mover == ItemMove00)
			{
				//パワーアップ
				OBJ2D*child = pEffectManager->searchSet(effect_move_powerup, VECTOR2(obj->position.x, obj->position.y));

				child->parent = obj;

				pPlayerManager->isPowerup_flg = true;//パワーアップ解除


				AddScore(obj, ADDSCORE_ITEM);

				item.clear();
			}

			if (item.mover == ItemMove01)
			{
				//バリア張る
				OBJ2D*child = pEffectManager->searchSet(effect_move_barrier, VECTOR2(obj->position.x, obj->position.y));
				sound::play(SOUND::SOUND_SPEED_UP);

				child->parent = obj;
				//バリアゲット文字エフェクト
				OBJ2D*child2 = pEffectManager->searchSet(effect_move_protect, VECTOR2(obj->position.x, obj->position.y));

				child2->parent = obj;
				pPlayerManager->isBarrier_flg = true;
				AddScore(obj, ADDSCORE_ITEM);

				item.clear();
			}

			if (item.mover == ItemMove02)
			{
				obj->comboCnt+=5;
				sound::play(SOUND::SOUND_SPEED_UP);
				OBJ2D*child = pEffectManager->searchSet(effect_move_speedup, VECTOR2(obj->position.x, obj->position.y));
				child->parent = obj;
				AddScore(obj, ADDSCORE_ITEM);

				item.clear();
			}

			if (item.mover == ItemMove03)
			{
				sound::play(SOUND::SOUND_RESQUE);
				AddScore(obj, ADDSCORE_HELP);
				pPlayerManager->resqueNum++;//救助数加算
				item.clear();
			}


			
		}
	}



	//ゴール地点との当たり判定
	for (auto &door : *pDoorManager)
	{
		if (judgeCheck(door, *obj))
			pBitFlg->Flg_On(obj->iWork[player::goalFlg], player::isGoal);//ゴールフラグオン
	}
}

//--------------------------------
//  位置更新
//--------------------------------
// エンターキーを押すとショットの方向へ移動
void PlayerMovePos(OBJ2D* obj, float oldX, float oldY)
{
	using namespace input;
	float & fromPlShotAngle = obj->fWork[player::fWork::fromPlShotAngle];
	float dir = 0;//方向決定用



	if (GET_LEFT_STICK_Y > 0)
		dir = -1;
	if (GET_LEFT_STICK_Y < 0)
		dir = 1;



	if (!obj->moveFlg&&obj->speed.x > -GRAVITY_LIMIT)obj->speed.x -= GRAVITY;



	// スティック情報保持用
	VECTOR2 direction;

	//// ダッシュしてないときだけスティックの情報を取得する
	if (!obj->moveFlg) {
		direction = VECTOR2(GET_LEFT_STICK_X, -GET_LEFT_STICK_Y);


		//	HACK:デバッグ用キーボード操作
		if (STATE(0)&PAD_UP)
			direction = VECTOR2(0, -1);
		if (STATE(0)&PAD_DOWN)
			direction = VECTOR2(0,  1);
		if (STATE(0)&PAD_RIGHT)
			direction = VECTOR2(1, 0);
		if (STATE(0)&PAD_LEFT)
			direction = VECTOR2(-1, 0);


		fromPlShotAngle = atan2(direction.y, direction.x);

	}


	// 速度算出 
	if (!obj->moveFlg) {


		PlayerSpeedDiside(obj);



		if (obj->damageTimer<MUTEKI_FRAME&&TRG(0) & PAD_TRG1) {

			if (pPlayerManager->isPowerup_flg)
				sound::play(SOUND::SOUND_DRILL2);
			else
				sound::play(SOUND::SOUND_DRILL);

			obj->moveFlg = true;
			obj->speed.y = sinf(fromPlShotAngle)*((PLAYER_SPEED_INIT) + obj->param * 2);//初速X方向
			obj->speed.x = cosf(fromPlShotAngle)*((PLAYER_SPEED_INIT) + obj->param * 2);//初速Y方向
			obj->dashCnt = PLAYER_DASH_CNT;
			obj->velocity.x = PLAYER_VELOCITY*sinf(fromPlShotAngle);
			obj->velocity.y = PLAYER_VELOCITY*cosf(fromPlShotAngle);

			OBJ2D* child2 = pEffectManager->searchSet(effect_move_player02, VECTOR2(0, 0));
			child2->parent = obj;
		}
		else Floating_Move(obj);//操作していないときはふわふわ

	}
	obj->dashCnt--;

	if (obj->dashCnt < 0)
	{
		obj->moveFlg = false;
	}


	//画面外補正（ゴール処理時は無視
	if (!pBitFlg->Flg_Check(obj->iWork[player::goalFlg], player::isGoal) &&
		(obj->position.x > system::SCREEN_WIDTH - obj->size.x + pBGManager->getScrollPos().x))
	{
		obj->position.x = system::SCREEN_WIDTH - obj->size.x + pBGManager->getScrollPos().x;

		obj->moveFlg = false;
	}

	else if (pBitFlg->Flg_Check(obj->iWork[player::goalFlg], player::isGoal)) {
		obj->mover = player_move_finish;
	}
	



	//===各方向軸成分の移動とマップチップとの当たり判定、および補正==================

	// y軸加算
	if (obj->moveFlg)obj->speed.y += obj->velocity.y*dir;//加速度を向いてる方向に加える

	if(obj->slow)
	obj->position.y += obj->speed.y*PL_SLOW_SPEED;//スロウモード時
	else 
	obj->position.y += obj->speed.y;

	float deltaY = obj->position.y - oldY;//y軸変位計算


	// 下方向
	if (deltaY > 0)
	{
		//マップチップとの当たり判定及び補正
		if (pBGManager->isFloor(obj->position.x, obj->position.y + obj->size.y, obj->size.x))
		{
			pBGManager->mapHoseiDown(obj);
			obj->moveFlg = false;
			obj->speed = {};
		}
		//ギミックとの当たり判定及び補正
		if (pGimmickTerrManager->isFloor(obj->position.x, obj->position.y + obj->size.y, obj->size.x))
		{
			pGimmickTerrManager->mapHoseiDown(obj);
			obj->moveFlg = false;
			obj->speed = {};
		}
	}
	// 上方向
	if (deltaY < 0)
	{
		//マップチップとの当たり判定及び補正
		if (pBGManager->isCeiling(obj->position.x, obj->position.y - obj->size.y, obj->size.x))
		{
			pBGManager->mapHoseiUp(obj);
			obj->moveFlg = false;
			obj->speed = {};

		}
		//ギミックとの当たり判定及び補正
		if (pGimmickTerrManager->isCeiling(obj->position.x, obj->position.y - obj->size.y, obj->size.x))
		{
			pGimmickTerrManager->mapHoseiUp(obj);
			obj->moveFlg = false;
			obj->speed = {};

		}
	}

	// x軸加算
	if (obj->moveFlg)obj->speed.x += obj->velocity.x;
	if(obj->slow)obj->position.x += obj->speed.x*PL_SLOW_SPEED;//スロウモード時
	else obj->position.x += obj->speed.x;
	float deltaX = obj->position.x - oldX;//x軸変位計算



	// 右方向
	if (deltaX > 0)
	{

		//マップチップとの当たり判定及び補正
		if (pBGManager->isWall(obj->position.x + obj->size.x, obj->position.y, obj->size.y))
		{
			pBGManager->mapHoseiRight(obj);
			obj->moveFlg = false;
			obj->speed = {};

		}

		//ギミックとの当たり判定及び補
		if (pGimmickTerrManager->isWall(obj->position.x + obj->size.x, obj->position.y, obj->size.y))
		{
			pGimmickTerrManager->mapHoseiRight(obj);
			obj->moveFlg = false;
			obj->speed = {};

		}

	}
	// 左方向
	if (deltaX < 0)
	{

		//マップチップとの当たり判定及び補正
		if (pBGManager->isWall(obj->position.x - obj->size.x, obj->position.y, obj->size.y))
		{
			pBGManager->mapHoseiLeft(obj);
			obj->moveFlg = false;
			obj->speed = {};

		}


		//ギミックとの当たり判定及び補正
		if (pGimmickTerrManager->isWall(obj->position.x - obj->size.x, obj->position.y, obj->size.y))
		{
			pGimmickTerrManager->mapHoseiLeft(obj);
			obj->moveFlg = false;
			obj->speed = {};

		}

	}

}


//--------------------------------
// プレイヤー移動関数
//--------------------------------
void player_move(OBJ2D* obj)
{
	using namespace input;  // 関数内で入力処理を行うときに記述する

	AnimeData* animeData = nullptr;

	float & fromPlShotAngle = obj->fWork[player::fWork::fromPlShotAngle];

	switch (obj->state)
	{
	case 0:
		//////// 初期設定 ////////
		obj->size = VECTOR2(PLAYER_SIZEX , PLAYER_SIZEY);//当たり判定
		obj->scale *= PLAYER_SCALE;//スケール
		obj->param = 1;//汎用パラメータをスピード倍率用に使用している
		obj->wait = PLAYER_WAIT;//重量
		// アニメの初期設定
		animeData = animePlayerN;   // 初期値アニメ
		obj->state++;
		//	break;このbreak;は意図的にコメントとして記述している

	case 1:

		//////// 通常時 ////////
		animeData = animePlayerN;

		if (obj->moveFlg)
		{
			animeData = animePlayerA;	//攻撃モーション１
			if(pPlayerManager->isPowerup_flg)
			animeData = animePlayerA2;	//攻撃モーション2
		}
		float oldX = obj->position.x;
		float oldY = obj->position.y;
		obj->center = obj->position;

		obj->timer++;

		//ゲーム開始まで操作不可
		if (SCENE_GAME->timer > START_TIME)
			PlayerMovePos(obj, oldX, oldY);

		//描画用角度計算
		if (obj->moveFlg) {
			obj->angle = fromPlShotAngle;
			if (obj->angle == ToRadian(180))
				obj->scale.y = -PLAYER_SCALE;
			
		}
		else
		{
			obj->scale.y = PLAYER_SCALE;
			obj->angle = 0;
		}

		//ダメージ時間計算
		obj->damageTimer--;
		if (obj->damageTimer < 0) obj->damageTimer = 0;


		//ダメージ中なら
		if (obj->damageTimer) {

			if(obj->damageTimer>MUTEKI_FRAME)
			animeData = animePlayerD;

			//点処理滅
			PlayerBlink(obj);
			
		}
		else
		obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);//カラー初期化（バグ防止)



		//当たり判定チェック
		PlayerHitCheck(obj);


		obj->animeUpdate(animeData);

	
		break;
	}

	//HACK::強制ゴール
	/*if(DEBUG_NOW&&TRG(0) & PAD_SELECT)
	pBitFlg->Flg_On(obj->iWork[player::goalFlg], player::isGoal);
*/
	
	//上の角度計算だと進行方向と逆に行くと上下反対になってしまうので例外処理
	if (obj->moveFlg&&obj->speed.x < 0)
		obj->scale.y = -PLAYER_SCALE;
}



//プレイヤー移動関数２(ステージクリア時)
void player_move_finish(OBJ2D* obj)
{
	AnimeData* animeData = nullptr;

	switch (obj->state)
	{
	case 0:
		//////// 初期設定 ////////
		obj->scale *= PLAYER_SCALE;
		// アニメの初期設定
		animeData = animePlayerN;   // 初期値として下向きのアニメを設定する

		obj->state++;
		//	break;このbreak;は意図的にコメントとして記述している

	case 1:

		//演出用パラメータ設定
		obj->speed.y = 0;
		obj->speed.x = -FINISH_SPEED;
		obj->velocity.x = FINISH_VELOCITY;
		obj->state++;
		obj->angle = 0;
		GameLib::sound::play(SOUND::SOUND_GOAL);

		//break;
	case 2:

		animeData = animePlayerA2ROOP;	//攻撃モーション2をループする
		obj->animeUpdate(animeData);
		obj->speed.x += obj->velocity.x;
		obj->position.x += obj->speed.x;
		break;
	}
	obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);//バグ防止のカラー初期化

}

//==============================================================================
//
//		PlayerManagerクラス
//
//==============================================================================

//--------------------------------
//  初期設定
//--------------------------------
void PlayerManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化

	STAGE_SCRIPT*pScript = pStageManager->getStageScript();

	//  プレイヤーデータの読み込み
	char** mapPlayer = new char*[pBGManager->CHIP_NUM_Y];
	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
		mapPlayer[i] = new char[pBGManager->CHIP_NUM_X];
		SecureZeroMemory(mapPlayer[i], sizeof(char)*pBGManager->CHIP_NUM_X);
	}


	if (!pBGManager->loadMapData(pScript->fileNamePlayer, mapPlayer))
	{
		assert(!"プレイヤーデータ読み込み失敗");
	}

	// プレイヤー配置
	for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
	{
		for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
		{
			const int playerIndex = mapPlayer[y][x];
			if (playerIndex < 0) continue;

			// 生成
			obj_w[0].mover = &player_move;
			obj_w[0].position = VECTOR2(static_cast<float>(x*BG::CHIP_SIZE + BG::CHIP_SIZE / 2), static_cast<float>(y*BG::CHIP_SIZE + BG::CHIP_SIZE));
			break;
		}
		if (obj_w[0].mover)break;
	}

	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
	{
		delete[] mapPlayer[i];
	}
	delete[] mapPlayer;



	//メンバ変数の初期化
	isPowerup_flg=false;
	isBarrier_flg=false;
	resqueNum = 0;
	int score = pPlayerManager->getOBJ2D(0)->iWork[player::iWork::score];
	score = 0;
}

//--------------------------------
//  更新
//--------------------------------
void PlayerManager::update()
{
	// 移動処理
	OBJ2DManager::update(); // OBJ2DManagerの更新
}

//--------------------------------
//  描画
//--------------------------------
void PlayerManager::draw()
{
	// 描画処理

	
	OBJ2DManager::draw();   // OBJ2DManagerの描画

	//デバッグ用に当たり判定可視化
	//if (DEBUG_NOW)
	//for (auto &obj : *pPlayerManager) {
	//	primitive::rect(obj.position.x - obj.size.x - pBGManager->getScrollPos().x, obj.position.y - obj.size.y - pBGManager->getScrollPos().y, obj.size.x * 2, obj.size.y * 2, 0, 0, 0, 1, 0, 0, 0.3f);
	//}
}


//点滅処理
void PlayerBlink(OBJ2D*obj)
{
	switch (obj->damageTimer % 10)
	{
	case 9:	obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 0.8f); break;
	case 8: obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 0.6f); break;
	case 7: obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 0.4f); break;
	case 6: obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 0.2f); break;
	case 5: obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 0);	  break;
	case 4: obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 0.2f); break;
	case 3: obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 0.4f); break;
	case 2: obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 0.6f); break;
	case 1: obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 0.8f); break;
	case 0: break;
	}
}

