#pragma once

//******************************************************************************
//
//
//      OBJ2Dクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "./GameLib/vector.h"
#include "./GameLib/obj2d_data.h"


// 前方宣言
class OBJ2D;

//==============================================================================

class OBJ2D;							// 前方宣言
typedef	void(*MOVER)(OBJ2D* obj);		// 関数ポインタ（更新処理）
typedef bool(*ERASER)(OBJ2D* obj);		// 関数ポインタ（消去チェック）
void Floating_Move(OBJ2D*obj);



//==============================================================================

//==============================================================================
//
//      OBJ2Dクラス
//
//==============================================================================

class OBJ2D
{
public:
	// メンバ変数
	GameLib::SpriteData*    data;               // スプライトデータ
	GameLib::Anime          anime;              // アニメーションクラス
	VECTOR2                 position;           // 位置
	VECTOR2					center;				//　中心
	VECTOR2                 scale;              // スケール
	float                   angle;              // 描画角度
	VECTOR4                 color;              // 描画色
	VECTOR2                 size;               // あたり用サイズ（縦横
	MOVER					mover;				// 更新処理
	ERASER					eraser;				// 消去チェック
	VECTOR2                 velocity;           // 速度
	VECTOR2                 speed;              // 瞬間の移動量ベクトル
	int                     state;              // ステート
	int						act_state;			// アクションステート
	int                     timer;              // タイマー
	int                     param;              // 汎用パラメータ
	int						sway;	            // 画面揺れ幅
	int						cnt;				// 汎用カウンター
	int						index;				// インデックス
	int                     iWork[16];          // 汎用ワークエリア
	float                   fWork[16];          // 汎用ワークエリア
	int                     mutekiTimer;        // 無敵タイマー
	int                     damageTimer;        // ダメージタイマー
	float					wait;				// 質量
	bool                    judgeFlag;          // あたり判定の有無（true:有り / false:無し）
	bool                    flg;				// 汎用フラグ
	bool                    slow;				// スピード減少フラグ
	bool					moveFlg;			// 移動中かのフラグ
	OBJ2D*					parent;				// 親子関係用
	int						type;				// 敵のタイプをenumで管理
	int						comboCnt;			// コンボ数
	int						dashCnt;			// ダッシュ持続フレーム

public:

	OBJ2D();        // コンストラクタ
	void clear();   // メンバ変数のクリア
	void update();
	void draw();    // 描画
	void draw_not_scroll();    // 描画

	void GameSway(OBJ2D*obj);//画面揺れ
	bool animeUpdate(GameLib::AnimeData* animeData);    // アニメーションのアップデート
};

//==============================================================================

// OBJ2DManagerクラス
class OBJ2DManager
{
protected:
	std::vector<OBJ2D>	obj_w;
public:
	virtual void init();				// 初期設定
	virtual	void update();				// 更新処理
	virtual void draw();				// 描画処理
	virtual void draw_not_scroll();				// 描画処理


	// ゲッターセッター諸々
	OBJ2D* getOBJ2D(int workNum) { return &obj_w[workNum]; }
	
	void setFWork(int workNum, int fWorkNum, float setFloat) { obj_w[workNum].fWork[fWorkNum] = setFloat; }
	

	OBJ2D* searchSet(MOVER mover, VECTOR2 pos);	// OBJの追加
												//画面揺れ

	auto begin() { return obj_w.begin(); }
	auto end() { return obj_w.end(); }
private:
	virtual int getSize() = 0;
};


//******************************************************************************

