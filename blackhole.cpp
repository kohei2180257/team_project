#include "All.h"

using namespace GameLib;


#define HOLE_POSX  -200
#define HOLE_POSY   340
#define HOLE_SIZEX  290
#define HOLE_SIZEY  740
#define HOLE_SCALE  2.0f


void hole_hitcheck(OBJ2D * obj)
{
	OBJ2D* pl = pPlayerManager->getOBJ2D(0);

	//終了演出中は処理を行わない
	if (pl->mover == player_move_finish)return;

	//当たるとプレイヤー死亡フラグOn
	if (judgeCheck(*pl, *obj))
		pBitFlg->Flg_On(pl->iWork[player::goalFlg], player::isDead);

}

AnimeData animeBlackHole00[]
{
	{ &spr_enemy_black_hole00,3 },
	{ &spr_enemy_black_hole01,3 },
	{ &spr_enemy_black_hole02,3 },
	{ &spr_enemy_black_hole03,3 },
	{ &spr_enemy_black_hole04,3 },
	{ &spr_enemy_black_hole05,3 },
	{ &spr_enemy_black_hole06,3 },
	{ &spr_enemy_black_hole07,3 },
	{ nullptr, -1 },// 終了フラグ
};


void hole_move(OBJ2D * obj)
{
	switch (obj->state)
	{
	case 0:
		obj->position = VECTOR2(HOLE_POSX, HOLE_POSY);
		obj->size = VECTOR2(HOLE_SIZEX, HOLE_SIZEY);
		obj->scale = VECTOR2(HOLE_SCALE, HOLE_SCALE);
		obj->timer = 0;
		obj->state++;

		//break
	case 1:
		AnimeData* animeData = NULL;
		animeData = animeBlackHole00;
		obj->animeUpdate(animeData);
		hole_hitcheck(obj);//当たり判定チェック

		//スクロール速度と同じスピードで動き、プレイヤーを追いかける
		if(SCENE_GAME->timer>START_TIME)
		obj->position.x+=1* pStageManager->scroll_speed;

		obj->timer++;
		break;
	}
}

void HoleManager::init()
{
	OBJ2DManager::init();
	obj_w[0].mover = hole_move;//移動関数のセット
}

void HoleManager::update()
{
	OBJ2DManager::update();
}

void HoleManager::draw()
{
	
	OBJ2DManager::draw();

	//OBJ2D obj = obj_w[0];
	/*if(DEBUG_NOW)
	primitive::rect(obj.position.x - obj.size.x-pBGManager->getScrollPos().x, obj.position.y - obj.size.y-pBGManager->getScrollPos().y, obj.size.x * 2, obj.size.y * 2, 0, 0, 0, 1, 0, 0, 0.3f);
*/
}

