//******************************************************************************
//
//
//      シーン管理
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

//==============================================================================
//
//      Sceneクラス
//
//==============================================================================
//
void Scene::init()
{

	state = 0;
	timer = 0;
	nextScene = nullptr;
	select = 0;

	ZeroMemory(&selectData, sizeof(selectData));
}

void Scene::uninit()
{
	
}


//--------------------------------
//  実行
//--------------------------------
Scene* Scene::execute()
{
    using namespace GameLib;

    // 初期化処理
    init();

    // ゲームループ
    while (GameLib::gameLoop(true))    // falseをtrueにするとタイトルバーにフレームレート表示
    {
        // 入力処理
        input::update();

        // 更新処理
        update();

        // 描画処理
        draw();

        // デバッグ文字列の描画
        debug::display(1, 1, 0, 1, 1);	// 黄色でスケール等倍

        // 画面フリップ
        GameLib::present(1, 0);

        // 終了チェック
        if (nextScene) break;
    }

    // 終了処理
    uninit();

    return nextScene;	// 次のシーンを返す
}

//******************************************************************************
//
//      SceneManagerクラス
//
//******************************************************************************

//--------------------------------
//  実行
//--------------------------------
void SceneManager::execute(Scene* scene)
{
    using namespace GameLib;

    bool isFullScreen = true;	// フルスクリーンにするならtrueに変える
                                //（Releaseモードのみ）

    // ゲームライブラリの初期化処理
    GameLib::init(L"PLABIT", 
        static_cast<int>(system::SCREEN_WIDTH), 
        static_cast<int>(system::SCREEN_HEIGHT), 
        isFullScreen);

    ShowCursor(!isFullScreen);	// フルスクリーン時はカーソルを消す

    // メインループ
    while (scene)
    {
        scene = scene->execute();
    }

    // ゲームライブラリの終了処理
    GameLib::uninit();
}

//******************************************************************************
