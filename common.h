#pragma once

//******************************************************************************
//
//
//      common
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include <DirectXMath.h>



#define DEBUG_NOW false//trueにすると当たり判定描画

#define FRAME_RATE 60

//------< 関数 >----------------------------------------------------------------

static float(*const ToRadian)(float) = DirectX::XMConvertToRadians;  // 角度をラジアンに
static float(*const ToDegree)(float) = DirectX::XMConvertToDegrees;  // ラジアンを角度に


//BGM用ラベル
enum BGM
{
	DEMOBGM,
	TITLEBGM,
	GAMEBGM,
	WINBGM,
	LOSEBGM
};

//効果音用ラベル
enum SOUND
{
	SOUND_GET,
	SOUND_RESQUE,
	SOUND_DECIDE,
	SOUND_DRILL,
	SOUND_SELECT,
	SOUND_DAMAGED,
	SOUND_DESTROY,
	SOUND_SPEED_UP,
	SOUND_GOAL,
	SOUND_SPAWN,
	SOUND_DRILL2,
	SOUND_SHOT,

};
