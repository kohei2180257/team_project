#pragma once

#define DEMO_MAX_SIZE 2

class DemoManager : public OBJ2DManager, public Singleton<DemoManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return DEMO_MAX_SIZE; }
};

#define pDemoManager	(DemoManager::getInstance())

