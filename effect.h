#pragma once

// ラベル定義
#define EFFECT_MAX		1000

// EffectManagerクラス
class EffectManager : public OBJ2DManager, public Singleton<EffectManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return EFFECT_MAX; }
};

#define pEffectManager	(EffectManager::getInstance())

// プロトタイプ宣言
void effect_move_enemy_death(OBJ2D*);
void effect_move_player_spawn(OBJ2D*);

void effect_move_player00(OBJ2D*obj);
void effect_move_player01(OBJ2D*obj);
void effect_move_player02(OBJ2D*obj);

void effect_move_powerup(OBJ2D*obj);
void effect_move_speedup(OBJ2D*obj);
void effect_move_barrier(OBJ2D*obj);
void effect_move_protect(OBJ2D*obj);

