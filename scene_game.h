#pragma once

//******************************************************************************
//
//
//      ゲームシーン
//
//
//******************************************************************************

//==============================================================================
//
//      Gameクラス
//
//==============================================================================

class Game : public Scene, public Singleton<Game>
{
public:

private:
    bool isPaused;
	int	 hitStopFrame;
public:
    void init();
    void update();
    void draw();
    void uninit();		
	bool HitStop();
	void HitStopTrg(int hitStopFrame2) { hitStopFrame=hitStopFrame2,stop_fg = true; }
};

static Game* const SCENE_GAME = Game::getInstance();

//******************************************************************************

