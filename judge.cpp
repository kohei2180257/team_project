
#include "all.h"


using namespace GameLib;





//--------------------------------
//  矩形と矩形のあたり判定
//--------------------------------
bool rectHitCheck(const fRECT& rect1, const fRECT& rect2)
{
	// 条件を満たさなければすぐリターンさせる（早期リターン）
	if (rect1.left   > rect2.right)  return false;
	if (rect1.right  < rect2.left)   return false;
	if (rect1.top    > rect2.bottom) return false;
	if (rect1.bottom < rect2.top)    return false;
	return true;    // あたっている
}


//--------------------------------
//  obj1とobj2が当たっているかどうかのチェック
//--------------------------------
bool judgeCheck(const OBJ2D& obj1, const OBJ2D& obj2)
{
		fRECT rect1(
		obj1.position.x - obj1.size.x, obj1.position.y - obj1.size.y,
		obj1.position.x + obj1.size.x, obj1.position.y +obj2.size.y
		);

	fRECT rect2(
		obj2.position.x - obj2.size.x, obj2.position.y - obj2.size.y,
		obj2.position.x + obj2.size.x, obj2.position.y + obj2.size.y
		);

	return rectHitCheck(rect1, rect2);
}

bool judgeCheck2(const OBJ2D& obj1, const OBJ2D& obj2)
{

	if (obj1.moveFlg&&!pPlayerManager->isPowerup_flg) {
		fRECT rect1(
			obj1.position.x - obj1.size.x * 2, obj1.position.y - obj1.size.y * 2,
			obj1.position.x + obj1.size.x * 2, obj1.position.y + obj1.size.y * 2
		);
		fRECT rect2(
			obj2.position.x - obj2.size.x, obj2.position.y - obj2.size.y,
			obj2.position.x + obj2.size.x, obj2.position.y + obj2.size.y
		);
		return rectHitCheck(rect1, rect2);

	}
	else if (obj1.moveFlg&&pPlayerManager->isPowerup_flg) {//パワーアップ時は当たり判定を大きく
		fRECT rect1(
			obj1.position.x - obj1.size.x * 3, obj1.position.y - obj1.size.y * 3,
			obj1.position.x + obj1.size.x * 3, obj1.position.y + obj1.size.y * 3
		);
		fRECT rect2(
			obj2.position.x - obj2.size.x, obj2.position.y - obj2.size.y,
			obj2.position.x + obj2.size.x, obj2.position.y + obj2.size.y
		);
		return rectHitCheck(rect1, rect2);

	}
	else {
		fRECT rect1(
			obj1.position.x - obj1.size.x, obj1.position.y - obj1.size.y,
			obj1.position.x + obj1.size.x, obj1.position.y + obj1.size.y
		);

		fRECT rect2(
			obj2.position.x - obj2.size.x, obj2.position.y - obj2.size.y,
			obj2.position.x + obj2.size.x, obj2.position.y + obj2.size.y
		);
		return rectHitCheck(rect1, rect2);

	}
	
}

