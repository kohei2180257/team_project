//******************************************************************************
//
//
//      タイトル
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;


#define DEMO_END_TIME 1800

void Demo::init()
{
	
	state = 0;
	Scene::init();	    // 基底クラスのinitを呼ぶ
}


//--------------------------------
//  更新処理
//--------------------------------
void Demo::update()
{
	using namespace input;

	switch (state)
	{
	case 0:

		//////// 初期設定 ////////
		timer = 0;  // タイマーを初期化
		GameLib::setBlendMode(Blender::BS_ALPHA);   // 通常のアルファ処理
		texture::load(loadTextureDemo);
		music::load(DEMOBGM, L"./Data/Musics/opening.wav", 1.0f);
		music::play(DEMOBGM,true);



		pDemoManager->init();
		

		state++;    // 初期化処理の終了
		//break;    // 意図的なコメントアウト

	case 1:
		//////// 通常時の処理 ////////
		++timer;

		pDemoManager->update();


		//30秒でゲームへ
		if ((TRG(0)& PAD_TRG1)|| timer>DEMO_END_TIME)
		{         
			// PAD_TRG1が押されたら
			changeScene(SCENE_TITLE);    // ゲームシーンに切り替え
		}

	}
}

//--------------------------------
//  描画処理
//--------------------------------
void Demo::draw()
{
    // 画面クリア
    GameLib::clear(VECTOR4(0.0f, 0.0f, 0.0f, 1));

    
	pDemoManager->draw();
	// "Push Start Button" 点滅
	if ((timer / (FRAME_RATE/2)) % 2)
	{
		font::textOut(4, "Push A Button", VECTOR2(TEXTOUT_POSX, TEXTOUT_POSY), VECTOR2(TEXTOUT_SCALE, TEXTOUT_SCALE),VECTOR4(1,1,1,1));
	}
}


void Demo::uninit()
{
	texture::releaseAll();
	music::unload(DEMOBGM);
	
}
//******************************************************************************
